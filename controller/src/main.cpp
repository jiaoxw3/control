/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#include <ros/ros.h>
#include "controller_handle.hpp"

typedef ns_controller::ControllerHandle ControllerHandle;

int main(int argc, char **argv) {
    ros::init (argc, argv, "controller");
    ros::NodeHandle nodeHandle("~");
    ControllerHandle controllerHandle(nodeHandle);
    ros::Rate loop_rate(controllerHandle.node_rate_);

    while (ros::ok()) {
        ros::spinOnce();
        controllerHandle.run();    
        loop_rate.sleep();
    }
    return 0;
}

