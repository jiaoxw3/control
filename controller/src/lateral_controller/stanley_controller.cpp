/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#include "lateral_controller/stanley_controller.hpp"

namespace ns_controller {

// double StanleyController::createLocalSteeringControlCommand(double older_steering_command) {

//     int i_increase_index, i_next;
//     int trajectory_length = 0;
//     double length, beta_est;
//     double Vx = front_axis_center_state_.car_state_dt.car_state_dt.x;
//     double look_forward_distance = look_forward_distance_local_ + look_forward_k_local_ * Vx;

//     trajectory_length = local_trajectory_.poses.size();

//     std::cout << "\nwe are doing StanleyController::createLocalSteeringControlCommand" 
//     << "\nlook_forward_distance = " << "look_forward_distance_local: " << look_forward_distance_local_ << " + " << 
//     "look_forward_k_local: " << look_forward_k_local_ << " * " << "Vx: " << Vx << " = " << look_forward_distance
//     << "\nsteering_p_local: " << steering_p_local_ << std::endl;

//     if (trajectory_length <= 0){
//         std::cout << "the trajectory is empty, delta = 0" << std::endl;
//         return delta_ = 0;
//     }

//     i_increase_index = ceil(look_forward_distance / path_resolution_);

//     i_next           = (index_ + i_increase_index) % trajectory_length;

//     geometry_msgs::Pose next_point = local_trajectory_.poses[i_next].pose;

//     length           = std::hypot(next_point.position.y, next_point.position.x + lidar2rearwheel_ - wheel_base_);

//     //1.calculate crosstrack error
//     crosstrack_error_ = next_point.position.y;

//     //2.limit the crosstrack error
//     crosstrack_error_ = (crosstrack_error_ > 0)?std::min(crosstrack_error_, 5.0):std::max(crosstrack_error_, -5.0);

//     //3.calculate heading error
//     beta_est = older_steering_command * 0.5;
//     heading_error_ = next_point.orientation.z;

//     //4.limit the heading error
//     if (heading_error_ > 0)
//         heading_error_ = (heading_error_ < M_PI / 3)?heading_error_:(M_PI / 3);
//     else
//         heading_error_ = (heading_error_ > -M_PI / 3)?heading_error_:(-M_PI / 3);
//     //5.calculate the delta(steering angle)
//     Vx = std::max(Vx, 1.0);

//     delta_ = heading_error_ + atan2(steering_p_global_ * crosstrack_error_, Vx);

//     std::cout << "trajectory_length: " << trajectory_length
//     << "\nheading error: " << heading_error_
//     << "\nindex: " << index_
//     << "\ni_next: " << i_next 
//     << "\nreal look forward distance: " << length
//     << "\nthe CURRENT car state \"/world\" : [" << state_.car_state.x << ", " << state_.car_state.y << ", " << state_.car_state.theta << "]" 
//     << "\nthe FRONTAXIS point \"/world\" : [" <<  front_axis_center_state_.car_state.x << ", " << front_axis_center_state_.car_state.y << ", " << front_axis_center_state_.car_state.theta << "]"
//     << "\nthe CLOSEST point \"/rslidar\" : [" <<  local_trajectory_.poses[index_].pose.position.x << ", " << local_trajectory_.poses[index_].pose.position.y << ", " << local_trajectory_.poses[index_].pose.orientation.z << "]"
//     << "\nthe NEXT point \"/rslidar\" : [" <<  next_point.position.x << ", " << next_point.position.y << ", " << next_point.orientation.z << "]" << std::endl;

//     return delta_;
// }
double StanleyController::createLocalSteeringControlCommand(double older_steering_command) {

    int i_increase_index, i_next;
    int trajectory_length = 0;
    double length, beta_est;
    double Vx = front_axis_center_state_.car_state_dt.car_state_dt.x;
    double look_forward_distance = look_forward_distance_local_ + look_forward_k_local_ * Vx;

    trajectory_length = local_trajectory_.poses.size();

    std::cout << "\nwe are doing StanleyController::createLocalSteeringControlCommand" 
    << "\nlook_forward_distance = " << "look_forward_distance_local: " << look_forward_distance_local_ << " + " << 
    "look_forward_k_local: " << look_forward_k_local_ << " * " << "Vx: " << Vx << " = " << look_forward_distance
    << "\nsteering_p_local: " << steering_p_local_ << std::endl;

    if (trajectory_length <= 0){
        std::cout << "the trajectory is empty, delta = 0" << std::endl;
        return delta_ = 0;
    }

    i_increase_index = ceil(look_forward_distance / path_resolution_);
    int i_range = 5;

    i_next           = (index_ + i_increase_index) % trajectory_length;

    int i_next_max = std::min(i_next + i_range, trajectory_length);
    int i_next_min = std::max(i_next - i_range, 0);
    double temp = 0.0;
    for (int i = i_next_min; i <= i_next_max; ++i){
        temp += local_trajectory_.poses[i].pose.orientation.z;
    }
    double ref_yaw = temp / (i_next_max - i_next_min + 1);

    geometry_msgs::Pose next_point = local_trajectory_.poses[i_next].pose;

    length           = std::hypot(next_point.position.y, next_point.position.x + lidar2rearwheel_ - wheel_base_);
    double alpha     = std::atan2(next_point.position.y, next_point.position.x + lidar2rearwheel_ - wheel_base_);

    //1.calculate lateral error angle
    lateral_error_angle_ = steering_p_local_ * atan2(2 * wheel_base_ * std::sin(alpha), (length + 2 * wheel_base_ * std::cos(alpha)));

    //2.limit the lateral error angle
    if (lateral_error_angle_ > 0)
        lateral_error_angle_ = (lateral_error_angle_ < M_PI / 3)?lateral_error_angle_:(M_PI / 3);
    else
        lateral_error_angle_ = (lateral_error_angle_ > -M_PI / 3)?lateral_error_angle_:(-M_PI / 3);

    //3.calculate heading error
    beta_est = older_steering_command * 0.5;
    heading_error_ = ref_yaw;

    //4.limit the heading error
    if (heading_error_ > 0)
        heading_error_ = (heading_error_ < M_PI / 3)?heading_error_:(M_PI / 3);
    else
        heading_error_ = (heading_error_ > -M_PI / 3)?heading_error_:(-M_PI / 3);
    //5.calculate the delta(steering angle)
    Vx = std::max(Vx, 1.0);

    delta_ = heading_error_ + lateral_error_angle_;

    std::cout << "trajectory_length: " << trajectory_length
    << "\nz: " << local_trajectory_.poses[i_next].pose.orientation.z
    << "\nheading error: " << heading_error_
    << "\nlateral error angle: " << lateral_error_angle_
    << "\nindex: " << index_
    << "\ni_next: " << i_next 
    << "\nreal look forward distance: " << length
    << "\nthe CURRENT car state \"/world\" : [" << state_.car_state.x << ", " << state_.car_state.y << ", " << state_.car_state.theta << "]" 
    << "\nthe FRONTAXIS point \"/world\" : [" <<  front_axis_center_state_.car_state.x << ", " << front_axis_center_state_.car_state.y << ", " << front_axis_center_state_.car_state.theta << "]"
    << "\nthe CLOSEST point \"/rslidar\" : [" <<  local_trajectory_.poses[index_].pose.position.x << ", " << local_trajectory_.poses[index_].pose.position.y << ", " << local_trajectory_.poses[index_].pose.orientation.z << "]"
    << "\nthe NEXT point \"/rslidar\" : [" <<  next_point.position.x << ", " << next_point.position.y << ", " << next_point.orientation.z << "]" << std::endl;
    
    //visualize
    // FOR FSSIM ONLY
    // markers_ = createMarkers(local_trajectory_.poses[index_].pose.position.x , local_trajectory_.poses[index_].pose.position.y, next_point.position.x , next_point.position.y, "fssim/vehicle/front_center_lidar_link");
    markers_ = createMarkers(local_trajectory_.poses[index_].pose.position.x , local_trajectory_.poses[index_].pose.position.y, next_point.position.x , next_point.position.y, "/rslidar");
    
    return delta_;
}

double StanleyController::createGlobalSteeringControlCommand(double older_steering_command) {

    int i_increase_index, i_next;
    int trajectory_length = 0;
    double length, beta_est;
    double Vx = front_axis_center_state_.car_state_dt.car_state_dt.x;
    double look_forward_distance = look_forward_distance_global_ + look_forward_k_global_ * Vx;

    trajectory_length = global_trajectory_.poses.size();

    std::cout << "\nwe are doing StanleyController::createGlobalSteeringControlCommand" 
    << "\nlook_forward_distance = " << "look_forward_distance_global: " << look_forward_distance_global_ << " + " << 
    "look_forward_k_global: " << look_forward_k_global_ << " * " << "Vx: " << Vx << " = " << look_forward_distance
    << "\nsteering_p_global: " << steering_p_global_ << std::endl;

    if (trajectory_length <= 0){
        std::cout << "the trajectory is empty, delta = 0" << std::endl;
        return delta_ = 0;
    }

    i_increase_index = ceil(look_forward_distance / path_resolution_);

    i_next           = (index_ + i_increase_index) % trajectory_length;

    geometry_msgs::Pose next_point = global_trajectory_.poses[i_next].pose;

    length           = std::hypot(next_point.position.y - front_axis_center_state_.car_state.y, next_point.position.x - front_axis_center_state_.car_state.x);

    //1.calculate crosstrack error
    if (((front_axis_center_state_.car_state.y - next_point.position.y) * cos(front_axis_center_state_.car_state.theta) - (front_axis_center_state_.car_state.x - next_point.position.x) * sin(front_axis_center_state_.car_state.theta)) < 0)
        crosstrack_error_ = fabs(sqrt((front_axis_center_state_.car_state.x - next_point.position.x) * (front_axis_center_state_.car_state.x - next_point.position.x) + (front_axis_center_state_.car_state.y - next_point.position.y) * (front_axis_center_state_.car_state.y - next_point.position.y)));
    else
        crosstrack_error_ = -fabs(sqrt((front_axis_center_state_.car_state.x - next_point.position.x) * (front_axis_center_state_.car_state.x - next_point.position.x) + (front_axis_center_state_.car_state.y - next_point.position.y) * (front_axis_center_state_.car_state.y - next_point.position.y)));

    //2.limit the crosstrack error
    crosstrack_error_ = (crosstrack_error_ > 0)?std::min(crosstrack_error_, 5.0):std::max(crosstrack_error_, -5.0);

    //3.calculate heading error
    beta_est = older_steering_command * 0.5;
    double beta = atan2(state_.car_state_dt.car_state_dt.y, state_.car_state_dt.car_state_dt.x);
    heading_error_ = next_point.orientation.z - state_.car_state.theta;
    if (heading_error_ > M_PI)
        heading_error_ -= 2 * M_PI;
    if (heading_error_ < -1 * M_PI)
        heading_error_ += 2 * M_PI;

    //4.limit the heading error
    if (heading_error_ > 0)
        heading_error_ = (heading_error_ < M_PI / 3)?heading_error_:(M_PI / 3);
    else
        heading_error_ = (heading_error_ > -M_PI / 3)?heading_error_:(-M_PI / 3);

    //5.calculate the delta(steering angle)
    Vx = std::max(Vx, 1.0);
    delta_ = heading_error_ + atan2(steering_p_global_ * crosstrack_error_, Vx) + 1 * beta;
    std::cout << "beta: " << beta << std::endl;

    std::cout << "trajectory_length: " << trajectory_length
    << "\nheading error: " << heading_error_
    << "\nindex: " << index_
    << "\ni_next: " << i_next 
    << "\nreal look forward distance: " << length
    << "\nthe CURRENT car state \"/world\" : [" << state_.car_state.x << ", " << state_.car_state.y << ", " << state_.car_state.theta << "]" 
    << "\nthe FRONTAXIS point \"/world\" : [" <<  front_axis_center_state_.car_state.x << ", " << front_axis_center_state_.car_state.y << ", " << front_axis_center_state_.car_state.theta << "]"
    << "\nthe CLOSEST point \"/world\" : [" <<  global_trajectory_.poses[index_].pose.position.x << ", " << global_trajectory_.poses[index_].pose.position.y << ", " << global_trajectory_.poses[index_].pose.orientation.z << "]"
    << "\nthe NEXT point \"/world\" : [" <<  next_point.position.x << ", " << next_point.position.y << ", " << next_point.orientation.z << "]" << std::endl;

    markers_ = createMarkers(global_trajectory_.poses[index_].pose.position.x , global_trajectory_.poses[index_].pose.position.y, next_point.position.x , next_point.position.y, "/world");
    return delta_;
}

//visualization
visualization_msgs::MarkerArray StanleyController::createMarkers(double x_pos, double y_pos, double x_next, double y_next, std::string frame_str) const {
    visualization_msgs::MarkerArray markers;

    const int isArrow = 0;

    visualization_msgs::Marker marker;
    visualization_msgs::Marker markerArrow;
    if (!isArrow)
    {
        marker.color.r = 1.0;
        marker.color.a = 1.0;
        marker.pose.position.x = x_pos;
        marker.pose.position.y = y_pos;
        marker.pose.orientation.w = 1.0;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.id = 0;
        marker.scale.x = 0.5;
        marker.scale.y = 0.5;
        marker.scale.z = 0.5;
        marker.header.stamp = ros::Time::now();
        marker.header.frame_id = frame_str;
        markers.markers.push_back(marker);

        marker.pose.position.x = x_next;
        marker.pose.position.y = y_next;
        marker.color.b = 1.0;
        marker.id = 1;
        markers.markers.push_back(marker);
    }
    else
    {
        markerArrow.action = visualization_msgs::Marker::ADD;
        markerArrow.type = visualization_msgs::Marker::ARROW;
        markerArrow.header.stamp = ros::Time::now();
        markerArrow.header.frame_id = frame_str;
        markerArrow.scale.x = 0.25;
        markerArrow.scale.y = 0.4;
        markerArrow.scale.z = 0.5;
        markerArrow.color.r = 1.0f;
        markerArrow.color.a = 1.0;
        geometry_msgs::Point p1, p2;
        p1.x = x_pos;
        p1.y = y_pos;
        p1.z = 0;
        p2.x = x_next;
        p2.y = y_next;
        p2.z = 0;
        markerArrow.points.push_back(p1);
        markerArrow.points.push_back(p2);
        markers.markers.push_back(markerArrow);
    }
    return markers;
}
}