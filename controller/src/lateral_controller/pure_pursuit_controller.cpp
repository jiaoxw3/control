/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#include "lateral_controller/pure_pursuit_controller.hpp"

namespace ns_controller{

double PurePursuitController::createLocalSteeringControlCommand(double older_steering_command) {
    int trajectory_length = 0;
    int i_increase_index, next_index = 0;
    double dist, look_ahead_d;
    double Vx = rear_axis_center_state_.car_state_dt.car_state_dt.x;
    double look_forward_distance = look_forward_distance_local_ + look_forward_k_local_ * Vx;

    trajectory_length = local_trajectory_.poses.size();

    std::cout << "\nwe are doing PurePursuitController::createLocalSteeringControlCommand" 
    << "\nlook_forward_distance = " << "look_forward_distance_local: " << look_forward_distance_local_ << " + " << 
    "look_forward_k_local: " << look_forward_k_local_ << " * " << "Vx: " << Vx << " = " << look_forward_distance
    << "\nsteering_p_local: " << steering_p_local_ << std::endl;

    if (trajectory_length <= 0){
        std::cout << "the trajectory is empty, delta = 0" << std::endl;
        return delta_ = 0;
    }
    // use path resolution to calculate i_increase_index
    i_increase_index = ceil(look_forward_distance / path_resolution_);
    // use real lenght to calculate i_increase_index
    // double temp_look_ahead_d = 0.0;
    // int temp_next_index = 0;
    // while (temp_look_ahead_d < look_forward_distance){
    //     ++i_increase_index;
    //     temp_next_index = index_ + i_increase_index;
    //     if (temp_next_index >= trajectory_length - 1){
    //         std::cout << "has searched the whole local trajectory!" << std::endl;
    //         break;
    //     }
    //     temp_look_ahead_d = std::hypot(local_trajectory_.poses[temp_next_index].pose.position.y, local_trajectory_.poses[temp_next_index].pose.position.x + lidar2rearwheel_);
    // }

    next_index = std::min(index_ + i_increase_index, trajectory_length - 1);

    // 1.calculate lookahead distance
    look_ahead_d = std::hypot(local_trajectory_.poses[next_index].pose.position.y, local_trajectory_.poses[next_index].pose.position.x + lidar2rearwheel_);

    // 2.beta_est and alpha
    beta_est_ = older_steering_command * 0.5;
    alpha_ = std::atan2(local_trajectory_.poses[next_index].pose.position.y, local_trajectory_.poses[next_index].pose.position.x + lidar2rearwheel_) - 0 * beta_est_;

    // 3.delta
    delta_ = steering_p_local_ * std::atan(2.0 * wheel_base_ * std::sin(alpha_) / look_ahead_d); 

    std::cout << "trajectory_length: " << trajectory_length
    << "\nindex: " << index_
    << "\nnext_index: " << next_index 
    << "\nreal look forward distance: " << look_ahead_d
    << "\nthe CURRENT car state \"/world\" : [" << state_.car_state.x << ", " << state_.car_state.y << ", " << state_.car_state.theta << "]" 
    << "\nthe REARAXIS point \"/world\" : [" <<  rear_axis_center_state_.car_state.x << ", " << rear_axis_center_state_.car_state.y << ", " << rear_axis_center_state_.car_state.theta << "]"
    << "\nthe CLOSEST point \"/rslidar\": [" <<  local_trajectory_.poses[index_].pose.position.x << ", " << local_trajectory_.poses[index_].pose.position.y << ", " << local_trajectory_.poses[index_].pose.orientation.z << "]"
    << "\nthe NEXT point \"/rslidar\" : [" <<  local_trajectory_.poses[next_index].pose.position.x << ", " << local_trajectory_.poses[next_index].pose.position.y << ", " << local_trajectory_.poses[next_index].pose.orientation.z << "]" << std::endl;
    
    //visualize
    // FOR FSSIM ONLY
    // markers_ = createMarkers(local_trajectory_.poses[index_].pose.position.x , local_trajectory_.poses[index_].pose.position.y, local_trajectory_.poses[next_index].pose.position.x , local_trajectory_.poses[next_index].pose.position.y, "fssim/vehicle/front_center_lidar_link");
    markers_ = createMarkers(local_trajectory_.poses[index_].pose.position.x , local_trajectory_.poses[index_].pose.position.y, local_trajectory_.poses[next_index].pose.position.x , local_trajectory_.poses[next_index].pose.position.y, "/rslidar");
    
    return delta_;
}

double PurePursuitController::createGlobalSteeringControlCommand(double older_steering_command) {
    int i_increase_index, i_next;
    int trajectory_length = 0;
    double length;
    double Vx = rear_axis_center_state_.car_state_dt.car_state_dt.x;
    double look_forward_distance = look_forward_distance_global_ + look_forward_k_global_ * Vx;

    trajectory_length = global_trajectory_.poses.size();

    std::cout << "\nwe are doing PurePursuitController::createGlobalSteeringControlCommand" 
    << "\nlook_forward_distance = " << "look_forward_distance_global: " << look_forward_distance_global_ << " + " << 
    "look_forward_k_global: " << look_forward_k_global_ << " * " << "Vx: " << Vx << " = " << look_forward_distance
    << "\nsteering_p_global: " << steering_p_global_ << std::endl;

    if (trajectory_length <= 0){
        std::cout << "the trajectory is empty, delta = 0" << std::endl;
        return delta_ = 0;
    }

    // use path resolution to calculate i_increase_index
    i_increase_index = ceil(look_forward_distance / path_resolution_);
    // use real lenght to calculate i_increase_index
    // double temp_look_ahead_d = 0.0;
    // int temp_next_index = 0;
    // while (temp_look_ahead_d < look_forward_distance){
    //     ++i_increase_index;
    //     temp_next_index = (index_ + i_increase_index) % trajectory_length;
    //     temp_look_ahead_d = std::hypot(global_trajectory_.poses[temp_next_index].pose.position.y - rear_axis_center_state_.car_state.y, global_trajectory_.poses[temp_next_index].pose.position.x - rear_axis_center_state_.car_state.x);
    // }

    i_next = (index_ + i_increase_index) % trajectory_length;
    geometry_msgs::Pose next_point = global_trajectory_.poses[i_next].pose;
    // 0.calculate errors
    if (((rear_axis_center_state_.car_state.y - global_trajectory_.poses[index_].pose.position.y) * cos(rear_axis_center_state_.car_state.theta) - (rear_axis_center_state_.car_state.x - global_trajectory_.poses[index_].pose.position.x) * sin(rear_axis_center_state_.car_state.theta)) < 0)
        lat_error_rear_axis_ = fabs(sqrt((rear_axis_center_state_.car_state.x - global_trajectory_.poses[index_].pose.position.x) * (rear_axis_center_state_.car_state.x - global_trajectory_.poses[index_].pose.position.x) + (rear_axis_center_state_.car_state.y - global_trajectory_.poses[index_].pose.position.y) * (rear_axis_center_state_.car_state.y - global_trajectory_.poses[index_].pose.position.y)));
    else
        lat_error_rear_axis_ = -fabs(sqrt((rear_axis_center_state_.car_state.x - global_trajectory_.poses[index_].pose.position.x) * (rear_axis_center_state_.car_state.x - global_trajectory_.poses[index_].pose.position.x) + (rear_axis_center_state_.car_state.y - global_trajectory_.poses[index_].pose.position.y) * (rear_axis_center_state_.car_state.y - global_trajectory_.poses[index_].pose.position.y)));

    yaw_error_rear_axis_ = global_trajectory_.poses[index_].pose.orientation.z - rear_axis_center_state_.car_state.theta;
    if (yaw_error_rear_axis_ > M_PI)
        yaw_error_rear_axis_ -= 2 * M_PI;
    if (yaw_error_rear_axis_ < -1 * M_PI)
        yaw_error_rear_axis_ += 2 * M_PI;

    // 1.calculate lookahead distance
    length    = std::hypot(next_point.position.y - rear_axis_center_state_.car_state.y, next_point.position.x - rear_axis_center_state_.car_state.x);

    // 2.beta_est and alpha
    beta_est_ = older_steering_command * 0.5;
    alpha_    = std::atan2(next_point.position.y - rear_axis_center_state_.car_state.y, next_point.position.x - rear_axis_center_state_.car_state.x) - (rear_axis_center_state_.car_state.theta + 0 * beta_est_);

    // 3.delta
    delta_ = steering_p_global_ * std::atan(2.0 * wheel_base_ * std::sin(alpha_) / length);

    std::cout << "trajectory_length: " << trajectory_length
    << "\nindex: " << index_
    << "\ni_next: " << i_next 
    << "\nreal look forward distance: " << length
    << "\nthe CURRENT car state \"/world\" : [" << state_.car_state.x << ", " << state_.car_state.y << ", " << state_.car_state.theta << "]" 
    << "\nthe REARAXIS point \"/world\" : [" <<  rear_axis_center_state_.car_state.x << ", " << rear_axis_center_state_.car_state.y << ", " << rear_axis_center_state_.car_state.theta << "]"
    << "\nthe CLOSEST point  \"/world\" : [" <<  global_trajectory_.poses[index_].pose.position.x << ", " << global_trajectory_.poses[index_].pose.position.y << ", " <<global_trajectory_.poses[index_].pose.orientation.z << "]"
    << "\nthe NEXT point \"/world\" : [" <<  next_point.position.x << ", " << next_point.position.y << ", " << next_point.orientation.z  << "]" << std::endl;

    //visualize
    // FOR FSSIM ONLY
    // markers_ = createMarkers(global_trajectory_.poses[index_].pose.position.x , global_trajectory_.poses[index_].pose.position.y, next_point.position.x , next_point.position.y, "/map");
    markers_ = createMarkers(global_trajectory_.poses[index_].pose.position.x , global_trajectory_.poses[index_].pose.position.y, next_point.position.x , next_point.position.y, "/world");

    return delta_;
}

//visualization
visualization_msgs::MarkerArray PurePursuitController::createMarkers(double x_pos, double y_pos, double x_next, double y_next, std::string frame_str) const {
    visualization_msgs::MarkerArray markers;

    const int isArrow = 0;

    visualization_msgs::Marker marker;
    visualization_msgs::Marker markerArrow;
    if (!isArrow)
    {
        marker.color.r = 1.0;
        marker.color.a = 1.0;
        marker.pose.position.x = x_pos;
        marker.pose.position.y = y_pos;
        marker.pose.orientation.w = 1.0;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.id = 0;
        marker.scale.x = 0.5;
        marker.scale.y = 0.5;
        marker.scale.z = 0.5;
        marker.header.stamp = ros::Time::now();
        marker.header.frame_id = frame_str;
        markers.markers.push_back(marker);

        marker.pose.position.x = x_next;
        marker.pose.position.y = y_next;
        marker.color.b = 1.0;
        marker.id = 1;
        markers.markers.push_back(marker);
    }
    else
    {
        markerArrow.action = visualization_msgs::Marker::ADD;
        markerArrow.type = visualization_msgs::Marker::ARROW;
        markerArrow.header.stamp = ros::Time::now();
        markerArrow.header.frame_id = frame_str;
        markerArrow.scale.x = 0.25;
        markerArrow.scale.y = 0.4;
        markerArrow.scale.z = 0.5;
        markerArrow.color.r = 1.0f;
        markerArrow.color.a = 1.0;
        geometry_msgs::Point p1, p2;
        p1.x = x_pos;
        p1.y = y_pos;
        p1.z = 0;
        p2.x = x_next;
        p2.y = y_next;
        p2.z = 0;
        markerArrow.points.push_back(p1);
        markerArrow.points.push_back(p2);
        markers.markers.push_back(markerArrow);
    }
    return markers;
}

} //ns_controller