/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#include "lateral_controller/ribbon_controller.hpp"

namespace ns_controller{

int RibbonController::find_nearCone(fsd_common_msgs::ConeDetections ConeDetections)
{
    int index=-1;
    double f_theta;
    double f_length;
    double min_length;
    min_length=std::numeric_limits<double>::max();
    for(int i=0;i<ConeDetections.cone_detections.size();i++)
    {    
        f_theta=front_axis_center_state_.car_state.theta-std::atan2(front_axis_center_state_.car_state.y-ConeDetections.cone_detections[i].position.y,front_axis_center_state_.car_state.x-ConeDetections.cone_detections[i].position.x)-M_PI;
        if(std::cos(f_theta)>0)
        {
            f_length=std::hypot(front_axis_center_state_.car_state.y-ConeDetections.cone_detections[i].position.y,front_axis_center_state_.car_state.x-ConeDetections.cone_detections[i].position.x);
            if(f_length<min_length)
            {
                min_length=f_length;
                index=i;  
            }
            
        }
    }
    
    if (index==-1)
    {
        for(int i=0;i<ConeDetections.cone_detections.size();i++)
    {    
        f_theta=front_axis_center_state_.car_state.theta-std::atan2(front_axis_center_state_.car_state.y-ConeDetections.cone_detections[i].position.y,front_axis_center_state_.car_state.x-ConeDetections.cone_detections[i].position.x)-M_PI;
        if(std::cos(f_theta)<=0)
        {
            f_length=std::hypot(front_axis_center_state_.car_state.y-ConeDetections.cone_detections[i].position.y,front_axis_center_state_.car_state.x-ConeDetections.cone_detections[i].position.x);
            if(f_length<min_length)
            {
                min_length=f_length;
                index=i+1;
            }
            
        }
    }
    }


    // for(int i=0;i<ConeDetections.cone_detections.size();i++)
    // {    
    //     //f_theta=front_axis_center_state_.car_state.theta-std::atan2(front_axis_center_state_.car_state.y-ConeDetections.cone_detections[i].position.y,front_axis_center_state_.car_state.x-ConeDetections.cone_detections[i].position.x)-M_PI;    
    //     f_length=std::hypot(front_axis_center_state_.car_state.y-ConeDetections.cone_detections[i].position.y,front_axis_center_state_.car_state.x-ConeDetections.cone_detections[i].position.x);
    //     if(f_length<min_length)
    //     {
    //         min_length=f_length;
    //         index=i;  
    //     }
            
        
    // }
    
    return index;
}




void RibbonController::find_angle_rangle(fsd_common_msgs::Cone Cone_left,fsd_common_msgs::Cone Cone_right)
{
    double fl_theta;
    double fl_length;
    double rl_theta;
    double rl_length;
    double fr_theta;
    double fr_length;
    double rr_theta;
    double rr_length;
    double fl_delta;
    double fr_delta;
    double rl_delta;
    double rr_delta;

    // std::cout << " Cone_left.position.x: " << Cone_left.position.x 
    //     << " Cone_left.position.y: " << Cone_left.position.y 
    //     << "\n Cone_right.position.x: " << Cone_right.position.x
    //     << " Cone_right.position.y: " << Cone_right.position.y
    //     << "\n front_axis_center_state_.car_state.x: " << front_axis_center_state_.car_state.x
    //     << " front_axis_center_state_.car_state.y: " << front_axis_center_state_.car_state.y
    //     << "\n rear_axis_center_state_.car_state.x: " << rear_axis_center_state_.car_state.x
    //     << " rear_axis_center_state_.car_state.y: " << rear_axis_center_state_.car_state.y
    //     << "\n front_axis_center_state_.car_state.theta: " << front_axis_center_state_.car_state.theta   
    //     << std::endl;
    // fl_theta=std::atan2(Cone_left.position.y-front_axis_center_state_.car_state.y,Cone_left.position.x-front_axis_center_state_.car_state.x)-front_axis_center_state_.car_state.theta;
    // fl_length=std::hypot(Cone_left.position.y-front_axis_center_state_.car_state.y,Cone_left.position.x-front_axis_center_state_.car_state.x);
    // rl_theta=std::atan2(Cone_left.position.y-rear_axis_center_state_.car_state.y,Cone_left.position.x-rear_axis_center_state_.car_state.x)-rear_axis_center_state_.car_state.theta;
    // rl_length=std::hypot(Cone_left.position.y-rear_axis_center_state_.car_state.y,Cone_left.position.x-rear_axis_center_state_.car_state.x);
    // fr_theta=std::atan2(Cone_right.position.y-front_axis_center_state_.car_state.y,Cone_right.position.x-front_axis_center_state_.car_state.x)-front_axis_center_state_.car_state.theta;
    // fr_length=std::hypot(Cone_right.position.y-front_axis_center_state_.car_state.y,Cone_right.position.x-front_axis_center_state_.car_state.x);
    // rr_theta=std::atan2(Cone_right.position.y-rear_axis_center_state_.car_state.y,Cone_right.position.x-rear_axis_center_state_.car_state.x)-rear_axis_center_state_.car_state.theta;
    // rr_length=std::hypot(Cone_right.position.y-rear_axis_center_state_.car_state.y,Cone_right.position.x-rear_axis_center_state_.car_state.x);
    fl_theta=std::atan2(Cone_left.position.y-front_axis_center_state_.car_state.y-car_width/2*std::cos(front_axis_center_state_.car_state.theta),Cone_left.position.x-front_axis_center_state_.car_state.x+car_width/2*std::sin(front_axis_center_state_.car_state.theta))-front_axis_center_state_.car_state.theta;
    fl_length=std::hypot(Cone_left.position.y-front_axis_center_state_.car_state.y-car_width/2*std::cos(front_axis_center_state_.car_state.theta),Cone_left.position.x-front_axis_center_state_.car_state.x+car_width/2*std::sin(front_axis_center_state_.car_state.theta));
    rl_theta=std::atan2(Cone_left.position.y-rear_axis_center_state_.car_state.y-car_width/2*std::cos(rear_axis_center_state_.car_state.theta),Cone_left.position.x-rear_axis_center_state_.car_state.x+car_width/2*std::sin(rear_axis_center_state_.car_state.theta))-rear_axis_center_state_.car_state.theta;
    rl_length=std::hypot(Cone_left.position.y-rear_axis_center_state_.car_state.y-car_width/2*std::cos(rear_axis_center_state_.car_state.theta),Cone_left.position.x-rear_axis_center_state_.car_state.x+car_width/2*std::sin(rear_axis_center_state_.car_state.theta));
    fr_theta=std::atan2(Cone_right.position.y-front_axis_center_state_.car_state.y+car_width/2*std::cos(front_axis_center_state_.car_state.theta),Cone_right.position.x-front_axis_center_state_.car_state.x-car_width/2*std::sin(front_axis_center_state_.car_state.theta))-front_axis_center_state_.car_state.theta;
    fr_length=std::hypot(Cone_right.position.y-front_axis_center_state_.car_state.y+car_width/2*std::cos(front_axis_center_state_.car_state.theta),Cone_right.position.x-front_axis_center_state_.car_state.x-car_width/2*std::sin(front_axis_center_state_.car_state.theta));
    rr_theta=std::atan2(Cone_right.position.y-rear_axis_center_state_.car_state.y+car_width/2*std::cos(rear_axis_center_state_.car_state.theta),Cone_right.position.x-rear_axis_center_state_.car_state.x-car_width/2*std::sin(rear_axis_center_state_.car_state.theta))-rear_axis_center_state_.car_state.theta;
    rr_length=std::hypot(Cone_right.position.y-rear_axis_center_state_.car_state.y+car_width/2*std::cos(rear_axis_center_state_.car_state.theta),Cone_right.position.x-rear_axis_center_state_.car_state.x-car_width/2*std::sin(rear_axis_center_state_.car_state.theta));
    // std::cout 
    //     << " fl_theta: " << fl_theta 
    //     << " fr_theta: " << fr_theta
    //     << " rl_theta: " << rl_theta
    //     << " rr_theta: " << rr_theta
    //     << " fl_length: " << fl_length 
    //     << " fr_length: " << fr_length
    //     << " rl_length: " << rl_length
    //     << " rr_length: " << rr_length      
    //     << std::endl;
    rl_delta=std::atan(wheel_base_/((rl_length)/2/std::sin(rl_theta)));
    rr_delta=std::atan(wheel_base_/((rr_length)/2/std::sin(rr_theta)));
    fl_delta=std::atan(wheel_base_/((fl_length/2+wheel_base_*std::cos(fl_theta))/(std::sin(fl_theta))));
    fr_delta=std::atan(wheel_base_/((fr_length/2+wheel_base_*std::cos(fr_theta))/(std::sin(fr_theta))));

    if(fl_delta<rl_delta){temp_l_delta_=fl_delta;}
    else{temp_l_delta_=rl_delta;}
    if(fr_delta>rr_delta){temp_r_delta_=fr_delta;}
    else{temp_r_delta_=rr_delta;}
    std::cout 
        << " temp_l_delta_ " << temp_l_delta_
        << " temp_r_delta_ " << temp_r_delta_
        // << " fl_delta " << fl_delta 
        // << " rl_delta "<< rl_delta
        // << " fr_delta " << fr_delta
        // << " rr_delta " << rr_delta
        << std::endl;
}


double RibbonController::createSpeedFromControlCommmand()
{
    std::cout << "\n--------------------------------------------------------------"
    << "\nwe are testing RibbonController::createSpeedFromControlCommmand"
    << "\nribbon_max_speed_: " << ribbon_max_speed_ << std::endl;

    if (lenLeft_Conedetections<=0 or lenRight_Conedetections<=0){
        std::cout << "the ConeDetections_left_ or ConeDetections_right_ is empty, speed = 0" << std::endl;
        return speed_ = 0;
    }

    

    double v = std::hypot(front_axis_center_state_.car_state_dt.car_state_dt.x,front_axis_center_state_.car_state_dt.car_state_dt.y);
    
    // this is just a simple speed local planning for test
    
    // make the input: delta as smooth as possible
    old_steering_command_set.push_back(abs(delta_));
    old_steering_command_set.erase(old_steering_command_set.begin());
    double temp = 0.0;
    for (int i = 0 ; i < old_steering_command_set.size() ; ++i){
        temp += old_steering_command_set[i];
    }
    double delta =  temp / old_steering_command_set.size();
    double view_distance = std::hypot(ConeDetections_left_.cone_detections[l_index_+angle_index].position.y/2+ConeDetections_right_.cone_detections[r_index_+angle_index].position.y/2-rear_axis_center_state_.car_state.y,ConeDetections_left_.cone_detections[l_index_+angle_index].position.x/2+ConeDetections_right_.cone_detections[r_index_+angle_index].position.x/2-rear_axis_center_state_.car_state.x);

    circle_speed_=sqrt(gra_acc*car_width/(2*delta+0.001*gra_acc*car_width));
    brake_speed_=sqrt(2*max_brake_a*view_distance);
    
    std::cout << " delta:" << delta << " view_distance:" << view_distance
    << "\ncircle_speed_: " << circle_speed_k*circle_speed_ << " brake_speed_: "  << brake_speed_k*brake_speed_ << std::endl;

    target_speed_=std::min(circle_speed_k*circle_speed_,brake_speed_k*brake_speed_);
    target_speed_=std::min(ribbon_max_speed_,target_speed_);
    speed_ = v + speed_k_ * (target_speed_ - v);
    // limit the speed command
    speed_ = std::min(speed_, target_speed_ + 1);

    // FOR FSSSIM ONLY
    // speed_ = 0.1 * (target_speed_ - v);
    speed_=std::min(5.,speed_);
    std::cout << " target_speed_: "<< target_speed_ << " speed_: "<< speed_ << " v: " << v << std::endl;
    return speed_;
}

double RibbonController::createGlobalSteeringControlCommand(double older_steering_command) {
    
    double center_rear_theta;
    double center_rear_length;
    
    l_delta_=M_PI/2;
    r_delta_=-M_PI/2;
    lenLeft_Conedetections=ConeDetections_left_.cone_detections.size();
    lenRight_Conedetections=ConeDetections_right_.cone_detections.size();
    
    


    std::cout << "\nwe are doing RibbonController::createGlobalSteeringControlCommand" << std::endl;

 
    if (lenLeft_Conedetections<=0 or lenRight_Conedetections<=0){
        std::cout << "the ConeDetections_left_ or ConeDetections_right_ is empty, delta = 0" << std::endl;
        return delta_ = 0;
    }

    std::cout << "find index " << std::endl;
    // 0.find index
    
    l_index_=find_nearCone(ConeDetections_left_);
    r_index_=find_nearCone(ConeDetections_right_);
    int l_index=l_index_;
    int r_index=r_index_;

    std::cout << "find index end" << std::endl;

    std::cout << "l_index_:" << l_index_ << " r_index_:" << r_index_ <<std::endl;
    std::cout << "find angle rangle  " << std::endl;
    // 1.find angle rangle 
    for(int i=0;i<std::min(lenLeft_Conedetections,lenRight_Conedetections);i++)
    {
        angle_index=i;
        if(l_index_+i>=lenLeft_Conedetections){l_index_-=lenLeft_Conedetections;}
        if(r_index_+i>=lenRight_Conedetections){r_index_-=lenRight_Conedetections;}
        
        find_angle_rangle(ConeDetections_left_.cone_detections[l_index_+i],ConeDetections_right_.cone_detections[r_index_+i]);
        
        if(i==0)
        {
            center_rear_theta=std::atan2(ConeDetections_left_.cone_detections[l_index_+i].position.y/2+ConeDetections_right_.cone_detections[r_index_+i].position.y/2-rear_axis_center_state_.car_state.y,ConeDetections_left_.cone_detections[l_index_+i].position.x/2+ConeDetections_right_.cone_detections[r_index_+i].position.x/2-rear_axis_center_state_.car_state.x)-rear_axis_center_state_.car_state.theta;
            center_rear_length=std::hypot(ConeDetections_left_.cone_detections[l_index_+i].position.y/2+ConeDetections_right_.cone_detections[r_index_+i].position.y/2-rear_axis_center_state_.car_state.y,ConeDetections_left_.cone_detections[l_index_+i].position.x/2+ConeDetections_right_.cone_detections[r_index_+i].position.x/2-rear_axis_center_state_.car_state.x);
            center_deflection_angle_=std::atan(wheel_base_/((center_rear_length)/2/std::sin(center_rear_theta)));
            //center_deflection_angle_=std::atan(wheel_base_/((center_rear_length/2+wheel_base_*std::cos(center_rear_theta))/(std::sin(center_rear_theta))));
        }     
        if(((temp_l_delta_<=l_delta_ and temp_l_delta_>=r_delta_) or (temp_r_delta_>=r_delta_ and temp_r_delta_<=l_delta_)) and temp_l_delta_>temp_r_delta_)
        {
            if(temp_l_delta_<=l_delta_ and temp_l_delta_>=r_delta_){l_delta_= temp_l_delta_;}
            if(temp_r_delta_>=r_delta_ and temp_r_delta_<=l_delta_){r_delta_= temp_r_delta_;}
            //std::cout << "l_delta_:" << l_delta_ << " r_delta_:" << r_delta_ <<std::endl;     
        }        
        else
        {
            //  if(temp_l_delta_<0 and temp_r_delta_<0)
            // {
            //     temp_l_delta_=temp_r_delta_=-M_PI/2;
            // }
            // if(temp_l_delta_>0 and temp_r_delta_>0)
            // {
            //     temp_l_delta_=temp_r_delta_=M_PI/2;
            // }
            l_delta_=temp_l_delta_*1./(i+1.)+l_delta_*i/(i+1.);
            r_delta_=temp_r_delta_*1./(i+1.)+r_delta_*i/(i+1.);
            break;
        }
    }
    std::cout << "find angle rangle  end" << std::endl;
    // 2.delta
    //delta_=center_deflection_angle_/4+(l_delta_+r_delta_)/2*3/4;
    if(l_delta_>0 and r_delta_<0)
    {
        delta_=l_delta_*l_delta_/(l_delta_-r_delta_)-r_delta_*r_delta_/(l_delta_-r_delta_);
    }
    else if(l_delta_>0 and r_delta_>0)
    {
        delta_=l_delta_;
    }
    else if(l_delta_<0 and r_delta_<0)
    {
        delta_=r_delta_;
    }
    else
    {
        delta_=(l_delta_+r_delta_)/2;
    }
    delta_=center_deflection_angle_/4+delta_*3/4;

    std::cout<< "\ndelta_: " << delta_
    << "\nv^2/R " << (std::pow(std::hypot(front_axis_center_state_.car_state_dt.car_state_dt.x,front_axis_center_state_.car_state_dt.car_state_dt.y),2)*std::tan(delta_)/(wheel_base_))
    << std::endl;

    
    delta_=delta_ + 0.05*1./6.*(std::pow(std::hypot(front_axis_center_state_.car_state_dt.car_state_dt.x,front_axis_center_state_.car_state_dt.car_state_dt.y),2)*std::tan(delta_)/(wheel_base_));
    
    delta_=2*delta_;

    markers_ = createMarkers(ConeDetections_left_.cone_detections[l_index].position.x , ConeDetections_left_.cone_detections[l_index].position.y, ConeDetections_right_.cone_detections[r_index].position.x , ConeDetections_right_.cone_detections[r_index].position.y,ConeDetections_left_.cone_detections[l_index_+angle_index].position.x , ConeDetections_left_.cone_detections[l_index_+angle_index].position.y, ConeDetections_right_.cone_detections[r_index_+angle_index].position.x , ConeDetections_right_.cone_detections[r_index_+angle_index].position.y, "/map");


    std::cout << "left_index: " << l_index_
    << "\nright_index: " << r_index_
    << "\nlenLeft_Conedetections " << lenLeft_Conedetections
    << "\nlenRight_Conedetections " <<lenRight_Conedetections
    << "\nangle_index: " << angle_index
    << "\nl_delta_: " << l_delta_
    << "\nr_delta_: " << r_delta_
    << "\ncenter_deflection_angle_ " << center_deflection_angle_
    << "\ndelta_: " << delta_
    << std::endl;

    return delta_;
}

visualization_msgs::MarkerArray RibbonController::createMarkers(double x_l, double y_l, double x_r, double y_r, double x_l2, double y_l2, double x_r2, double y_r2,std::string frame_str) const {
    visualization_msgs::MarkerArray markers;

    const int isArrow = 0;

    visualization_msgs::Marker marker;
    visualization_msgs::Marker markerArrow;
    if (!isArrow)
    {
        marker.color.r = 1.0;
        marker.color.a = 1.0;
        marker.color.b = 1.0;
        marker.pose.position.x = front_axis_center_state_.car_state.x-car_width/2*std::sin(front_axis_center_state_.car_state.theta);
        marker.pose.position.y = front_axis_center_state_.car_state.y+car_width/2*std::cos(front_axis_center_state_.car_state.theta);
        marker.pose.orientation.w = 1.0;
        marker.type = visualization_msgs::Marker::SPHERE;
        marker.action = visualization_msgs::Marker::ADD;
        marker.id = 0;
        marker.scale.x = 0.5;
        marker.scale.y = 0.5;
        marker.scale.z = 0.5;
        marker.header.stamp = ros::Time::now();
        marker.header.frame_id = frame_str;
        markers.markers.push_back(marker);

        marker.pose.position.x = rear_axis_center_state_.car_state.x-car_width/2*std::sin(rear_axis_center_state_.car_state.theta);
        marker.pose.position.y = rear_axis_center_state_.car_state.y+car_width/2*std::cos(rear_axis_center_state_.car_state.theta);
        marker.color.r = 1.0;
        marker.color.a = 1.0;
        marker.color.b = 1.0;
        marker.id = 1;
        markers.markers.push_back(marker);

        marker.pose.position.x = front_axis_center_state_.car_state.x+car_width/2*std::sin(front_axis_center_state_.car_state.theta);
        marker.pose.position.y = front_axis_center_state_.car_state.y-car_width/2*std::cos(front_axis_center_state_.car_state.theta);
        marker.color.r = 1.0;
        marker.color.a = 1.0;
        marker.color.b = 1.0;
        marker.id = 2;
        markers.markers.push_back(marker); 

        marker.pose.position.x = rear_axis_center_state_.car_state.x+car_width/2*std::sin(rear_axis_center_state_.car_state.theta);
        marker.pose.position.y = rear_axis_center_state_.car_state.y-car_width/2*std::cos(rear_axis_center_state_.car_state.theta);
        marker.color.r = 1.0;
        marker.color.a = 1.0;
        marker.color.b = 1.0;
        marker.id = 3;
        markers.markers.push_back(marker); 

        marker.pose.position.x = x_l;
        marker.pose.position.y = y_l;
        marker.color.r = 0;
        marker.color.a = 1.0;
        marker.color.b = 0;
        marker.color.g = 1.0;
        marker.id = 4;
        markers.markers.push_back(marker);        

        marker.pose.position.x = x_r;
        marker.pose.position.y = y_r;
        marker.color.r = 0;
        marker.color.a = 1.0;
        marker.color.b = 0;
        marker.color.g = 1.0;
        marker.id = 5;
        markers.markers.push_back(marker);

        marker.pose.position.x = x_l2;
        marker.pose.position.y = y_l2;
        marker.color.r = 0;
        marker.color.a = 1.0;
        marker.color.b = 0;
        marker.color.g = 0;
        marker.id = 6;
        markers.markers.push_back(marker);

        marker.pose.position.x = x_r2;
        marker.pose.position.y = y_r2;
        marker.color.r = 0;
        marker.color.a = 1.0;
        marker.color.b = 0;
        marker.color.g = 0;
        marker.id = 7;
        markers.markers.push_back(marker);
    }
    else
    {
        markerArrow.action = visualization_msgs::Marker::ADD;
        markerArrow.type = visualization_msgs::Marker::ARROW;
        markerArrow.header.stamp = ros::Time::now();
        markerArrow.header.frame_id = frame_str;
        markerArrow.scale.x = 0.25;
        markerArrow.scale.y = 0.4;
        markerArrow.scale.z = 0.5;
        markerArrow.color.r = 1.0f;
        markerArrow.color.a = 1.0;
        geometry_msgs::Point p1, p2,p3,p4;
        p1.x = x_l;
        p1.y = y_l;
        p1.z = 0;
        p2.x = x_r;
        p2.y = y_r;
        p2.z = 0;
        p3.x = x_l2;
        p3.y = y_l2;
        p3.z = 0;
        p4.x = x_r2;
        p4.y = y_r2;
        p4.z = 0;
        

        markerArrow.points.push_back(p1);
        markerArrow.points.push_back(p2);
        markerArrow.points.push_back(p3);
        markerArrow.points.push_back(p4);
        markers.markers.push_back(markerArrow);
    }
    return markers;
}
    
    





} //ns_controller