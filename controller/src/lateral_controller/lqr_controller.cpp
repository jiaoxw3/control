/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#include "lateral_controller/lqr_controller.hpp"

namespace ns_controller{

double LqrController::createLocalSteeringControlCommand() {
    double look_forward_distance = look_forward_distance_local_;
    std::cout << "\nwe are doing LqrController::createLocalSteeringControlCommand" 
    << "\nTs: " << Ts_ << "   ref_k_limit: " << ref_k_limit_  << "   lqr_max_iteration: " << lqr_max_iteration_  << "\nlqr_tolerance: " 
    << lqr_tolerance_ << "   look_forward_distance_local: " << look_forward_distance_local_ << "   feedforward_distance_local_: " << feedforward_distance_local_ << std::endl;

    fsd_common_msgs::CarState state = state_;
    state.car_state.x = -(lidar2rearwheel_ - lr_); //局部路径跟踪情况下，以/rslidar坐标系为基准
    state.car_state.y = 0;
    state.car_state.theta = 0;

    nav_msgs::Path trajectory = local_trajectory_;
    int trajectory_length = trajectory.poses.size();

    //0.return 0, when there is no trajectory
    if (trajectory_length <= 0){
        std::cout << "the trajectory is empty, delta = 0" << std::endl;
        return delta_ = 0;
    }
    int increase_index = ceil(look_forward_distance / path_resolution_);
    int index = (index_ + increase_index) % trajectory_length;

    //1.更新误差矩阵，matrix_state_
    updateState(index, state, trajectory);

    //2.计算matrix_ad_和matrix_bd_
    updateMatrix(state);

    //3.计算matrix_adc_和matrix_bdc_
    updateMatrixCompound();

    //4.求解LQR，解得K矩阵，即matrix_k_
    math::SolveLQRProblem(matrix_adc_, matrix_bdc_, matrix_q_local_, matrix_r_local_, lqr_tolerance_, lqr_max_iteration_, &matrix_k_);

    //5.计算反馈控制的转角，feedback = - K * state_
    const double delta_feedback = -(matrix_k_ * matrix_state_)(0, 0);

    //6.计算前馈控制的转角
    int look_ahead_index = ceil(feedforward_distance_local_ / path_resolution_);
    int next_index = (index + look_ahead_index) % trajectory_length; 
    double look_ahead_d = std::hypot(trajectory.poses[next_index].pose.position.y - state.car_state.y, trajectory.poses[next_index].pose.position.x - state.car_state.x);
    const double delta_feedforward = computeFeedForward(next_index, state, trajectory);

    //7.计算delta
    delta_ = delta_feedback + delta_feedforward;
    std::cout << "delta = "<< "delta_feedback: " << delta_feedback << " + "<<"delta_feedforward: " << delta_feedforward << " = " << delta_
    << "\ntrajectory_length: " << trajectory_length
    << "\nindex: " << index_
    << "\nindex + increase_index: " << index
    << "\nnext_index: " << next_index 
    << "\nreal look forward distance: " << look_ahead_d
    << "\nthe CURRENT car state \"/world\" : [" << state_.car_state.x << ", " << state_.car_state.y << ", " << state_.car_state.theta << "]" 
    << "\nthe CLOSEST point \"/rslidar\": [" <<  trajectory.poses[index_].pose.position.x << ", " << trajectory.poses[index_].pose.position.y << ", " << trajectory.poses[index_].pose.orientation.z << ", " << trajectory.poses[index_].pose.orientation.w << "]"
    << "\nthe CLOSEST+increase_index point \"/rslidar\": [" <<  trajectory.poses[index].pose.position.x << ", " << trajectory.poses[index].pose.position.y << ", " << trajectory.poses[index].pose.orientation.z << ", " << trajectory.poses[index].pose.orientation.w << "]"
    << "\nthe NEXT point \"/rslidar\" : [" <<  trajectory.poses[next_index].pose.position.x << ", " << trajectory.poses[next_index].pose.position.y << ", " << trajectory.poses[next_index].pose.orientation.z << ", " << trajectory.poses[next_index].pose.orientation.w << "]" << std::endl;
  
    return delta_;
}

double LqrController::createGlobalSteeringControlCommand(){
    double look_forward_distance = look_forward_distance_global_;
    std::cout << "\nwe are doing LqrController::createGlobalSteeringControlCommand" 
    << "\nTs: " << Ts_ << "   ref_k_limit: " << ref_k_limit_  << "   lqr_max_iteration: " << lqr_max_iteration_  << "\nlqr_tolerance: " 
    << lqr_tolerance_ << "   look_forward_distance_global: " << look_forward_distance_global_ << "   feedforward_distance_global: " << feedforward_distance_global_ << std::endl;

    fsd_common_msgs::CarState state = state_;

    nav_msgs::Path trajectory = global_trajectory_;

    int trajectory_length = trajectory.poses.size();

    //0.return 0, when there is no trajectory
    if (trajectory_length <= 0){
        std::cout << "the trajectory is empty, delta = 0" << std::endl;
        return delta_ = 0;
    }
    int increase_index = ceil(look_forward_distance / path_resolution_);
    int index = (index_ + increase_index) % trajectory_length;

    //1.更新误差矩阵，matrix_state_
    updateState(index, state, trajectory);

    //2.计算matrix_ad_和matrix_bd_
    updateMatrix(state);

    //3.计算matrix_adc_和matrix_bdc_
    updateMatrixCompound();

    //4.求解LQR，解得K矩阵，即matrix_k_
    math::SolveLQRProblem(matrix_adc_, matrix_bdc_, matrix_q_global_, matrix_r_global_, lqr_tolerance_, lqr_max_iteration_, &matrix_k_);

    //5.计算反馈控制的转角，feedback = - K * state_
    const double delta_feedback = -(matrix_k_ * matrix_state_)(0, 0);

    //6.计算前馈控制的转角
    int look_ahead_index = ceil(feedforward_distance_global_ / path_resolution_);
    int next_index = (index + look_ahead_index) % trajectory_length; 
    double look_ahead_d = std::hypot(trajectory.poses[next_index].pose.position.y - state.car_state.y, trajectory.poses[next_index].pose.position.x - state.car_state.x);
    const double delta_feedforward = computeFeedForward(next_index, state, trajectory);

    //7.计算delta
    delta_ = delta_feedback + delta_feedforward;
    std::cout << "delta = "<< "delta_feedback: " << delta_feedback << " + "<<"delta_feedforward: " << delta_feedforward << " = " << delta_
    << "\ntrajectory_length: " << trajectory_length
    << "\nindex: " << index_
    << "\nindex + increase_index: " << index
    << "\nnext_index: " << next_index 
    << "\nreal look forward distance: " << look_ahead_d
    << "\nthe CURRENT car state \"/world\" : [" << state_.car_state.x << ", " << state_.car_state.y << ", " << state_.car_state.theta << "]" 
    << "\nthe CLOSEST point \"/world\": [" <<  trajectory.poses[index_].pose.position.x << ", " << trajectory.poses[index_].pose.position.y << ", " << trajectory.poses[index_].pose.orientation.z << ", " << trajectory.poses[index_].pose.orientation.w << "]"
    << "\nthe CLOSEST+increase_index point \"/world\": [" <<  trajectory.poses[index].pose.position.x << ", " << trajectory.poses[index].pose.position.y << ", " << trajectory.poses[index].pose.orientation.z << ", " << trajectory.poses[index].pose.orientation.w << "]"
    << "\nthe NEXT point \"/worldr\" : [" <<  trajectory.poses[next_index].pose.position.x << ", " << trajectory.poses[next_index].pose.position.y << ", " << trajectory.poses[next_index].pose.orientation.z << ", " << trajectory.poses[next_index].pose.orientation.w << "]" << std::endl;
  
    return delta_;
}

void LqrController::updateState(int index, fsd_common_msgs::CarState state, nav_msgs::Path trajectory) 
{   
    double v = std::max(state.car_state_dt.car_state_dt.x, 0.5);   //set the velocity in every time step
    double ref_k = trajectory.poses[index].pose.orientation.w;     //the curvature of the reference trajectory point
    double ref_yaw = trajectory.poses[index].pose.orientation.z;   //the yaw angle of the reference trajectory point
    double yaw = state.car_state.theta;                            //the measured yaw angle, in fact, we are talking about heading angle, but in many codes, it's common to conflat two parameters
    double yaw_rate = state.car_state_dt.car_state_dt.theta;       //the mearsured yaw rate

    //limit the ref_k
    ref_k = std::min(ref_k, ref_k_limit_);
    ref_k = std::max(ref_k, -ref_k_limit_);

    const double dx = state.car_state.x - trajectory.poses[index].pose.position.x;
    const double dy = state.car_state.y - trajectory.poses[index].pose.position.y;

    double dyaw = yaw - ref_yaw;
    if (dyaw > M_PI)
        dyaw -= 2 * M_PI;
    else if (dyaw < -M_PI)
        dyaw += 2 * M_PI;

    matrix_state_(0, 0) = dy * std::cos(ref_yaw) - dx * std::sin(ref_yaw);
    matrix_state_(2, 0) = dyaw;
    matrix_state_(1, 0) = v * std::sin(matrix_state_(2, 0));
    matrix_state_(3, 0) = yaw_rate - v * ref_k;

    // std::cout << "matrix_state_(0, 0): " << matrix_state_(0, 0) << "  matrix_state_(2, 0): " << matrix_state_(2, 0)
    // <<"  matrix_state_(1, 0): " << matrix_state_(1, 0) << "  matrix_state_(3, 0): " << matrix_state_(3, 0) << std::endl;
}

void LqrController::updateMatrix(fsd_common_msgs::CarState state)
{
    double v = std::max(state.car_state_dt.car_state_dt.x, 0.5);

    matrix_a_coeff_(1, 1) = -(cf_ + cr_) / total_mass_;
    matrix_a_coeff_(1, 3) = (lr_ * cr_ - lf_ * cf_) / total_mass_;
    matrix_a_coeff_(3, 1) = (lr_ * cr_ - lf_ * cf_) / iz_;
    matrix_a_coeff_(3, 3) = -1.0 * (lf_ * lf_ * cf_ + lr_ * lr_ * cr_) / iz_;

    matrix_a_(0, 1) = 1.0;
    matrix_a_(1, 2) = (cf_ + cr_) / total_mass_;
    matrix_a_(2, 3) = 1.0;
    matrix_a_(3, 2) = (lf_ * cf_ - lr_ * cr_) / iz_;
    matrix_a_(0, 2) = 0.0;
    matrix_a_(1, 1) = matrix_a_coeff_(1, 1) / v;
    matrix_a_(1, 3) = matrix_a_coeff_(1, 3) / v;
    matrix_a_(3, 1) = matrix_a_coeff_(3, 1) / v;
    matrix_a_(3, 3) = matrix_a_coeff_(3, 3) / v;
    Eigen::MatrixXd matrix_i = Eigen::MatrixXd::Identity(matrix_a_.cols(), matrix_a_.cols());

    matrix_b_(1, 0) = cf_ / total_mass_;
    matrix_b_(3, 0) = lf_ * cf_ / iz_;

    // 离散化
    matrix_ad_ = (matrix_i - Ts_ * 0.5 * matrix_a_).inverse() * (matrix_i + Ts_ * 0.5 * matrix_a_);
    matrix_bd_ = matrix_b_ * Ts_;
}

void LqrController::updateMatrixCompound()
{
    // Initialize preview matrix
    matrix_adc_.block(0, 0, basic_state_size_, basic_state_size_) = matrix_ad_;
    matrix_bdc_.block(0, 0, basic_state_size_, 1) = matrix_bd_;

    //以下功能未知
    // if (preview_window_ > 0)
    // {
    //     matrix_bdc_(matrix_bdc_.rows() - 1, 0) = 1;
    //     // Update A matrix;
    //     for (int i = 0; i < preview_window_ - 1; ++i)
    //     {
    //         matrix_adc_(basic_state_size_ + i, basic_state_size_ + 1 + i) = 1;
    //     }
    // }
}

double LqrController::computeFeedForward(int &next_index, fsd_common_msgs::CarState state, nav_msgs::Path trajectory) const
{
    auto curvature = trajectory.poses[next_index].pose.orientation.w; //the curvature of the feedforward reference trajectory point
    double v = std::max(state.car_state_dt.car_state_dt.x, 0.5);

    const double kv = lr_ * total_mass_ / 2 / cf_ / wheel_base_ - lf_ * total_mass_ / 2 / cr_ / wheel_base_;

    double delta_feedforward = (wheel_base_ * curvature + kv * v * v * curvature - 
    matrix_k_(0, 2) * (lr_ * curvature - lf_ * total_mass_ * v * v * curvature / 2 / cr_ / wheel_base_));

    return delta_feedforward;
}

}