/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#include "lateral_controller/mpc_controller.hpp"

namespace ns_controller {

double MpcController::createLocalSteeringControlCommand(double older_steering_command) {
    std::cout << "\nwe are doing MpcController::createLocalSteeringControlCommand" 
    << "\nTs: " << Ts_ << "   ref_k_limit:"  << ref_k_limit_ 
    << "\nmat_q1: " << mat_q_local_(0, 0) << "   mat_q2: " << mat_q_local_(1, 1) << "   mat_q3: " << mat_q_local_(2, 2) 
    << "\nmat_q4: " << mat_q_local_(3, 3) << "   mat_r: " << mat_r_local_(0) << std::endl;

    fsd_common_msgs::CarState state = state_;
    state.car_state.x = -(lidar2rearwheel_ - lr_); //局部路径跟踪情况下，以rslidar坐标系为基准
    state.car_state.y = 0;
    state.car_state.theta = 0;
    nav_msgs::Path trajectory = local_trajectory_;

    //0.return 0, when there is no trajectory
    int trajectory_length = trajectory.poses.size();
    if (trajectory_length <= 0){
        std::cout << "the trajectory is empty, delta = 0"<< std::endl;
        return delta_ = 0;
    }

    //1.update index
    int index = index_; 

    //2.update state
    MPC_ErrorVec error_vec = getMPCErrorMatrix(index, trajectory, state);
    
    //3.求Matrix A B C并离散化
    updateMPCMatrixABCBar(index, trajectory, state);
    
    //4.求取总状态
    MpcStateTransMat mpc_lin_mat = getMPCConfusionMatrix();
    
    //5.二次规划矩阵
    QuadProgMat quad_prog_mat = getQuadProgMatrix(mpc_lin_mat, error_vec, mat_q_local_, mat_r_local_);
    
    //6.求解
    int status = -1;
    std::vector<double> du_vec = interface_.solve(quad_prog_mat, &status);

    //7.set the delta
    old_delta_ = older_steering_command;
    delta_ = old_delta_ + du_vec[0];

    std::cout << "delta = "<< "old_delta: " << old_delta_ << " + "<<"du_vec[0]: " << du_vec[0] << " = " << delta_
    << "\ntrajectory_length: " << trajectory_length
    << "\nindex: " << index
    << "\nthe CURRENT car state \"/world\" : [" << state_.car_state.x << ", " << state_.car_state.y << ", " << state_.car_state.theta << "]" 
    << "\nthe CLOSEST point \"/rslidar\": [" <<  trajectory.poses[index_].pose.position.x << ", " << trajectory.poses[index_].pose.position.y << ", " << trajectory.poses[index_].pose.orientation.z << ", " << trajectory.poses[index_].pose.orientation.w << "]"
    << std::endl;
    
    return delta_;
}

double MpcController::createGlobalSteeringControlCommand(double older_steering_command) {
    std::cout << "\nwe are doing MpcController::createGlobalSteeringControlCommand"
    << "\nTs: " << Ts_ << "   ref_k_limit:"  << ref_k_limit_ 
    << "\nmat_q1: " << mat_q_global_(0, 0) << "   mat_q2: " << mat_q_global_(1, 1) << "   mat_q3: " << mat_q_global_(2, 2) 
    << "\nmat_q4: " << mat_q_global_(3, 3) << "   mat_r: " << mat_r_global_(0) << std::endl;
    
    fsd_common_msgs::CarState state = state_;
    nav_msgs::Path trajectory = global_trajectory_;

    //0.return 0, when there is no trajectory
    int trajectory_length = trajectory.poses.size();
    if (trajectory_length <= 0){
        std::cout << "the trajectory is empty, delta = 0"<< std::endl;
        return delta_ = 0;
    }

    //1.update index
    int index = index_; //

    //2.update state
    MPC_ErrorVec error_vec = getMPCErrorMatrix(index, trajectory, state);
    
    //3.求Matrix A B C并离散化
    updateMPCMatrixABCBar(index, trajectory, state);
    
    //4.求取总状态
    MpcStateTransMat mpc_lin_mat = getMPCConfusionMatrix();
    
    //5.二次规划矩阵
    QuadProgMat quad_prog_mat = getQuadProgMatrix(mpc_lin_mat, error_vec, mat_q_global_, mat_r_global_);
    
    //6.求解
    int status = -1;
    std::vector<double> du_vec = interface_.solve(quad_prog_mat, &status);

    //7.set the delta
    old_delta_ = older_steering_command;
    delta_ = old_delta_ + du_vec[0];

    std::cout << "delta = "<< "old_delta: " << old_delta_ << " + "<<"du_vec[0]: " << du_vec[0] << " = " << delta_
    << "\ntrajectory_length: " << trajectory_length
    << "\nindex: " << index
    << "\nthe CURRENT car state \"/world\" : [" << state_.car_state.x << ", " << state_.car_state.y << ", " << state_.car_state.theta << "]" 
    << "\nthe CLOSEST point \"/world\": [" <<  trajectory.poses[index_].pose.position.x << ", " << trajectory.poses[index_].pose.position.y << ", " << trajectory.poses[index_].pose.orientation.z << ", " << trajectory.poses[index_].pose.orientation.w << "]"
    << std::endl;
    
    return delta_;

}
MPC_ErrorVec MpcController::getMPCErrorMatrix(int index, nav_msgs::Path trajectory, fsd_common_msgs::CarState state){
    // getMPCMatrix
    // input:car_state and ref param
    // output:error state 
    MPC_ErrorVec error_vec = MPC_ErrorVec::Zero();

    double v = std::max(state.car_state_dt.car_state_dt.x, 0.5);//set the velocity in every time step
    double ref_k = trajectory.poses[index].pose.orientation.w; //the curvature of the reference trajectory point
    double ref_yaw = trajectory.poses[index].pose.orientation.z; //the yaw angle of the reference trajectory point
    double yaw = state.car_state.theta;  //the measured yaw angle. In fact, sometimes we are talking about heading angle and yaw angle, but in many codes, it's common to conflat two parameters, but in here, we use yaw angle that measured from GNSS/IMU
    double yaw_rate = state.car_state_dt.car_state_dt.theta; //the mearsured yaw rate

    //limit the ref_k
    ref_k = std::min(ref_k, ref_k_limit_);
    ref_k = std::max(ref_k, -ref_k_limit_);

    const double dx = state.car_state.x - trajectory.poses[index].pose.position.x;
    const double dy = state.car_state.y - trajectory.poses[index].pose.position.y; 

    double dyaw = yaw - ref_yaw;
    if (dyaw > M_PI)
        dyaw -= 2 * M_PI;
    else if (dyaw < -M_PI)
        dyaw += 2 * M_PI;

    error_vec(0) = dy * std::cos(ref_yaw) - dx * std::sin(ref_yaw);
    error_vec(2) = 1 * dyaw;
    error_vec(1) = v * std::sin(error_vec(2));
    error_vec(3) = yaw_rate - v * ref_k;

    return error_vec;
}

void MpcController::updateMPCMatrixABCBar(int index, nav_msgs::Path trajectory, fsd_common_msgs::CarState state) {
    // update: Ad_bar, Bd_bar, gd_bar
    double ref_k = trajectory.poses[index].pose.orientation.w;;
    ref_k = std::min(ref_k, ref_k_limit_);
    ref_k = std::max(ref_k, -ref_k_limit_);

    double v = std::max(state.car_state_dt.car_state_dt.x, 0.5);    

    MPC_MAT_Ad mat_a = MPC_MAT_Ad::Zero();
    MPC_MAT_Bd mat_b = MPC_MAT_Bd::Zero();
    MPC_MAT_gd mat_g = MPC_MAT_gd::Zero();

    mat_a(0, 1) = 1;
    mat_a(1, 1) = (cf_ + cr_) / (-1 * total_mass_ * v);
    mat_a(1, 2) = (cf_ + cr_) / total_mass_;
    mat_a(1, 3) = (cr_ * lr_ - cf_ * lf_) / (total_mass_ * v);
    mat_a(2, 3) = 1;
    mat_a(3, 1) = (cr_ * lr_ - cf_ * lf_) / (iz_ * v);
    mat_a(3, 2) = (cr_ * lr_ - cf_ * lf_) / (-iz_);
    mat_a(3, 3) = -1 * (cr_ * lr_ * lr_ + cf_ * lf_ * lf_) / (iz_ * v);

    mat_b(1) = cf_ / total_mass_;
    mat_b(3) = cf_ * lf_ / iz_;

    mat_g(1) = -1 * v + (cr_ * lr_ - cf_ * lf_) / (total_mass_ * v);
    mat_g(3) = -1 * (cr_ * lr_ * lr_ + cf_ * lf_ * lf_) / (iz_ * v);

    // 离散化
    Eigen::Matrix4d mat_I = Eigen::Matrix4d::Identity();
    auto euler_mat_tmp1 = mat_I + Ts_ / 2.0 * mat_a;
    auto euler_mat_tmp2 = mat_I - Ts_ / 2.0 * mat_a;
    auto mat_ad = euler_mat_tmp1 * euler_mat_tmp2.inverse();
    auto mat_bd = Ts_ * mat_b;
    auto mat_gd = mat_g * (v * ref_k * Ts_);

    Ad_bar_.setZero(5, 5);
    Bd_bar_.setZero(5, 1);
    gd_bar_.setZero(5, 1);

    Ad_bar_.block<4, 4>(0, 0) = mat_ad;
    Ad_bar_.block<4, 1>(0, 4) = mat_bd;
    Ad_bar_(4, 4) = 1;

    Bd_bar_.block<4, 1>(0, 0) = mat_bd;
    Bd_bar_(4, 0) = 1;

    gd_bar_.block<4, 1>(0, 0) = mat_gd;
    // std::cout << "mat_a = \n"
    //           << mat_a << std::endl;
    // std::cout << "mat_ad = \n"
    //           << mat_ad << std::endl;
    // std::cout << "Ad_bar_ = \n"
    //           << Ad_bar_ << std::endl;
}

MpcStateTransMat MpcController::getMPCConfusionMatrix() {
    // calculate mat A^2
    Eigen::Matrix<double, 5, 5> ad_bar_tmp = Ad_bar_;
    for (int i = 0; i != MPC_NP; ++i)
    {
        Ad_bar_power_[i] = ad_bar_tmp;
        ad_bar_tmp *= Ad_bar_;
    }

    // calculate Ad_bar_tilde
    MPC_ATILDE Ad_bar_tilde = MPC_ATILDE::Zero();
    for (int i = 0; i != MPC_NP; ++i)
    {
        Ad_bar_tilde.block<5, 5>(5 * i, 0) = Ad_bar_power_[i];
    }

    // calculate Bd_bar_tilde
    int bd_row = Bd_bar_.rows();
    int bd_col = Bd_bar_.cols();

    MPC_BTILDE Bd_bar_tilde = MPC_BTILDE::Zero();

    Bd_bar_tilde.block(0, 0, bd_row, 1) = Bd_bar_;

    for (int i = 1; i != MPC_NP; ++i)
        for (int j = 0; j <= i; ++j)
            if (i == j)
                Bd_bar_tilde.block(i * bd_row, j * bd_col, bd_row, 1) = Bd_bar_;
            else
                Bd_bar_tilde.block(i * bd_row, j * bd_col, bd_row, 1) = Ad_bar_power_[i - j - 1] * Bd_bar_;

    // calculate gd_bar_tilde
    MPC_gTILDE gd_bar_tilde = MPC_gTILDE::Zero();
    int gd_row = gd_bar_.rows();
    int gd_col = gd_bar_.cols();
    gd_bar_tilde.block(0, 0, gd_row, gd_col) = gd_bar_;

    for (int i = 1; i != MPC_NP; ++i)
        gd_bar_tilde.block(i * gd_row, 0, gd_row, gd_col) = gd_bar_tilde.block((i - 1) * gd_row, 0, gd_row, gd_col) +
                                                            Ad_bar_power_[i - 1] * gd_bar_;

    // std::cout << "Ad_bar_tilde = \n"
    //           << Ad_bar_tilde << std::endl;
    // std::cout << "Bd_bar_tilde = \n"
    //           << Bd_bar_tilde << std::endl;
    // std::cout << "gd_bar_tilde = \n"
    //           << gd_bar_tilde << std::endl;
    return {Ad_bar_tilde, Bd_bar_tilde, gd_bar_tilde};
}

QuadProgMat MpcController::getQuadProgMatrix(const MpcStateTransMat &mpc_lin_mat, const MPC_ErrorVec &error_vec, Eigen::Matrix<double, 4, 4> mat_q, Eigen::Matrix<double, 1, 1> mat_r) const {
    //       Transform MPC Problem to QP
    //       min q(x)=1/2*ΔU^T*H*ΔU+1/2*(Gt^T)^T*ΔU
    //     st. ΔUmin<=ΔU<=ΔUmax
    //            U <=Umax
    //           -U <=-Umin
    // 0.Cost mat, Q and R
    MPC_Q Q_cost = MPC_Q::Zero();
    Q_cost.block<4, 4>(0, 0) = mat_q;

    MPC_Q_TILDE Q_tilde = MPC_Q_TILDE::Zero();

    for (int i = 0; i != MPC_NP; ++i)
        Q_tilde.block<5, 5>(i * 5, i * 5) = Q_cost;

    MPC_R R_cost = MPC_R::Zero();
    R_cost(0, 0) = mat_r.value();

    MPC_R_TILDE R_tilde = MPC_R_TILDE::Zero();
    for (int i = 0; i != MPC_NP; ++i)
        R_tilde(i, i) = R_cost.value();
    
    // 1.get H
    MPC_H H = MPC_H::Zero();
    MPC_H H_not_sym = mpc_lin_mat.Bd_bar_tilde.transpose() * Q_tilde * mpc_lin_mat.Bd_bar_tilde + R_tilde;
    H = 0.5 * (H_not_sym + H_not_sym.transpose()) + 1e-9 * MPC_H::Identity();
    H *= 2;

    // 2.get G
    MPC_G G = MPC_G::Zero();
    Eigen::Matrix<double, 5, 1> error_vec_aug;
    error_vec_aug(4, 0) = old_delta_;
    error_vec_aug.block<4, 1>(0, 0) = error_vec;
    //Eigen::Matrix<double, 5 * MPC_NP, 1> Et = mpc_lin_mat.Ad_bar_tilde * error_vec_aug + mpc_lin_mat.gd_bar_tilde + 0 * ref_trajectory_matrix_;
    Eigen::Matrix<double, 5 * MPC_NP, 1> Et = mpc_lin_mat.Ad_bar_tilde * error_vec_aug + mpc_lin_mat.gd_bar_tilde;
    auto G_transpose = 2 * Et.transpose() * Q_tilde * mpc_lin_mat.Bd_bar_tilde;
    G = 2 * G_transpose.transpose();

    // 3.inequality constraints
    MPC_A A = MPC_A::Zero();
    MPC_b b_low = MPC_b::Zero();
    MPC_b b_up = MPC_b::Zero();
    // mat A
    Eigen::Matrix<double, MPC_NP, MPC_NP> m1_inequal_tril_gen = Eigen::Matrix<double, MPC_NP, MPC_NP>::Ones();
    Eigen::Matrix<double, MPC_NP, MPC_NP> m1_inequal_tril = m1_inequal_tril_gen.triangularView<Eigen::Lower>();
    A = m1_inequal_tril;
    // mat b
    b_up = steering_limit_ * Eigen::Matrix<double, MPC_NP, 1>::Ones() -
            old_delta_ * Eigen::Matrix<double, MPC_NP, 1>::Ones();
    b_low = (-steering_limit_) * Eigen::Matrix<double, MPC_NP, 1>::Ones() -
            old_delta_ * Eigen::Matrix<double, MPC_NP, 1>::Ones();

    // 4.boundary of function input
    MPC_BU LB_U = MPC_BU::Ones();
    MPC_BU UB_U = MPC_BU::Ones();
    LB_U *= -steering_change_rate_limit_ * Ts_;
    UB_U *= steering_change_rate_limit_ * Ts_;
    
    // std::cout << "mpc_lin_mat.Ad_bar_tilde * error_vec_aug + mpc_lin_mat.gd_bar_tilde = \n"
    //           << mpc_lin_mat.Ad_bar_tilde * error_vec_aug + mpc_lin_mat.gd_bar_tilde << std::endl;
    // std::cout << "ref_trajectory_matrix_ = \n"
    //           << ref_trajectory_matrix_ << std::endl;
    // std::cout << "Et = \n"
    //           << Et << std::endl;
    // std::cout << "H = \n"
    //           << H << std::endl;
    // std::cout << "G = \n"
    //           << G << std::endl;
    // std::cout << "A = \n"
    //           << A << std::endl;
    // std::cout << "b_up = \n"
    //           << b_up << std::endl;
    // std::cout << "b_low = \n"
    //           << b_low << std::endl;
    // std::cout << "LB_U = \n"
    //           << LB_U << std::endl;
    // std::cout << "UB_U = \n"
    //           << UB_U << std::endl;

    return {H, G, A, b_low, b_up, LB_U, UB_U};
}

}
