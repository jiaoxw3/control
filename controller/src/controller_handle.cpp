/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#include "controller_handle.hpp"

namespace ns_controller {
//constructor
ControllerHandle::ControllerHandle(ros::NodeHandle &nodeHandle) : nodeHandle_(nodeHandle), controller_(nodeHandle) {
    ROS_INFO("Constructing Handle");
    loadParameters();
    subscribeToTopics();
    publishToTopics();
}

void ControllerHandle::loadParameters() {
    ROS_INFO("loading handle parameters");
    if (!nodeHandle_.param<std::string>("slam_state_topic_name", slam_state_topic_name_, "/estimation/slam/state")) {
        ROS_WARN_STREAM("Did not load slam_state_topic_name. Standard value is: " << slam_state_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("slam_front_axis_center_state_topic_name", slam_front_axis_center_state_topic_name_, "/estimation/slam/front_axis_center_state")) {
        ROS_WARN_STREAM("Did not load slam_front_axis_center_state_topic_name. Standard value is: " << slam_front_axis_center_state_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("slam_rear_axis_center_state_topic_name", slam_rear_axis_center_state_topic_name_, "/estimation/slam/rear_axis_center_state")) {
        ROS_WARN_STREAM("Did not load slam_rear_axis_center_state_topic_name. Standard value is: " << slam_rear_axis_center_state_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("velocity_estimate_topic_name", velocity_estimate_topic_name_, "/estimation/velocity_estimation/velocity_estimate")) {
        ROS_WARN_STREAM("Did not load velocity_estimate_topic_name. Standard value is: " << velocity_estimate_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("control_command_topic_name", control_command_topic_name_, "/control/pure_pursuit/control_command")) {
        ROS_WARN_STREAM("Did not load control_command_topic_name. Standard value is: " << control_command_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("local_trajectory_topic_name", local_trajectory_topic_name_, "/control/pure_pursuit/local_center_line")) {
        ROS_WARN_STREAM("Did not load local_trajectory_topic_name. Standard value is: " << local_trajectory_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("global_trajectory_topic_name", global_trajectory_topic_name_, "/control/pure_pursuit/global_center_line")) {
        ROS_WARN_STREAM("Did not load global_trajectory_topic_name. Standard value is: " << global_trajectory_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("asState_topic_name", asState_topic_name_, "/asState")) {
        ROS_WARN_STREAM("Did not load asState_topic_name. Standard value is: " << asState_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("res_and_ami_topic_name", res_and_ami_topic_name_, "/res_and_ami_data")) {
        ROS_WARN_STREAM("Did not load res_and_ami_topic_name. Standard value is: " << res_and_ami_topic_name_);
    }    
    if (!nodeHandle_.param<int>("node_rate", node_rate_, 20)) {
        ROS_WARN_STREAM("Did not load node_rate. Standard value is: " << node_rate_);
    }
    if (!nodeHandle_.param<std::string>("compute_time_topic_name", compute_time_topic_name_, "/control_compute_time")) {
        ROS_WARN_STREAM("Did not load compute_time_topic_name. Standard value is: " << compute_time_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("makers_topic_name", makers_topic_name_, "/control/pure_pursuit/markers")) {
        ROS_WARN_STREAM("Did not load makers_topic_name. Standard value is: " << makers_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("visualization_topic_name", visualization_topic_name_, "/control/pure_pursuit/visualization")) {
        ROS_WARN_STREAM("Did not load visualization_topic_name. Standard value is: " << visualization_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("steering_return_topic_name", steering_return_topic_name_, "/steering_motor_receive_data")) {
        ROS_WARN_STREAM("Did not load steering_return_topic_name. Standard value is: " << steering_return_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("cones_topic_name_left", cones_topic_name_left, "map_left")) {
        ROS_WARN_STREAM("Did not load cones_topic_name_left. Standard value is: " << cones_topic_name_left);
    }
    if (!nodeHandle_.param<std::string>("cones_topic_name_right", cones_topic_name_right, "map_right")) {
        ROS_WARN_STREAM("Did not load cones_topic_name_right. Standard value is: " << cones_topic_name_right);
    }
}

void ControllerHandle::subscribeToTopics() {
    ROS_INFO("subscribe to topics");
    slamStateSubscriber_ = nodeHandle_.subscribe(slam_state_topic_name_, 1, &ControllerHandle::slamStateCallback, this);
    slamFrontAxisCenterStateSubscriber_ = nodeHandle_.subscribe(slam_front_axis_center_state_topic_name_, 1, &ControllerHandle::slamFrontAxisCenterStateCallback, this);
    slamRearAxisCenterStateSubscriber_ = nodeHandle_.subscribe(slam_rear_axis_center_state_topic_name_, 1, &ControllerHandle::slamRearAxisCenterStateCallback, this);
    velocityEstimateSubscriber_ = nodeHandle_.subscribe(velocity_estimate_topic_name_, 1, &ControllerHandle::velocityEstimateCallback, this);
    localTrajectorySubscriber_ = nodeHandle_.subscribe(local_trajectory_topic_name_, 1, &ControllerHandle::localTrajectoryCallback, this);
    globalTrajectorySubscriber_ = nodeHandle_.subscribe(global_trajectory_topic_name_, 1, &ControllerHandle::globalTrajectoryCallback, this);
    decisionAndAsStateSubscriber_ = nodeHandle_.subscribe(asState_topic_name_, 1, &ControllerHandle::decisionAndAsStateCallback, this);
    resAndAmiSubscriber_ = nodeHandle_.subscribe(res_and_ami_topic_name_, 1, &ControllerHandle::resAndAmiCallback, this);
    steeringReturnSubscriber_ = nodeHandle_.subscribe(steering_return_topic_name_, 1, &ControllerHandle::steeringReturnCallback, this);
    leftConesSubscriber_ = nodeHandle_.subscribe(cones_topic_name_left, 1, &ControllerHandle::leftConesCallback, this);
    rightConesSubscriber_ = nodeHandle_.subscribe(cones_topic_name_right, 1, &ControllerHandle::rightConesCallback, this);
}

void ControllerHandle::publishToTopics() {
    ROS_INFO("publish to topics");
    controlCommandPublisher_ = nodeHandle_.advertise<fsd_common_msgs::ControlCommand>(control_command_topic_name_, 1);
    computeTimePublisher_ = nodeHandle_.advertise<std_msgs::Float32>(compute_time_topic_name_, 1);
    makersPublisher_ = nodeHandle_.advertise<visualization_msgs::MarkerArray>(makers_topic_name_, 1);
    visualizationPublisher_ = nodeHandle_.advertise<fsd_common_msgs::Visualization>(visualization_topic_name_, 1);
}

void ControllerHandle::run() {
    //set the start time of the algorithm
    gettimeofday(&start_time_, NULL);

    //run the algorithm
    controller_.runAlgorithm();
    sendControlCommand();
    sendMarkers();
    controller_.calculateVisualization();
    sendVisualization();
    
    //set the end time of the algorithm
    gettimeofday(&end_time_, NULL);
    //publish the compute time of the algorithm
    calculate_compute_time(start_time_, end_time_);
}

void ControllerHandle::sendControlCommand() {
    control_command_ = controller_.createControlCommand();
    control_command_.header.stamp = ros::Time::now();
    controlCommandPublisher_.publish(control_command_);
}

void ControllerHandle::sendVisualization() {
    visualization_data_ = controller_.visualization_data_;
    visualization_data_.header.stamp = ros::Time::now();
    ++visualization_data_.header.seq;
    visualizationPublisher_.publish(visualization_data_);
}

void ControllerHandle::sendMarkers() {
    markers_ = controller_.markers_;
    makersPublisher_.publish(markers_);
}

void ControllerHandle::calculate_compute_time(struct timeval start_time, struct timeval end_time){
    //unit is [ms],using 1000.0 is to tell the program that the result has to be float
    const auto compute_time = static_cast<float>((end_time_.tv_sec * 1000.0 + end_time_.tv_usec / 1000.0) - (start_time_.tv_sec * 1000.0 + start_time_.tv_usec / 1000.0)); 
    control_compute_time.data = compute_time;
    computeTimePublisher_.publish(control_compute_time);
    std::cout << "The compute time of the module is :" << compute_time << "ms" << "\n" << std::endl;
}

void ControllerHandle::velocityEstimateCallback(const fsd_common_msgs::CarStateDt &velocity) {
    controller_.setVelocity(velocity);
}

void ControllerHandle::localTrajectoryCallback(const nav_msgs::Path &local_trajectory) {
    controller_.setLocalTrajectory(local_trajectory);
}

void ControllerHandle:: globalTrajectoryCallback(const nav_msgs::Path &global_trajectory) {
    controller_.setGlobaTrajectory(global_trajectory);
}

void ControllerHandle::slamStateCallback(const fsd_common_msgs::CarState &state) {
    controller_.setState(state);
}

void ControllerHandle::slamFrontAxisCenterStateCallback(const fsd_common_msgs::CarState &front_axis_center_state) {
    controller_.setFrontAxisCenterState(front_axis_center_state);
}

void ControllerHandle::slamRearAxisCenterStateCallback(const fsd_common_msgs::CarState &rear_axis_center_state) {
    controller_.setRearAxixCenterState(rear_axis_center_state);
}

void ControllerHandle::decisionAndAsStateCallback(const fsd_common_msgs::AsState &decision_and_asState) {
    controller_.setDecisionAndAsState(decision_and_asState);
}

void ControllerHandle::resAndAmiCallback(const fsd_common_msgs::ResAndAmi &res_and_ami) {
    controller_.setResAndAmi(res_and_ami);
}

void ControllerHandle::steeringReturnCallback(const fsd_common_msgs::ControlCommand &steering_return) {
    controller_.setSteeringReturn(steering_return);
}

void ControllerHandle::leftConesCallback(const fsd_common_msgs::ConeDetections &leftconeDetections) {
    controller_.setLeftConeDetections(leftconeDetections);
}

void ControllerHandle::rightConesCallback(const fsd_common_msgs::ConeDetections &rightconeDetections) {
    controller_.setRightConeDetections(rightconeDetections);
}
}//namespace ns_controller
