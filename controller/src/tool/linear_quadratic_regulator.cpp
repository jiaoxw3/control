/*
Solver for discrete-time linear quadratic problem.
*/
#include <iostream>
#include <cstdlib>
#include "Eigen/Dense"
#include "tool/linear_quadratic_regulator.hpp"

namespace math
{

using std::cerr;
using std::cout;
using std::endl;
using Matrix = Eigen::MatrixXd;

void SolveLQRProblem(const Eigen::MatrixXd &A, const Eigen::MatrixXd &B,
						const Eigen::MatrixXd &Q, const Eigen::MatrixXd &R,
						const double tolerance, const uint max_num_iteration,
						Eigen::MatrixXd *ptr_K)
	{
	if (A.rows() != A.cols() || B.rows() != A.rows() || Q.rows() != Q.cols() ||
		Q.rows() != A.rows() || R.rows() != R.cols() || R.rows() != B.cols())
	{
		cerr << "LQR solver: one or more matrices have incompatible dimensions." << endl;
		exit(1);
	}

	Matrix AT = A.transpose();
	Matrix BT = B.transpose();

	// Solves a discrete-time Algebraic Riccati equation (DARE)
	// Calculate Matrix Difference Riccati Equation, initialize P and Q
	Matrix P = Q;
	uint num_iteration = 0;
	double diff = std::numeric_limits<double>::max();
	while (num_iteration++ < max_num_iteration && diff > tolerance)
	{
		Matrix P_next = AT * P * A - AT * P * B * (R + BT * P * B).inverse() * BT * P * A + Q;
		// check the difference between P and P_next
		diff = fabs((P_next - P).maxCoeff());
		P = P_next;
	}

	if (num_iteration >= max_num_iteration)
	{
		cout << "LQR solver cannot converge to a solution, "
				"last consecutive result diff is: "
				<< diff << " at iteration:"<< num_iteration << endl;
	}
	else
	{
		cout << "LQR solver converged at iteration: " << num_iteration
				<< ", max consecutive result diff.: " << diff << endl;
	}
	*ptr_K = (R + BT * P * B).inverse() * BT * P * A;
}
} // namespace math
