/*
*/
#include "tool/interface/hpipm_interface.hpp"
#include "lateral_controller/mpc_controller.hpp" //to get the global variables that define in mpc_controller, such as MPC_NP

namespace ns_controller
{
    void HpipmInterface::setDynamics()
    {
        nx_[0] = MPC_NP;
        nu_[0] = 0;
        nsg_[0] = 0;
        hA_[0] = nullptr;
        hB_[0] = nullptr;
        hb_[0] = nullptr;
    }

    void HpipmInterface::setCost(QuadProgMat &quad_mat)
    {
        hQ_[0] = quad_mat.H.data();
        hS_[0] = nullptr;
        hq_[0] = quad_mat.G.data();
        hR_[0] = nullptr;
        hr_[0] = nullptr;
    }

    void HpipmInterface::setBounds(QuadProgMat &quad_mat)
    {
        nbx_[0] = MPC_NP;

        hpipm_bounds_.idx_x.resize(0);
        hpipm_bounds_.lower_bounds_x.resize(0);
        hpipm_bounds_.upper_bounds_x.resize(0);

        for (int j = 0; j < MPC_NP; j++)
        {
            hpipm_bounds_.idx_x.push_back(j);
            hpipm_bounds_.lower_bounds_x.push_back(quad_mat.LB_U(j));
            hpipm_bounds_.upper_bounds_x.push_back(quad_mat.UB_U(j));
        }

        hidxbx_[0] = hpipm_bounds_.idx_x.data();
        hlbx_[0] = hpipm_bounds_.lower_bounds_x.data();
        hubx_[0] = hpipm_bounds_.upper_bounds_x.data();

        nbu_[0] = 0;
        hidxbu_[0] = nullptr;
        hlbu_[0] = nullptr;
        hubu_[0] = nullptr;
    }

    void HpipmInterface::setPolytopicConstraints(QuadProgMat &quad_mat)
    {
        ng_[0] = MPC_NP;
        hC_[0] = quad_mat.A.data();
        hD_[0] = nullptr;
        hug_[0] = quad_mat.b_up.data();
        hlg_[0] = quad_mat.b_low.data();
    }

    std::vector<double> HpipmInterface::solve(QuadProgMat &quad_mat, int *status)
    {
        setDynamics();
        setCost(quad_mat);
        setBounds(quad_mat);
        setPolytopicConstraints(quad_mat);
        // print_data();
        
        std::vector<double> opt_solution = Solve(status);

        return opt_solution;
    }

    std::vector<double> HpipmInterface::Solve(int *status)
    {
        // ocp qp dim
        int dim_size = d_ocp_qp_dim_memsize(0);
        void *dim_mem = malloc(dim_size);

        struct d_ocp_qp_dim dim;
        d_ocp_qp_dim_create(0, &dim, dim_mem);

        d_ocp_qp_dim_set_all(nx_, nu_, nbx_, nbu_, ng_, nsbx_, nsbu_, nsg_, &dim);
        // ocp qp
        int qp_size = d_ocp_qp_memsize(&dim);
        void *qp_mem = malloc(qp_size);

        struct d_ocp_qp qp;
        d_ocp_qp_create(&dim, &qp, qp_mem);
        d_ocp_qp_set_all(hA_, hB_, hb_, hQ_, hS_, hR_, hq_, hr_,
                         hidxbx_, hlbx_, hubx_, hidxbu_, hlbu_, hubu_,
                         hC_, hD_, hlg_, hug_, hZl_, hZu_, hzl_, hzu_,
                         hidxs_, hlls_, hlus_, &qp);

        // ocp qp sol
        int qp_sol_size = d_ocp_qp_sol_memsize(&dim);
        void *qp_sol_mem = malloc(qp_sol_size);

        struct d_ocp_qp_sol qp_sol;
        d_ocp_qp_sol_create(&dim, &qp_sol, qp_sol_mem);

        // ipm arg
        int ipm_arg_size = d_ocp_qp_ipm_arg_memsize(&dim);
        //printf("\nipm arg size = %d\n", ipm_arg_size);
        void *ipm_arg_mem = malloc(ipm_arg_size);

        struct d_ocp_qp_ipm_arg arg;
        d_ocp_qp_ipm_arg_create(&dim, &arg, ipm_arg_mem);

        //    enum hpipm_mode mode = SPEED_ABS;
        //    enum hpipm_mode mode = SPEED;
        //    enum hpipm_mode mode = BALANCE;
        enum hpipm_mode mode = ROBUST;

        //    int mode = 1;
        double mu0 = 1e2;
        int iter_max = 500;
        double tol_stat = 1e-6;
        double tol_eq = 1e-6;
        double tol_ineq = 1e-6;
        double tol_comp = 1e-6;
        double reg_prim = 1e-12;
        int warm_start = 0;
        int pred_corr = 1;
        int ric_alg = 0;

        d_ocp_qp_ipm_arg_set_default(mode, &arg);

        // d_ocp_qp_ipm_arg_set_mu0(&mu0, &arg);
        d_ocp_qp_ipm_arg_set_iter_max(&iter_max, &arg);
        // d_ocp_qp_ipm_arg_set_tol_stat(&tol_stat, &arg);
        // d_ocp_qp_ipm_arg_set_tol_eq(&tol_eq, &arg);
        // d_ocp_qp_ipm_arg_set_tol_ineq(&tol_ineq, &arg);
        // d_ocp_qp_ipm_arg_set_tol_comp(&tol_comp, &arg);
        // d_ocp_qp_ipm_arg_set_reg_prim(&reg_prim, &arg);
        //    d_ocp_qp_ipm_arg_set_warm_start(&warm_start, &arg);
        //    d_ocp_qp_ipm_arg_set_pred_corr(&pred_corr, &arg);
        //    d_ocp_qp_ipm_arg_set_ric_alg(&ric_alg, &arg);

        // ipm

        int ipm_size = d_ocp_qp_ipm_ws_memsize(&dim, &arg);
        //    printf("\nipm size = %d\n", ipm_size);
        void *ipm_mem = malloc(ipm_size);

        struct d_ocp_qp_ipm_ws workspace;
        d_ocp_qp_ipm_ws_create(&dim, &arg, &workspace, ipm_mem);

        int hpipm_return; // 0 normal; 1 max iter; 2 linesearch issues?
        int iter;

        struct timeval tv0, tv1;

        //gettimeofday(&tv0, nullptr); // start
        d_ocp_qp_ipm_solve(&qp, &qp_sol, &arg, &workspace);
        d_ocp_qp_ipm_get_status(&workspace, &hpipm_return);
        d_ocp_qp_ipm_get_iter(&workspace, &iter);
        //gettimeofday(&tv1, nullptr); // stop
        double time_ocp_ipm = (tv1.tv_usec - tv0.tv_usec) / (1e6);

        //printf("comp time = %f\n", time_ocp_ipm);
        printf("HPIPM solver exitflag: %d, converged at iteration: %d\n", hpipm_return, workspace.iter);

        // extract and print solution
        int nu_max = *nx_;
        double *u = (double *)malloc(nu_max * sizeof(double));

        d_ocp_qp_sol_get_x(0, &qp_sol, u);
        std::vector<double> u_vec(u, u + nu_max);

    /************************************************
    * print ipm statistics
    ************************************************/

        double res_stat;
        d_ocp_qp_ipm_get_max_res_stat(&workspace, &res_stat);
        double res_eq;
        d_ocp_qp_ipm_get_max_res_eq(&workspace, &res_eq);
        double res_ineq;
        d_ocp_qp_ipm_get_max_res_ineq(&workspace, &res_ineq);
        double res_comp;
        d_ocp_qp_ipm_get_max_res_comp(&workspace, &res_comp);
        double *stat;
        d_ocp_qp_ipm_get_stat(&workspace, &stat);
        int stat_m;
        d_ocp_qp_ipm_get_stat_m(&workspace, &stat_m);

        //    printf("\nipm return = %d\n", hpipm_return);
        //    printf("\nipm residuals max: res_g = %e, res_b = %e, res_d = %e, res_m = %e\n", res_stat, res_eq, res_ineq, res_comp);
        //
        //    printf("\nipm iter = %d\n", iter);
        //    printf("\nalpha_aff\tmu_aff\t\tsigma\t\talpha\t\tmu\t\tres_stat\tres_eq\t\tres_ineq\tres_comp\n");
        //    d_print_exp_tran_mat(stat_m, iter+1, stat, stat_m);

        free(dim_mem);
        free(qp_mem);
        free(qp_sol_mem);
        free(ipm_arg_mem);
        free(ipm_mem);

        free(u);

        *status = hpipm_return;

        return u_vec;
    }

    // void HpipmInterface::print_data()
    // {
    //     int N = 0;
    //     int NX = MPC_NP;
    //     int NU = 0;
    //     for (int k = 0; k <= N; k++)
    //     {
    //         if (k != N)
    //         {
    //             if (k != 0)
    //             {
    //                 std::cout << "A_" << k << " = " << std::endl;
    //                 for (int i = 0; i < NX; i++)
    //                 {
    //                     for (int j = 0; j < NX; j++)
    //                     {
    //                         std::cout << hA_[k][i + j * NX] << "\t";
    //                     }
    //                     std::cout << std::endl;
    //                 }
    //             }

    //             std::cout << "B_" << k << " = " << std::endl;
    //             for (int i = 0; i < NX; i++)
    //             {
    //                 for (int j = 0; j < NU; j++)
    //                 {
    //                     std::cout << hB_[k][i + j * NX] << "\t";
    //                 }
    //                 std::cout << std::endl;
    //             }

    //             std::cout << "b_" << k << " = " << std::endl;
    //             for (int i = 0; i < NX; i++)
    //             {
    //                 for (int j = 0; j < 1; j++)
    //                 {
    //                     std::cout << hb_[k][i + j * NX] << "\t";
    //                 }
    //                 std::cout << std::endl;
    //             }
    //         }

    //         std::cout << "Q_" << k << " = " << std::endl;
    //         for (int i = 0; i < NX; i++)
    //         {
    //             for (int j = 0; j < NX; j++)
    //             {
    //                 std::cout << hQ_[k][i + j * NX] << "\t";
    //             }
    //             std::cout << std::endl;
    //         }

    //         if (k != N)
    //         {
    //             std::cout << "R_" << k << " = " << std::endl;
    //             for (int i = 0; i < NU; i++)
    //             {
    //                 for (int j = 0; j < NU; j++)
    //                 {
    //                     std::cout << hR_[k][i + j * NU] << "\t";
    //                 }
    //                 std::cout << std::endl;
    //             }
    //             std::cout << "r_" << k << " = " << std::endl;
    //             for (int i = 0; i < NU; i++)
    //             {
    //                 std::cout << hr_[k][i] << "\t";
    //                 std::cout << std::endl;
    //             }
    //         }

    //         std::cout << "q_" << k << " = " << std::endl;
    //         for (int i = 0; i < NX; i++)
    //         {
    //             std::cout << hq_[k][i] << "\t";
    //             std::cout << std::endl;
    //         }

    //         std::cout << "sizes" << std::endl;
    //         std::cout << nx_[k] << std::endl;
    //         std::cout << nu_[k] << std::endl;
    //         std::cout << nbx_[k] << std::endl;
    //         std::cout << nbu_[k] << std::endl;
    //         std::cout << ng_[k] << std::endl;
    //     }
    // }

} // namespace ns_controller
