/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#include "controller.hpp"

namespace ns_controller {
//constructor
Controller::Controller(ros::NodeHandle &nodeHandle) : nodeHandle_(nodeHandle) {
    loadParameters();
    initialControllers();
}

void Controller::loadParameters(){
    //common parameters
    if (!nodeHandle_.param<double>("path_resolution", path_resolution_, 0.1)) {
        ROS_WARN_STREAM("Did not load path_resolution. Standard value is: " << path_resolution_);
    }
    if (!nodeHandle_.param<double>("lidar2rearwheel", lidar2rearwheel_, 2.4)) {
        ROS_WARN_STREAM("Did not load lidar2rearwheel. Standard value is: " << lidar2rearwheel_);
    }
    if (!nodeHandle_.param<double>("imu2rearwheel", imu2rearwheel_, 1.29)) {
        ROS_WARN_STREAM("Did not load imu2rearwheel. Standard value is: " << imu2rearwheel_);
    }
    if (!nodeHandle_.param<double>("steering_change_rate_limit", steering_change_rate_limit_, 0.15)) {
        ROS_WARN_STREAM("Did not load steering_change_rate_limit. Standard value is: " << steering_change_rate_limit_);
    }
    if (!nodeHandle_.param<double>("steering_limit", steering_limit_, 0.15)) {
        ROS_WARN_STREAM("Did not load steering_limit. Standard value is: " << steering_limit_);
    }
    if (!nodeHandle_.param<double>("wheel_base", wheel_base_, 1.575)) {
        ROS_WARN_STREAM("Did not load wheel_base. Standard value is: " << wheel_base_);
    }
    if (!nodeHandle_.param<double>("total_mass", total_mass_, 290)) {
        ROS_WARN_STREAM("Did not load total_mass. Standard value is: " << total_mass_);
    }
    if (!nodeHandle_.param<double>("cf", cf_, 62280.512)) {
        ROS_WARN_STREAM("Did not load cf. Standard value is: " << cf_);
    }
    if (!nodeHandle_.param<double>("cr", cr_, 62280.512)) {
        ROS_WARN_STREAM("Did not load cr. Standard value is: " << cr_);
    }
    if (!nodeHandle_.param<double>("lf", lf_, 0.919)) {
        ROS_WARN_STREAM("Did not load lf. Standard value is: " << lf_);
    }
    if (!nodeHandle_.param<double>("lr", lr_, 0.656)) {
        ROS_WARN_STREAM("Did not load lr. Standard value is: " << lr_);
    }
    if (!nodeHandle_.param<double>("iz", iz_, 175.735)) {
        ROS_WARN_STREAM("Did not load iz. Standard value is: " << iz_);
    }
    if (!nodeHandle_.param<int>("run_mode", run_mode_, 0)){
        ROS_WARN_STREAM("Did not load run_mode. Standard value is: " << run_mode_);
    }
    if (!nodeHandle_.param<int>("ebs_test/steering_mode", ebs_test_steering_mode_, 0)){
        ROS_WARN_STREAM("Did not load ebs_test_steering_mode. Standard value is: " << ebs_test_steering_mode_);
    }
    if (!nodeHandle_.param<int>("ebs_test/speed_mode", ebs_test_speed_mode_, 0)){
        ROS_WARN_STREAM("Did not load ebs_test_speed_mod. Standard value is: " << ebs_test_speed_mode_);
    }
    if (!nodeHandle_.param<int>("acceleration/steering_mode", acceleration_steering_mode_, 0)){
        ROS_WARN_STREAM("Did not load accelerationt_steering_mode. Standard value is: " << acceleration_steering_mode_);
    }
    if (!nodeHandle_.param<int>("acceleration/speed_mode", acceleration_speed_mode_, 0)){
        ROS_WARN_STREAM("Did not load acceleration_speed_mod. Standard value is: " << acceleration_speed_mode_);
    }
    if (!nodeHandle_.param<int>("skidpad/steering_mode", skidpad_steering_mode_, 0)){
        ROS_WARN_STREAM("Did not load skidpad_steering_mode. Standard value is: " << skidpad_steering_mode_);
    }
    if (!nodeHandle_.param<int>("skidpad/speed_mode", skidpad_speed_mode_, 0)){
        ROS_WARN_STREAM("Did not load skidpad_speed_mode. Standard value is: " << skidpad_speed_mode_);
    }
    if (!nodeHandle_.param<int>("trackDrive/lap1/steering_mode", trackDrive_lap1_steering_mode_, 0)){
        ROS_WARN_STREAM("Did not load trackDrive_lap1_steering_mode. Standard value is: " << trackDrive_lap1_steering_mode_);
    }
    if (!nodeHandle_.param<int>("trackDrive/lap1/speed_mode", trackDrive_lap1_speed_mode_, 0)){
        ROS_WARN_STREAM("Did not load trackDrive_lap1_steering_mode. Standard value is: " << trackDrive_lap1_speed_mode_);
    }
    if (!nodeHandle_.param<int>("trackDrive/lap2_to_end/steering_mode", trackDrive_lap2_to_end_steering_mode_, 0)){
        ROS_WARN_STREAM("Did not load trackDrive_lap2_to_end_steering_mode. Standard value is: " << trackDrive_lap2_to_end_steering_mode_);
    }
    if (!nodeHandle_.param<int>("trackDrive/lap2_to_end/speed_mode", trackDrive_lap2_to_end_speed_mode_, 0)){
        ROS_WARN_STREAM("Did not load trackDrive_lap2_to_end_speed_mode. Standard value is: " << trackDrive_lap2_to_end_speed_mode_);
    }
    if (!nodeHandle_.param<double>("steering_compensate_gain", steering_compensate_gain_, 0.0)){
        ROS_WARN_STREAM("Did not load steering_compensate_gain. Standard value is: " << steering_compensate_gain_);
    }
    //inspection
    if (!nodeHandle_.param<float>("inspection/steering/a", inspection_steering_a_, 0.08)) {
        ROS_WARN_STREAM("Did not load inspection/steering/a. Standard value is: " << inspection_steering_a_);
    }
    if (!nodeHandle_.param<float>("inspection/steering/t", inspection_steering_t_, 5.0)) {
        ROS_WARN_STREAM("Did not load inspection/steering/t. Standard value is: " << inspection_steering_t_);
    }
    if (!nodeHandle_.param<float>("inspection/speed", inspection_speed_, 1.0)) {
        ROS_WARN_STREAM("Did not load inspection/speed. Standard value is: " << inspection_speed_);
    }
    //algorithem parameters
    //pid controller
    if (!nodeHandle_.param<double>("speed_k", pid_controller_.speed_k_, 0.5)) {
        ROS_WARN_STREAM("Did not load speed_k. Standard value is: " << pid_controller_.speed_k_);
    }
    if (!nodeHandle_.param<double>("const_speed_low", pid_controller_.const_speed_low_, 1.0)) {
        ROS_WARN_STREAM("Did not load const_speed_low. Standard value is: " << pid_controller_.const_speed_low_);
    }
    if (!nodeHandle_.param<double>("const_speed_high", pid_controller_.const_speed_high_, 1.0)) {
        ROS_WARN_STREAM("Did not load const_speed_high. Standard value is: " << pid_controller_.const_speed_high_);
    }
    if (!nodeHandle_.param<double>("friction_circle_max_speed", pid_controller_.friction_circle_max_speed_, 1.0)) {
        ROS_WARN_STREAM("Did not load friction_circle_max_speed. Standard value is: " << pid_controller_.friction_circle_max_speed_);
    }
    if (!nodeHandle_.param<double>("friction_circle_max_ay", pid_controller_.friction_circle_max_ay_, 1.0)) {
        ROS_WARN_STREAM("Did not load friction_circle_max_ay. Standard value is: " << pid_controller_.friction_circle_max_ay_);
    }
    if (!nodeHandle_.param<double>("speed_from_planning_max_speed", pid_controller_.speed_from_planning_max_speed_, 1.0)) {
        ROS_WARN_STREAM("Did not load speed_from_planning_max_speed. Standard value is: " << pid_controller_.speed_from_planning_max_speed_);
    }
    if (!nodeHandle_.param<double>("speed_from_planning_lookforward_dist", pid_controller_.speed_from_planning_lookforward_dist_, 0.0)) {
        ROS_WARN_STREAM("Did not load speed_from_planning_lookforward_dist. Standard value is: " << pid_controller_.speed_from_planning_lookforward_dist_);
    }
    //################use to start####################
    if (!nodeHandle_.param<double>("acceleration_k", pid_controller_.acceleration_k_, 3)) {
        ROS_WARN_STREAM("Did not load acceleration_k. Standard value is: " << pid_controller_.acceleration_k_);
    }
    if (!nodeHandle_.param<double>("max_linear_acceleration_speed", pid_controller_.max_linear_acceleration_speed_, 3)) {
        ROS_WARN_STREAM("Did not load max_linear_acceleration_speed. Standard value is: " << pid_controller_.max_linear_acceleration_speed_);
    }
    if (!nodeHandle_.param<double>("max_log_acceleration_speed", pid_controller_.max_log_acceleration_speed_, 3)) {
        ROS_WARN_STREAM("Did not load max_log_acceleration_speed. Standard value is: " << pid_controller_.max_log_acceleration_speed_);
    }
    //###############use to stop######################
    if (!nodeHandle_.param<double>("cos_omiga", pid_controller_.cos_omiga_, 0.147)) {
        ROS_WARN_STREAM("Did not load cos_omiga. Standard value is: " << pid_controller_.cos_omiga_);
    }
    if (!nodeHandle_.param<double>("linear_k", pid_controller_.linear_k_, 4)) {
        ROS_WARN_STREAM("Did not load linear_k. Standard value is: " << pid_controller_.linear_k_);
    }
    //##############################################
    //pure pursuit controller
    if (!nodeHandle_.param<double>("pure_pursuit_local/steering_p_local", pure_pursuit_controller_.steering_p_local_, 1.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_local/steering_p_local. Standard value is: " << pure_pursuit_controller_.steering_p_local_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_local/look_forward_distance_local", pure_pursuit_controller_.look_forward_distance_local_, 3.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_local/look_forward_distance_local. Standard value is: " << pure_pursuit_controller_.look_forward_distance_local_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_local/look_forward_k_local", pure_pursuit_controller_.look_forward_k_local_, 0.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_local/look_forward_k_local. Standard value is: " << pure_pursuit_controller_.look_forward_k_local_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_global/steering_p_global", pure_pursuit_controller_.steering_p_global_, 1.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_global/steering_p_global. Standard value is: " << pure_pursuit_controller_.steering_p_global_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_global/look_forward_distance_global", pure_pursuit_controller_.look_forward_distance_global_, 3.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_global/look_forward_distance_global. Standard value is: " << pure_pursuit_controller_.look_forward_distance_global_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_global/look_forward_k_global", pure_pursuit_controller_.look_forward_k_global_, 0.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_global/look_forward_k_global. Standard value is: " << pure_pursuit_controller_.look_forward_k_global_);
    }
    //stanley controller
    if (!nodeHandle_.param<double>("stanley_local/steering_p_local", stanley_controller_.steering_p_local_, 0.5)) {
        ROS_WARN_STREAM("Did not load stanley_local/steering_p_local. Standard value is: " << stanley_controller_.steering_p_local_);
    }
    if (!nodeHandle_.param<double>("stanley_local/look_forward_distance_local", stanley_controller_.look_forward_distance_local_, 0.0)) {
        ROS_WARN_STREAM("Did not load stanley_local/look_forward_distance_local. Standard value is: " << stanley_controller_.look_forward_distance_local_);
    }
    if (!nodeHandle_.param<double>("stanley_local/look_forward_k_local", stanley_controller_.look_forward_k_local_, 0.0)) {
        ROS_WARN_STREAM("Did not load stanley_local/look_forward_k_local. Standard value is: " << stanley_controller_.look_forward_k_local_);
    }
    if (!nodeHandle_.param<double>("stanley_global/steering_p_global", stanley_controller_.steering_p_global_, 0.5)) {
        ROS_WARN_STREAM("Did not load stanley_global/steering_p_global. Standard value is: " << stanley_controller_.steering_p_global_);
    }
    if (!nodeHandle_.param<double>("stanley_global/look_forward_distance_global", stanley_controller_.look_forward_distance_global_, 0.0)) {
        ROS_WARN_STREAM("Did not load stanley_global/look_forward_distance_global. Standard value is: " << stanley_controller_.look_forward_distance_global_);
    }
    if (!nodeHandle_.param<double>("stanley_global/look_forward_k_global", stanley_controller_.look_forward_k_global_, 0.0)) {
        ROS_WARN_STREAM("Did not load stanley_global/look_forward_k_global. Standard value is: " << stanley_controller_.look_forward_k_global_);
    }    
    //lqr controller
    if (!nodeHandle_.param<double>("lqr_local/Ts", lqr_controller_.Ts_, 0.1)) {
        ROS_WARN_STREAM("Did not load lqr_local/Ts. Standard value is: " << lqr_controller_.Ts_);
    }
    if (!nodeHandle_.param<double>("lqr_local/ref_k_limit", lqr_controller_.ref_k_limit_, 0.12)) {
        ROS_WARN_STREAM("Did not load lqr_local/ref_k_limit. Standard value is: " << lqr_controller_.ref_k_limit_);
    }
    if (!nodeHandle_.param<int>("lqr_local/lqr_max_iteration", lqr_controller_.lqr_max_iteration_, 200)) {
        ROS_WARN_STREAM("Did not load lqr_local/lqr_max_iteration. Standard value is: " << lqr_controller_.lqr_max_iteration_);
    }
    if (!nodeHandle_.param<double>("lqr_local/lqr_tolerance", lqr_controller_.lqr_tolerance_, 0.008)) {
        ROS_WARN_STREAM("Did not load lqr_local/lqr_tolerance. Standard value is: " << lqr_controller_.lqr_tolerance_);
    }
    if (!nodeHandle_.param<double>("lqr_local/look_forward_distance", lqr_controller_.look_forward_distance_local_, 0.5)) {
        ROS_WARN_STREAM("Did not load lqr_local/look_forward_distance. Standard value is: " << lqr_controller_.look_forward_distance_local_);
    }
    if (!nodeHandle_.param<double>("lqr_local/feedforward_distance", lqr_controller_.feedforward_distance_local_, 1)) {
        ROS_WARN_STREAM("Did not load lqr_local/feedforward_distance. Standard value is: " << lqr_controller_.feedforward_distance_local_);
    }
    if (!nodeHandle_.param<double>("lqr_local/mat_q1", lqr_controller_.matrix_q_local_(0, 0), 0.05)) {
        ROS_WARN_STREAM("Did not load lqr_local/mat_q1. Standard value is: " << lqr_controller_.matrix_q_local_(0, 0));
    }
    if (!nodeHandle_.param<double>("lqr_local/mat_q2", lqr_controller_.matrix_q_local_(1, 1), 0.0)) {
        ROS_WARN_STREAM("Did not load lqr_local/mat_q2. Standard value is: " << lqr_controller_.matrix_q_local_(1, 1));
    }
    if (!nodeHandle_.param<double>("lqr_local/mat_q3", lqr_controller_.matrix_q_local_(2, 2), 1.0)) {
        ROS_WARN_STREAM("Did not load lqr_local/mat_q3. Standard value is: " << lqr_controller_.matrix_q_local_(2, 2));
    }
    if (!nodeHandle_.param<double>("lqr_local/mat_q4", lqr_controller_.matrix_q_local_(3, 3), 0.0)) {
        ROS_WARN_STREAM("Did not load lqr_local/mat_q4. Standard value is: " << lqr_controller_.matrix_q_local_(3, 3));
    }
    if (!nodeHandle_.param<double>("lqr_local/mat_r", lqr_controller_.matrix_r_local_(0), 1.0)) {
        ROS_WARN_STREAM("Did not load lqr_local/matmat_r. Standard value is: " << lqr_controller_.matrix_r_local_(0));
    }   
    if (!nodeHandle_.param<double>("lqr_global/look_forward_distance", lqr_controller_.look_forward_distance_global_, 0.5)) {
        ROS_WARN_STREAM("Did not load lqr_global/look_forward_distance. Standard value is: " << lqr_controller_.look_forward_distance_global_);
    }
    if (!nodeHandle_.param<double>("lqr_global/feedforward_distance", lqr_controller_.feedforward_distance_global_, 1)) {
        ROS_WARN_STREAM("Did not load lqr_global/feedforward_distance. Standard value is: " << lqr_controller_.feedforward_distance_global_);
    }
    if (!nodeHandle_.param<double>("lqr_global/mat_q1", lqr_controller_.matrix_q_global_(0, 0), 0.05)) {
        ROS_WARN_STREAM("Did not load lqr_global/mat_q1. Standard value is: " << lqr_controller_.matrix_q_global_(0, 0));
    }
    if (!nodeHandle_.param<double>("lqr_global/mat_q2", lqr_controller_.matrix_q_global_(1, 1), 0.0)) {
        ROS_WARN_STREAM("Did not load lqr_global/mat_q2. Standard value is: " << lqr_controller_.matrix_q_global_(1, 1));
    }
    if (!nodeHandle_.param<double>("lqr_global/mat_q3", lqr_controller_.matrix_q_global_(2, 2), 1.0)) {
        ROS_WARN_STREAM("Did not load lqr_global/mat_q3. Standard value is: " << lqr_controller_.matrix_q_global_(2, 2));
    }
    if (!nodeHandle_.param<double>("lqr_global/mat_q4", lqr_controller_.matrix_q_global_(3, 3), 0.0)) {
        ROS_WARN_STREAM("Did not load lqr_global/mat_q4. Standard value is: " << lqr_controller_.matrix_q_global_(3, 3));
    }
    if (!nodeHandle_.param<double>("lqr_global/mat_r", lqr_controller_.matrix_r_global_(0), 1.0)) {
        ROS_WARN_STREAM("Did not load lqr_global/matmat_r. Standard value is: " << lqr_controller_.matrix_r_global_(0));
    }  
    //mpc controller
    if (!nodeHandle_.param<double>("mpc_local/Ts", mpc_controller_.Ts_, 0.1)) {
        ROS_WARN_STREAM("Did not load mpc_local/Ts. Standard value is: " << mpc_controller_.Ts_);
    }
    if (!nodeHandle_.param<double>("mpc_local/ref_k_limit", mpc_controller_.ref_k_limit_, 0.12)) {
        ROS_WARN_STREAM("Did not load mpc_local/ref_k_limit. Standard value is: " << mpc_controller_.ref_k_limit_);
    }
    if (!nodeHandle_.param<double>("mpc_local/mat_q1", mpc_controller_.mat_q_local_(0, 0), 0.05)) {
        ROS_WARN_STREAM("Did not load mpc_local/mat_q1. Standard value is: " << mpc_controller_.mat_q_local_(0, 0));
    }
    if (!nodeHandle_.param<double>("mpc_local/mat_q2", mpc_controller_.mat_q_local_(1, 1), 0.0)) {
        ROS_WARN_STREAM("Did not load mpc_local/mat_q2. Standard value is: " << mpc_controller_.mat_q_local_(1, 1));
    }
    if (!nodeHandle_.param<double>("mpc_local/mat_q3", mpc_controller_.mat_q_local_(2, 2), 1.0)) {
        ROS_WARN_STREAM("Did not load mpc_local/mat_q3. Standard value is: " << mpc_controller_.mat_q_local_(2, 2));
    }
    if (!nodeHandle_.param<double>("mpc_local/mat_q4", mpc_controller_.mat_q_local_(3, 3), 0.0)) {
        ROS_WARN_STREAM("Did not load mpc_local/mat_q4. Standard value is: " << mpc_controller_.mat_q_local_(3, 3));
    }
    if (!nodeHandle_.param<double>("mpc_local/mat_r", mpc_controller_.mat_r_local_(0), 1.0)) {
        ROS_WARN_STREAM("Did not load mpc_local/mat_r. Standard value is: " << mpc_controller_.mat_r_local_(0));
    }
    if (!nodeHandle_.param<double>("mpc_global/mat_q1", mpc_controller_.mat_q_global_(0, 0), 0.05)) {
        ROS_WARN_STREAM("Did not load mpc_global/mat_q1. Standard value is: " << mpc_controller_.mat_q_global_(0, 0));
    }
    if (!nodeHandle_.param<double>("mpc_global/mat_q2", mpc_controller_.mat_q_global_(1, 1), 0.0)) {
        ROS_WARN_STREAM("Did not load mpc_global/mat_q2. Standard value is: " << mpc_controller_.mat_q_global_(1, 1));
    }
    if (!nodeHandle_.param<double>("mpc_global/mat_q3", mpc_controller_.mat_q_global_(2, 2), 1.0)) {
        ROS_WARN_STREAM("Did not load mpc_global/mat_q3. Standard value is: " << mpc_controller_.mat_q_global_(2, 2));
    }
    if (!nodeHandle_.param<double>("mpc_global/mat_q4", mpc_controller_.mat_q_global_(3, 3), 0.0)) {
        ROS_WARN_STREAM("Did not load mpc_global/mat_q4. Standard value is: " << mpc_controller_.mat_q_global_(3, 3));
    }
    if (!nodeHandle_.param<double>("mpc_global/mat_r", mpc_controller_.mat_r_global_(0), 1.0)) {
        ROS_WARN_STREAM("Did not load mpc_global/mat_r. Standard value is: " << mpc_controller_.mat_r_global_(0));
    }
    //pure pursuit front aixs controller
    if (!nodeHandle_.param<double>("pure_pursuit_front_axis_local/steering_p_local", pure_pursuit_front_axis_controller_.steering_p_local_, 1.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_front_axis_local/steering_p_local. Standard value is: " << pure_pursuit_front_axis_controller_.steering_p_local_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_front_axis_local/look_forward_distance_local", pure_pursuit_front_axis_controller_.look_forward_distance_local_, 3.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_front_axis_local/look_forward_distance_local. Standard value is: " << pure_pursuit_front_axis_controller_.look_forward_distance_local_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_front_axis_local/look_forward_k_local", pure_pursuit_front_axis_controller_.look_forward_k_local_, 0.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_front_axis_local/look_forward_k_local. Standard value is: " << pure_pursuit_front_axis_controller_.look_forward_k_local_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_front_axis_global/steering_p_global", pure_pursuit_front_axis_controller_.steering_p_global_, 1.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_front_axis_global/steering_p_global. Standard value is: " << pure_pursuit_front_axis_controller_.steering_p_global_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_front_axis_global/look_forward_distance_global", pure_pursuit_front_axis_controller_.look_forward_distance_global_, 3.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_front_axis_global/look_forward_distance_global. Standard value is: " << pure_pursuit_front_axis_controller_.look_forward_distance_global_);
    }
    if (!nodeHandle_.param<double>("pure_pursuit_front_axis_global/look_forward_k_global", pure_pursuit_front_axis_controller_.look_forward_k_global_, 0.0)) {
        ROS_WARN_STREAM("Did not load pure_pursuit_front_axis_global/look_forward_k_global. Standard value is: " << pure_pursuit_front_axis_controller_.look_forward_k_global_);
    }
    //mpcc controller
    //ribbon controller
    if (!nodeHandle_.param<double>("ribbon/steering_p_global_", ribbon_controller_.steering_p_global_, 1.0)) {
        ROS_WARN_STREAM("Did not load ribbon/steering_p_global_. Standard value is: " << ribbon_controller_.steering_p_global_);
    }
    if (!nodeHandle_.param<double>("ribbon/ribbon_max_speed_", ribbon_controller_.ribbon_max_speed_, 10.)) {
        ROS_WARN_STREAM("Did not load ribbon/ribbon_max_speed_. Standard value is: " << ribbon_controller_.ribbon_max_speed_);
    }
    if (!nodeHandle_.param<double>("ribbon/circle_speed_k", ribbon_controller_.circle_speed_k, 1.0)) {
        ROS_WARN_STREAM("Did not load ribbon/circle_speed_k. Standard value is: " << ribbon_controller_.circle_speed_k);
    }
    if (!nodeHandle_.param<double>("ribbon/brake_speed_k", ribbon_controller_.brake_speed_k, 1./3.)) {
        ROS_WARN_STREAM("Did not load ribbon/brake_speed_k. Standard value is: " << ribbon_controller_.brake_speed_k);
    }
    if (!nodeHandle_.param<double>("ribbon/speed_k_", ribbon_controller_.speed_k_, 0.5)) {
        ROS_WARN_STREAM("Did not load ribbon/speed_k_. Standard value is: " << ribbon_controller_.speed_k_);
    }
}

void Controller::initialControllers() {
    pid_controller_.path_resolution_ = path_resolution_;
    pid_controller_.lidar2rearwheel_ = lidar2rearwheel_;
    pid_controller_.imu2rearwheel_ = imu2rearwheel_;
    pid_controller_.wheel_base_ = wheel_base_;
    
    pure_pursuit_controller_.path_resolution_ = path_resolution_;
    pure_pursuit_controller_.lidar2rearwheel_ = lidar2rearwheel_;
    pure_pursuit_controller_.imu2rearwheel_ = imu2rearwheel_;
    pure_pursuit_controller_.steering_change_rate_limit_ = steering_change_rate_limit_;
    pure_pursuit_controller_.steering_limit_ = steering_limit_;
    pure_pursuit_controller_.wheel_base_ = wheel_base_;

    stanley_controller_.path_resolution_ = path_resolution_;
    stanley_controller_.lidar2rearwheel_ = lidar2rearwheel_;
    stanley_controller_.imu2rearwheel_ = imu2rearwheel_;
    stanley_controller_.steering_change_rate_limit_ = steering_change_rate_limit_;
    stanley_controller_.steering_limit_ = steering_limit_;
    stanley_controller_.wheel_base_ = wheel_base_;

    mpc_controller_.cf_ = cf_;
    mpc_controller_.cr_ = cr_;
    mpc_controller_.lf_ = lf_;
    mpc_controller_.lr_ = lr_;
    mpc_controller_.total_mass_ = total_mass_;
    mpc_controller_.iz_ = iz_;
    mpc_controller_.path_resolution_ = path_resolution_;
    mpc_controller_.lidar2rearwheel_ = lidar2rearwheel_;
    mpc_controller_.imu2rearwheel_ = imu2rearwheel_;
    mpc_controller_.steering_change_rate_limit_ = steering_change_rate_limit_;
    mpc_controller_.steering_limit_ = steering_limit_;
    mpc_controller_.wheel_base_ = wheel_base_;

    pure_pursuit_front_axis_controller_.path_resolution_ = path_resolution_;
    pure_pursuit_front_axis_controller_.lidar2rearwheel_ = lidar2rearwheel_;
    pure_pursuit_front_axis_controller_.imu2rearwheel_ = imu2rearwheel_;
    pure_pursuit_front_axis_controller_.steering_change_rate_limit_ = steering_change_rate_limit_;
    pure_pursuit_front_axis_controller_.steering_limit_ = steering_limit_;
    pure_pursuit_front_axis_controller_.wheel_base_ = wheel_base_;

    ribbon_controller_.path_resolution_ = path_resolution_;
    ribbon_controller_.lidar2rearwheel_ = lidar2rearwheel_;
    ribbon_controller_.imu2rearwheel_ = imu2rearwheel_;
    ribbon_controller_.steering_change_rate_limit_ = steering_change_rate_limit_;
    ribbon_controller_.steering_limit_ = steering_limit_;
    ribbon_controller_.wheel_base_ = wheel_base_; 
}

void Controller::setLocalTrajectory(const nav_msgs::Path &local_trajectory) {
    local_trajectory_ = local_trajectory;
    pid_controller_.local_trajectory_ = local_trajectory;
    pure_pursuit_controller_.local_trajectory_ = local_trajectory;
    stanley_controller_.local_trajectory_ = local_trajectory;
    lqr_controller_.local_trajectory_ = local_trajectory;
    mpc_controller_.local_trajectory_ = local_trajectory;
    pure_pursuit_front_axis_controller_.local_trajectory_ = local_trajectory;
}

void Controller::setGlobaTrajectory(const nav_msgs::Path &global_trajectory) {
    global_trajectory_ = global_trajectory;
    if(global_trajectory.poses.size() > 3){
    panduan_ = 2;
    }
    pid_controller_.global_trajectory_ = global_trajectory;
    pure_pursuit_controller_.global_trajectory_ = global_trajectory;
    stanley_controller_.global_trajectory_ = global_trajectory;
    lqr_controller_.global_trajectory_ = global_trajectory;
    mpc_controller_.global_trajectory_ = global_trajectory;
    pure_pursuit_front_axis_controller_.global_trajectory_ = global_trajectory;
}

void Controller::setState(const fsd_common_msgs::CarState &state) {
    state_ = state;
    pid_controller_.state_  = state;
    pure_pursuit_controller_.state_ = state;
    stanley_controller_.state_ = state;
    lqr_controller_.state_ = state;
    mpc_controller_.state_ = state;
    pure_pursuit_front_axis_controller_.state_ = state;
}

void Controller::setFrontAxisCenterState(const fsd_common_msgs::CarState &front_axis_center_state) {
    front_axis_center_state_ = front_axis_center_state;
    stanley_controller_.front_axis_center_state_ = front_axis_center_state;
    pure_pursuit_front_axis_controller_.front_axis_center_state_ = front_axis_center_state;
    ribbon_controller_.front_axis_center_state_ = front_axis_center_state;
}

void Controller::setRearAxixCenterState(const fsd_common_msgs::CarState &rear_axis_center_state) {
    rear_axis_center_state_ = rear_axis_center_state;
    pure_pursuit_controller_.rear_axis_center_state_ = rear_axis_center_state;
    ribbon_controller_.rear_axis_center_state_ = rear_axis_center_state;
}

void Controller::setVelocity(const fsd_common_msgs::CarStateDt &velocity) {
    velocity_ = velocity;
    pid_controller_.velocity_ = velocity;
    pure_pursuit_controller_.velocity_ = velocity;
    pure_pursuit_front_axis_controller_.velocity_ = velocity;
}

void Controller::setDecisionAndAsState(const fsd_common_msgs::AsState &decision_and_asState) {
    decision_and_asState_ = decision_and_asState;
}

void Controller::setResAndAmi(const fsd_common_msgs::ResAndAmi &res_and_ami) {
	res_and_ami_ = res_and_ami;
    //for test algorithm in real car without GO
	//res_and_ami_.resState = 1;
}

void Controller::setSteeringReturn(const fsd_common_msgs::ControlCommand &steering_return) {
    steering_return_ = steering_return;
}

void Controller::setLeftConeDetections(const fsd_common_msgs::ConeDetections &leftconeDetections){
    ribbon_controller_.ConeDetections_left_=leftconeDetections;
}

void Controller::setRightConeDetections(const fsd_common_msgs::ConeDetections &rightconeDetections){
    ribbon_controller_.ConeDetections_right_=rightconeDetections;
}

std::pair<double, double> Controller::createPreDist(double &delta, fsd_common_msgs::CarState &state, nav_msgs::Path &trajectory) {
    double r1, r2, ref_yaw_angle, pre_index, pre_dist, lateral_error_limit;
    double x1, y1, l, x2, y2,x3, y3, theta1, theta2;
    fsd_common_msgs::CarState pre_state;
    std::pair<double, double> result;

    r1 = fabs(wheel_base_ / std::sin(delta));
    r2 = fabs(wheel_base_ / std::tan(delta));
    ref_yaw_angle = std::max(rear_axis_center_state_.car_state_dt.car_state_dt.x, 0.5) * time_length_ / r2; //[rad]

    x1 = r1 * std::sin(ref_yaw_angle);
    y1 = r1 - r1 * std::cos(ref_yaw_angle);
    lateral_error_limit = road_width_ / 2 - (r1 - r2 + car_width_ / 2); //[m]
    l = std::hypot(x1, y1);
    theta1 = atan2(y1, x1);
    theta2 = theta1 + delta + state.car_state.theta;
    x2 = l * std::cos(theta2);
    y2 = l * std::sin(theta2);

    x3 = state.car_state.x + x2;
    y3 = state.car_state.y + y2;

    pre_state.car_state.x = x3;
    pre_state.car_state.y = y3;

    pre_index =calculate_closest_point(pre_state, trajectory);
    if (trajectory.poses.size() == 0){
        std::cout << "the trajectory is empty, result = (0, 0)" << std::endl;
        return result = std::make_pair(0, 0);
    }

    if (((pre_state.car_state.y - trajectory.poses[pre_index].pose.position.y) * cos(state.car_state.theta) - (pre_state.car_state.x - trajectory.poses[pre_index].pose.position.x) * sin(state.car_state.theta)) < 0)
        pre_dist = std::hypot(pre_state.car_state.x - trajectory.poses[pre_index].pose.position.x, pre_state.car_state.y - trajectory.poses[pre_index].pose.position.y);
    else
        pre_dist = -1 * std::hypot(pre_state.car_state.x - trajectory.poses[pre_index].pose.position.x, pre_state.car_state.y - trajectory.poses[pre_index].pose.position.y);

    result = std::make_pair(pre_dist, lateral_error_limit);
    return result;
}

double Controller::geometric_road_constraints(double &delta, fsd_common_msgs::CarState &state, nav_msgs::Path &trajectory){
    std::pair<double, double> input = createPreDist(delta, state, trajectory);  
    int max_num_itr = 5;
    int num_itr;
    double pre_dist = input.first;
    double lateral_error_limit = input.second;
    std::cout << "pre_dist: "<< pre_dist << "lateral_error_limit: "<< lateral_error_limit << std::endl;
    double temp = delta;
    
    while ((pre_dist > lateral_error_limit)&(num_itr < max_num_itr)){
        temp = temp + 0.1 * 0.01745; // every loop, change 1 deg = 0.01745 rad
        pre_dist = createPreDist(temp, state, local_trajectory_).first;
        lateral_error_limit = createPreDist(temp, state, local_trajectory_).second;
        std::cout << "reach geometric road constraints! " << "\nprevious delta: " << delta <<" now it's: " << temp << std::endl;  
    }
    while ((pre_dist < -lateral_error_limit)&(num_itr < max_num_itr)){
        temp = temp - 0.1 * 0.01745; // every loop, change 1 deg = 0.01745 rad
        pre_dist = createPreDist(temp, state, local_trajectory_).first;
        lateral_error_limit = createPreDist(temp, state, local_trajectory_).second;
        std::cout << "reach geometric road constraints! " << "\nprevious delta: " << delta <<" now it's: " << temp << std::endl;                
    }
        // std::cout << "pre_dist: "<< pre_dist << "lateral_error_limit: "<< lateral_error_limit << std::endl;
        // std::cout << "reach geometric road constraints! " << "\nprevious delta: " << delta <<" now it's: " << temp << std::endl;

    return temp;
}

fsd_common_msgs::ControlCommand Controller::createControlCommand() {
    //0.stop and wait for "go"
    if (res_and_ami_.resState == 0){   
        std::cout << "\nNO \"GO\" RECEIVED YET!" << std::endl;
        delta_ = 0.0;
        speed_ = 0.0;
    }

    //1.record the time when receive "go" for the first time, and then use this time to "delay" go
    // FOR FSSIM ONLY
    // res_and_ami_.resState = 1;
    if (res_and_ami_.resState == 1 && i_go < 4) {
        go_receive_time = ros::Time::now().toSec();
        //同时速度控制器也开始等待3秒
        pid_controller_.setGoReceiveTime(go_receive_time);
        i_go++;
    }
    if (((ros::Time::now().toSec() - go_receive_time) < 3) && res_and_ami_.resState == 1){ //delay for 3 seconds
        delta_ = 0.0;
        speed_ = 0.0;
        std::cout << "\nRECEIVE GO! control will start in: "<< 3 - (ros::Time::now().toSec() - go_receive_time) << "sec" << std::endl;
    }

    //2.if the mission is ended, set the speed = 0
    if (decision_and_asState_.end == 1){
        //delta_ = 0.0; //to be safe, keep steering
        if(i_stop_ < 4){
            stop_receive_time_ = ros::Time::now().toSec();
            i_stop_++;
            stop_speed_ = state_.car_state_dt.car_state_dt.x;
        }
        switch (stop_mode_)
        {
        case 0 :{
            speed_ = 0.0;
            break;
        
        }
        case 1:{
            //目前还是返回0，因为cos参数设置较麻烦
            speed_ = pid_controller_.createCosStopControlCommand(stop_receive_time_ , stop_speed_);
            break;
        }
        case 2:{
            speed_ = pid_controller_.createLinearStopControlCommand(stop_speed_,stop_speed_);
            break;
        }
        default:{
            speed_ = 0.0;
        }
        }
        std::cout << "\nMISSION ENDED! set speed to 0 m/s" << std::endl;
    }

    //3.limit the steering angle
    delta_ = std::min(delta_, steering_limit_);
    delta_ = std::max(delta_, -steering_limit_);  

    //4.limit the steering change rate
    float t = 1.0 / node_rate_;
    double old_delta = control_command_.steering_angle.data;
    delta_ = std::min(delta_, old_delta + t * steering_change_rate_limit_);
    delta_ = std::max(delta_, old_delta - t * steering_change_rate_limit_);

    //5.limit the throttle(speed command)
    speed_ = std::min(speed_, 20.0); //this is mandatory constraints for redundancy

    //6. compensate the steering error
    // if using FFSIM, commit this
    steering_without_compensate_ = delta_;
    if (fabs(delta_ - steering_return_.steering_angle.data) > 5 * M_PI / 180.0) {
        steering_with_compensate_ = delta_;
        std::cout << "steering error is too large, stop compensate! " << std::endl;
    }
    else
        steering_with_compensate_ = delta_ + steering_compensate_gain_ * (delta_ - steering_return_.steering_angle.data);

    //7.fill the ros msg
    control_command_.steering_angle.data = static_cast<float>(steering_with_compensate_);
    control_command_.throttle.data = static_cast<float>(speed_);

    std::cout << "control_command_.steering_angle.data: " <<  control_command_.steering_angle.data
    << "\ncontrol_command_.throttle.data: " << control_command_.throttle.data << std::endl;

    return control_command_;
}

void Controller::switch_cases(int steering_mode, int speed_mode){
    switch (steering_mode){
        case 0:{
            fsd_common_msgs::CarState state = rear_axis_center_state_;
            state.car_state.x = -lidar2rearwheel_;
            state.car_state.y = 0;
            state.car_state.theta = 0;
            pure_pursuit_controller_.index_ = calculate_closest_point(state, local_trajectory_); //local control use the frame: /rslidar
            delta_ = pure_pursuit_controller_.createLocalSteeringControlCommand(control_command_.steering_angle.data);

            markers_ =  pure_pursuit_controller_.markers_;
            break;
        }
        case 1:{
            pure_pursuit_controller_.index_ = calculate_closest_point(rear_axis_center_state_, global_trajectory_); //global control use the frame: /world
            delta_ = pure_pursuit_controller_.createGlobalSteeringControlCommand(control_command_.steering_angle.data);

            // visualization
            markers_ =  pure_pursuit_controller_.markers_;
            break;
        }
        case 2:{
            pure_pursuit_controller_.index_ = calculate_closest_new_point(rear_axis_center_state_, global_trajectory_);
            delta_ = pure_pursuit_controller_.createGlobalSteeringControlCommand(control_command_.steering_angle.data);

            // visualization
            markers_ =  pure_pursuit_controller_.markers_;
            break;
        }
        case 3:{
            fsd_common_msgs::CarState state = front_axis_center_state_;
            state.car_state.x = wheel_base_ - lidar2rearwheel_;
            state.car_state.y = 0;
            state.car_state.theta = 0;
            stanley_controller_.index_ = calculate_closest_point(state, local_trajectory_);
            delta_ = stanley_controller_.createLocalSteeringControlCommand(control_command_.steering_angle.data);
            // delta_ = geometric_road_constraints(delta_, state, local_trajectory_); //using the simple geometric road constraints
            markers_ =  stanley_controller_.markers_;
            break;
        }
        case 4:{
            stanley_controller_.index_ = calculate_closest_point(front_axis_center_state_, global_trajectory_);
            delta_  = stanley_controller_.createGlobalSteeringControlCommand(control_command_.steering_angle.data);
            markers_ =  pure_pursuit_front_axis_controller_.markers_;
            break;
        }
        case 5:{
            stanley_controller_.index_ = calculate_closest_new_point(front_axis_center_state_, global_trajectory_);
            delta_ = stanley_controller_.createGlobalSteeringControlCommand(control_command_.steering_angle.data); 
            markers_ =  pure_pursuit_front_axis_controller_.markers_;
            break;
        }
        case 6:{
            fsd_common_msgs::CarState state = state_;
            state.car_state.x = -(lidar2rearwheel_ - lr_);
            state.car_state.y = 0;
            state.car_state.theta = 0;
            lqr_controller_.index_ = calculate_closest_point(state, local_trajectory_);
            delta_ = lqr_controller_.createLocalSteeringControlCommand();
            break;
        }
        case 7:{
            lqr_controller_.index_ = calculate_closest_point(state_, global_trajectory_);
            delta_ = lqr_controller_.createGlobalSteeringControlCommand();
            break;
        }
        case 8:{
            lqr_controller_.index_ = calculate_closest_new_point(state_, global_trajectory_);
            delta_ = lqr_controller_.createGlobalSteeringControlCommand();
            break;
        }
        case 9:{
            fsd_common_msgs::CarState state = state_;
            state.car_state.x = -(lidar2rearwheel_ - lr_);
            state.car_state.y = 0;
            state.car_state.theta = 0;
            mpc_controller_.index_ = calculate_closest_point(state, local_trajectory_);
            delta_ = mpc_controller_.createLocalSteeringControlCommand(control_command_.steering_angle.data);
            break;
        }
        case 10:{
            mpc_controller_.index_ = calculate_closest_point(state_, global_trajectory_);
            delta_ = mpc_controller_.createGlobalSteeringControlCommand(control_command_.steering_angle.data);
            break;
        }
        case 11:{
            mpc_controller_.index_ = calculate_closest_new_point(state_, global_trajectory_);
            delta_ = mpc_controller_.createGlobalSteeringControlCommand(control_command_.steering_angle.data);
            break;
        }
        case 12:{
            fsd_common_msgs::CarState state = front_axis_center_state_;
            state.car_state.x = wheel_base_ - lidar2rearwheel_;
            state.car_state.y = 0;
            state.car_state.theta = 0;
            pure_pursuit_front_axis_controller_.index_ = calculate_closest_point(state, local_trajectory_);
            delta_ = pure_pursuit_front_axis_controller_.createLocalSteeringControlCommand(control_command_.steering_angle.data);
            markers_ =  pure_pursuit_front_axis_controller_.markers_;
            break;
        }
        case 13:{
            pure_pursuit_front_axis_controller_.index_ = calculate_closest_point(front_axis_center_state_, global_trajectory_);
            delta_  = pure_pursuit_front_axis_controller_.createGlobalSteeringControlCommand(control_command_.steering_angle.data);
            markers_ =  pure_pursuit_front_axis_controller_.markers_;
            break;
        }
        case 14:{
            pure_pursuit_front_axis_controller_.index_ = calculate_closest_new_point(front_axis_center_state_, global_trajectory_);
            delta_ = pure_pursuit_front_axis_controller_.createGlobalSteeringControlCommand(control_command_.steering_angle.data); 
            markers_ =  pure_pursuit_front_axis_controller_.markers_;
            break;
        }
        case 15:{
            delta_ = ribbon_controller_.createGlobalSteeringControlCommand(control_command_.steering_angle.data); 
            markers_ =  ribbon_controller_.markers_;
            break;
        }
        default :
            ROS_WARN_STREAM("wrong steering_mode!");
    }

    switch (speed_mode){
        case 0:{
            speed_ = pid_controller_.createConstLowSpeedControlCommand();
            break;
        }
        case 1:{
            speed_ = pid_controller_.createConstHighSpeedControlCommand();
            break;
        }
        case 2:{
            speed_ = pid_controller_.createFrictionCircleSpeedControlCommand(control_command_.steering_angle.data);
            break;
        }
        case 3:{
            pid_controller_.index_ =calculate_closest_point(state_, global_trajectory_);
            speed_ = pid_controller_.createSpeedFromPlannningControlCommmand();
            break;
        }
        case 4:{
            pid_controller_.index_ =calculate_closest_new_point(state_, global_trajectory_);
            speed_ = pid_controller_.createSpeedFromPlannningControlCommmand();
            break;
        }
        case 5:{
            //匀加速-开环
            speed_ = pid_controller_.createUniformlyAcceleratedSpeedControlCommand();
            break;
        }
        case 6:{
            //对数加速-开环
            speed_ = pid_controller_.createLogSpeedControlCommand();
            break;
        }
        default :
            ROS_WARN_STREAM("wrong speed_mode!");
    }
}

void Controller::runAlgorithm() {
    //switch cases, the run_mode_ represent the missions
    std::cout << "##################################################" << std::endl; 
    switch (run_mode_){
        case 0:{
            std::cout << "now we are using run mode 0: inspection" << std::endl;
            runInspection();
            break;
        }
        case 1:{
            std::cout << "now we are using run mode 1: ebs_test" << std::endl;
            switch_cases(ebs_test_steering_mode_, ebs_test_speed_mode_);
            break;
        }
        case 2:{
            std::cout << "now we are using run mode 2: acceleration" << std::endl;
            switch_cases(acceleration_steering_mode_, acceleration_speed_mode_);
            break;
        }
        case 3:{
            std::cout << "now we are using run mode 3: skidpad" << std::endl;
            switch_cases(skidpad_steering_mode_, skidpad_speed_mode_);
            break;
        }
        case 4:{
            std::cout << "now we are using run mode 4:trackdrive" << std::endl;
            // switch (decision_and_asState_.whichLap){
            switch (panduan_){

                case 0: case 1:{
                    std::cout << "racing in lap: " << static_cast<float>(decision_and_asState_.whichLap) << std::endl;
                    switch_cases(trackDrive_lap1_steering_mode_, trackDrive_lap1_speed_mode_);
                    break;
                }
                default :{
                    std::cout << "racing in lap: " << static_cast<float>(decision_and_asState_.whichLap) << std::endl;
                    switch_cases(trackDrive_lap2_to_end_steering_mode_, trackDrive_lap2_to_end_speed_mode_);                   
                }  
            }
            break;
        }
        default :{
            ROS_WARN_STREAM("wrong run mode!");
        }
    }
}

void Controller::runInspection() {
    std::cout << "we are doing runInspection" <<std::endl;
    if (res_and_ami_.resState == 0){   
        std::cout << "\nNO \"GO\" RECEIVED YET! " << std::endl;
        std::cout<<"Now return from inspection"<<std::endl;
        delta_ = 0.0;
        speed_ = 0.0;
        return;
    }
    std::cout << "inspection_steering_a_: " << inspection_steering_a_ << "[rad]\ninspection_steering_t_: " << inspection_steering_t_ 
    << "[s]\ninspection_speed_: " << inspection_speed_ << "[m/s]" << std::endl;
    //steering step output
    //double steering_angle = ((std::sin((2 * M_PI / inspection_steering_t_) * ros::Time::now().toSec()) >= 0.0) ? inspection_steering_a_ * 1.0 : inspection_steering_a_ * -1.0)
    //steering sin output
    double steering_angle = inspection_steering_a_ * std::sin((2 * M_PI / inspection_steering_t_) * ros::Time::now().toSec());
    double speed = inspection_speed_ * (std::sin((2 * M_PI / inspection_steering_t_) * ros::Time::now().toSec()) + 1.5);

    delta_ = steering_angle;
    speed_ = speed;
    return;
}

void Controller::calculateVisualization() {
    if (global_trajectory_.poses.size() <= 0){
        std::cout << "the trajectory is empty, delta = 0" << std::endl;
        return;
    }
    // 0. calculate index TODO: when racing in skidpad, we should use calculate_closest_new_point(), rather than calculate_closest_point()
    int index_front_axis, index_rear_axis, index_cog;
    index_front_axis = calculate_closest_point(front_axis_center_state_, global_trajectory_);
    index_rear_axis = calculate_closest_point(rear_axis_center_state_, global_trajectory_);
    index_cog = calculate_closest_point(state_, global_trajectory_);

    // 1. calculate lat error
    double lat_error_front_axis, lat_error_rear_axis, lat_error_cog;

    lat_error_front_axis = fabs(sqrt((front_axis_center_state_.car_state.x - global_trajectory_.poses[index_front_axis].pose.position.x) * (front_axis_center_state_.car_state.x - global_trajectory_.poses[index_front_axis].pose.position.x) + 
        (front_axis_center_state_.car_state.y - global_trajectory_.poses[index_front_axis].pose.position.y) * (front_axis_center_state_.car_state.y - global_trajectory_.poses[index_front_axis].pose.position.y)));
    if (((front_axis_center_state_.car_state.y - global_trajectory_.poses[index_front_axis].pose.position.y) * cos(front_axis_center_state_.car_state.theta) - 
        (front_axis_center_state_.car_state.x - global_trajectory_.poses[index_front_axis].pose.position.x) * sin(front_axis_center_state_.car_state.theta)) < 0)
        lat_error_front_axis = lat_error_front_axis;
    else
        lat_error_front_axis = -lat_error_front_axis;

    lat_error_rear_axis = fabs(sqrt((rear_axis_center_state_.car_state.x - global_trajectory_.poses[index_rear_axis].pose.position.x) * (rear_axis_center_state_.car_state.x - global_trajectory_.poses[index_rear_axis].pose.position.x) + 
        (rear_axis_center_state_.car_state.y - global_trajectory_.poses[index_rear_axis].pose.position.y) * (rear_axis_center_state_.car_state.y - global_trajectory_.poses[index_rear_axis].pose.position.y)));
    if (((rear_axis_center_state_.car_state.y - global_trajectory_.poses[index_rear_axis].pose.position.y) * cos(rear_axis_center_state_.car_state.theta) - 
        (rear_axis_center_state_.car_state.x - global_trajectory_.poses[index_rear_axis].pose.position.x) * sin(rear_axis_center_state_.car_state.theta)) < 0)
        lat_error_rear_axis = lat_error_rear_axis;
    else
        lat_error_rear_axis = -lat_error_rear_axis;

    lat_error_cog = fabs(sqrt((state_.car_state.x - global_trajectory_.poses[index_cog].pose.position.x) * (state_.car_state.x - global_trajectory_.poses[index_cog].pose.position.x) + 
        (state_.car_state.y - global_trajectory_.poses[index_cog].pose.position.y) * (state_.car_state.y - global_trajectory_.poses[index_cog].pose.position.y)));
    if (((state_.car_state.y - global_trajectory_.poses[index_cog].pose.position.y) * cos(state_.car_state.theta) - 
        (state_.car_state.x - global_trajectory_.poses[index_cog].pose.position.x) * sin(state_.car_state.theta)) < 0)
        lat_error_cog = lat_error_cog;
    else
        lat_error_cog = -lat_error_cog;

    // 2. calculate yaw error
    double yaw_error_front_axis, yaw_error_rear_axis, yaw_error_cog;

    yaw_error_front_axis = global_trajectory_.poses[index_front_axis].pose.orientation.z - front_axis_center_state_.car_state.theta;
    if (yaw_error_front_axis > M_PI)
        yaw_error_front_axis -= 2 * M_PI;
    if (yaw_error_front_axis < -1 * M_PI)
        yaw_error_front_axis += 2 * M_PI;

    yaw_error_rear_axis = global_trajectory_.poses[index_rear_axis].pose.orientation.z - rear_axis_center_state_.car_state.theta;
    if (yaw_error_rear_axis > M_PI)
        yaw_error_rear_axis -= 2 * M_PI;
    if (yaw_error_rear_axis < -1 * M_PI)
        yaw_error_rear_axis += 2 * M_PI;

    yaw_error_cog = global_trajectory_.poses[index_cog].pose.orientation.z - state_.car_state.theta;
    if (yaw_error_cog > M_PI)
        yaw_error_cog -= 2 * M_PI;
    if (yaw_error_cog < -1 * M_PI)
        yaw_error_cog += 2 * M_PI;

    // 3. reference data
    double yaw_ref, curvature_ref, vx_ref;

    yaw_ref = global_trajectory_.poses[index_cog].pose.orientation.z;
    curvature_ref = global_trajectory_.poses[index_cog].pose.orientation.w;
    vx_ref = global_trajectory_.poses[index_cog].pose.position.z;

    // 4. algorithm data
    double yaw_rate_pre;

    yaw_rate_pre = state_.car_state_dt.car_state_dt.x / wheel_base_ * control_command_.steering_angle.data;

    // 5. yaw rate error
    double yaw_rate_error;

    yaw_rate_error = yaw_rate_pre - state_.car_state_dt.car_state_dt.theta;

    // 6. v error
    double vx_error;
    vx_error = vx_ref - state_.car_state_dt.car_state_dt.x;

    // 7. steering error
    double steering_error;
    steering_error = steering_without_compensate_ - steering_return_.steering_angle.data;

    // 7. fill the msg TODO:there are a lot of data that had not been calculated yet
    visualization_data_.lat_error_front_axis = static_cast<float>(lat_error_front_axis);
    visualization_data_.lat_error_rear_axis = static_cast<float>(lat_error_rear_axis);
    visualization_data_.lat_error_cog = static_cast<float>(lat_error_cog);
    visualization_data_.yaw_error_front_axis = static_cast<float>(yaw_error_front_axis);
    visualization_data_.yaw_error_rear_axis = static_cast<float>(yaw_error_rear_axis);
    visualization_data_.yaw_error_cog = static_cast<float>(yaw_error_cog);
    visualization_data_.yaw_ref = static_cast<float>(yaw_ref);
    visualization_data_.vx_ref = static_cast<float>(vx_ref);
    visualization_data_.curvature_ref = static_cast<float>(curvature_ref);
    visualization_data_.yaw_rate_pre = static_cast<float>(yaw_rate_pre);
    visualization_data_.yaw_rate_error = static_cast<float>(yaw_rate_error);
    visualization_data_.steering_without_compensation = static_cast<float>(steering_without_compensate_);
    visualization_data_.steering_error = static_cast<float>(steering_error);

}

int Controller::calculate_closest_point(fsd_common_msgs::CarState &state, nav_msgs::Path &trajectory) {
    // std::cout << "\nwe are doing Controller::calculate_closest_point"<<std::endl;

    int index;
    int trajectory_length = trajectory.poses.size();

    if (trajectory_length <= 0){
        // std::cout << "the trajectory is empty, index = 0" << std::endl;
        return index = 0;
    }

    const auto it_index = std::min_element(trajectory.poses.begin(), trajectory.poses.end(), 
                                                             [&](const geometry_msgs::PoseStamped &a,
                                                                 const geometry_msgs::PoseStamped &b) {
                                                                 const double da = std::hypot(state.car_state.x - a.pose.position.x,
                                                                                              state.car_state.y - a.pose.position.y);
                                                                 const double db = std::hypot(state.car_state.x - b.pose.position.x,
                                                                                              state.car_state.y - b.pose.position.y);
                                                                 return da < db;
                                                             });
    index  = std::distance(trajectory.poses.begin(), it_index);

    // std::cout << "index:" << index << std::endl;
    return index;
}

int Controller::calculate_closest_new_point(fsd_common_msgs::CarState &state, nav_msgs::Path &trajectory) {
    // std::cout << "\nwe are doing Controller::calculate_closest_new_point"<<std::endl;

    int index;
    int trajectory_length = trajectory.poses.size();

    if (trajectory_length <= 0){
        // std::cout << "the trajectory is empty, index = 0" << std::endl;
        return index = 0;
    }
    // only search a limited area, which is +_60 * 0.1 = +_6m 
    const auto it_index = std::min_element(std::max(trajectory.poses.begin() + last_index_ - 60, trajectory.poses.begin()), std::min(trajectory.poses.begin() + last_index_ + 60, trajectory.poses.end()), 
                                                             [&](const geometry_msgs::PoseStamped &a,
                                                                 const geometry_msgs::PoseStamped &b) {
                                                                 const double da = std::hypot(state.car_state.x - a.pose.position.x,
                                                                                              state.car_state.y - a.pose.position.y);
                                                                 const double db = std::hypot(state.car_state.x - b.pose.position.x,
                                                                                              state.car_state.y - b.pose.position.y);
                                                                 return da < db;
                                                             });
    index  = std::distance(trajectory.poses.begin(), it_index);

    // std::cout << "index:" << index << std::endl;

    last_index_ = static_cast<int>(index); //save the index of the old closest point
    return index;
}



} //namespace ns_controller
