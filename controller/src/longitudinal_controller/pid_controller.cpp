/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#include "longitudinal_controller/pid_controller.hpp"
namespace ns_controller {

double PidController::createConstLowSpeedControlCommand() {
    std::cout << "\nwe are doing PidController::createConstLowSpeedControlCommand" 
    << "\nconst_speed_low: " << const_speed_low_ << "   speed_k: " << speed_k_ << std::endl;

    double v = state_.car_state_dt.car_state_dt.x;
    // calculate
    speed_ = v + speed_k_ * (const_speed_low_ - v);

    // limit the speed command
    speed_ = std::min(speed_, const_speed_low_ + 1);

    // FOR FSSSIM ONLY
    // speed_ = 0.2 * (const_speed_low_ - v);

    std::cout << "v: " << v << "   speed: " << speed_ << std::endl;
    return speed_;
}

double PidController::createConstHighSpeedControlCommand() {
    std::cout << "\nwe are doing PidController::createConstHighSpeedControlCommand" 
    << "\nconst_speed_high_: " << const_speed_high_ << "   speed_k: " << speed_k_ << std::endl;

    double v = state_.car_state_dt.car_state_dt.x;
    // calculate
    speed_ = v + speed_k_ * (const_speed_high_ - v);

    // limit the speed command
    speed_ = std::min(speed_, const_speed_high_ + 1);

    // FOR FSSSIM ONLY
    // speed_ = 0.2 * (const_speed_high_ - v);

    std::cout << "v: " << v << "   speed: " << speed_ << std::endl;
    return speed_;
}

double PidController::createFrictionCircleSpeedControlCommand(double older_steering_command) {
    std::cout << "\nwe are doing PidController::createFrictionCircleSpeedControlCommand"
    << "\nfriction_circle_max_speed: " << friction_circle_max_speed_ << "   friction_circle_max_ay: " << friction_circle_max_ay_ << std::endl;

    // this is just a simple speed local planning for test
    double v = state_.car_state_dt.car_state_dt.x;
    double r;
    // make the input: delta as smooth as possible
    old_steering_command_set.push_back(abs(older_steering_command));
    old_steering_command_set.erase(old_steering_command_set.begin());
    double temp = 0.0;
    for (int i = 0 ; i < old_steering_command_set.size() ; ++i){
        temp += old_steering_command_set[i];
    }
    double delta =  temp / old_steering_command_set.size();

    // simple steering radius and target speed
    r = sqrt(imu2rearwheel_ * imu2rearwheel_ + (wheel_base_ / std::tan(delta)) * (wheel_base_ / std::tan(delta)));
    target_speed_ = std::min(sqrt(friction_circle_max_ay_ * r), friction_circle_max_speed_);

    // calculate
    speed_ = v + speed_k_ * (target_speed_ - v);

    // limit the speed command
    speed_ = std::min(speed_, target_speed_ + 1);

    // FOR FSSSIM ONLY
    // speed_ = 0.2 * (target_speed_ - v);

    std::cout << "v: " << v << "   target_speed: " << target_speed_ << "   speed: " << speed_ << std::endl;
    return speed_;
}

double PidController::createSpeedFromPlannningControlCommmand() {
    std::cout << "\nwe are doing PidController::createSpeedFromPlannningControlCommmand"
    << "\nindex_:" << index_ << "   speed_from_planning_max_speed: " << speed_from_planning_max_speed_ << std::endl;

    int trajectory_length = global_trajectory_.poses.size();
    if (trajectory_length <= 0){
        std::cout << "the trajectory is empty, speed = 0" << std::endl;
        return speed_ = 0;
    }

    double v = state_.car_state_dt.car_state_dt.x;
    int idx = ceil(speed_from_planning_lookforward_dist_ / path_resolution_);
    int index = index_ + idx;
    target_speed_ = global_trajectory_.poses[index].pose.position.z;

    //temporary setting
    if (index >= trajectory_length - 20){
        target_speed_ = 0;
    }

    // calculate
    speed_ = v + speed_k_ * (target_speed_ - v);

    // limit the speed command
    speed_ = std::min(speed_, target_speed_ + 1);

    // FOR FSSSIM ONLY
    // speed_ = 0.2 * (target_speed_ - v);

    std::cout << "v: " << v << "   target_speed: " << target_speed_ << "   speed: " << speed_ << std::endl;

    return speed_;
}

double PidController::createUniformlyAcceleratedSpeedControlCommand(){
    std::cout << "\nwe are doing PidController::createUniformlyAcceleratedSpeedControlCommand" 
    << "\nmax_acceleration_speed: " << max_linear_acceleration_speed_ << "   acceleration_k: " << acceleration_k_ << std::endl;
    if(resState == 0){
        speed_ = 0;
        std::cout<<"Not receive GO yet , Set speed = 0"<<std::endl;
        return speed_;
    }
    //resstate = 1时说明已经设置了go_receive_time
    if((ros::Time::now().toSec() - go_receive_time_ < 3 )&& resState == 1 ){
        //3秒内不计算速度
        speed_ = 0;
        start_time_ = ros::Time::now().toSec();  //不断更新t=0对应的ros time
        std::cout<<"Wait to start acceleration!!!"<<std::endl;
        return speed_;
    }
    double v = state_.car_state_dt.car_state_dt.x;
    double t = ros::Time::now().toSec() - start_time_; 
    // calculate
    //   v = at  ------------>  speed = acceleration * t
    speed_ = acceleration_k_ * t;

    // limit the speed command
    speed_ = std::min(speed_, max_linear_acceleration_speed_+ 1);

    // FOR FSSSIM ONLY
    // speed_ = 0.2 * (const_speed_low_ - v);

    std::cout << "v: " << v << "   speed: " << speed_ << std::endl;
    return speed_;
}


double PidController::createLogSpeedControlCommand(){
    std::cout << "\nwe are doing PidController::createUniformlyAcceleratedSpeedControlCommand" 
    << "\nmax__logacceleration_speed: " << max_log_acceleration_speed_ << "   acceleration_k: " << acceleration_k_ << std::endl;
    if(resState == 0){
        speed_ = 0;
        std::cout<<"Not receive GO yet , Set speed = 0"<<std::endl;
        return speed_;
    }
    if(ros::Time::now().toSec() - go_receive_time_ < 3){
        //3秒内不计算速度
        speed_ = 0;
        start_time_ = ros::Time::now().toSec();  //不断更新t=0对应的ros time
        std::cout<<"Wait to start acceleration!!!"<<std::endl;
        return speed_;
    }
    double v = state_.car_state_dt.car_state_dt.x;
    double t = ros::Time::now().toSec() - start_time_; 
    // calculate
    //   v = at  ------------>  speed = acceleration * t
    speed_ = log(t + 1);

    // limit the speed command
    speed_ = std::min(speed_, max_log_acceleration_speed_+ 1);

    // FOR FSSSIM ONLY
    // speed_ = 0.2 * (const_speed_low_ - v);

    std::cout << "v: " << v << "   speed: " << speed_ << std::endl;
    return speed_;
}

void PidController::setGoReceiveTime(double go_receive_time){
    go_receive_time_ = go_receive_time;
    resState = 1;
    return;
}


double PidController::createCosStopControlCommand(double stop_time, double speed){
    double t = ros::Time::now().toSec() - stop_time;
    speed_ = 0;
    return speed_;
}

double PidController::createLinearStopControlCommand(double stop_time, double speed){
    double t = ros::Time::now().toSec() - stop_time;

    speed_ = linear_k_ * t + speed;
    speed_ = std::max(0.0,speed_);
    speed_ = std::min(speed_ ,  speed + 0.5); //因为linear_k_是复数，因此这句一般没什么用，只是为了防止linear_k不小心输入了正数
    return speed_;
}

}//namespace ns_controller