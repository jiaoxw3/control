# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.25

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Produce verbose output by default.
VERBOSE = 1

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build

# Include any dependencies generated for this target.
include test_problems/CMakeFiles/d_dense_qp.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include test_problems/CMakeFiles/d_dense_qp.dir/compiler_depend.make

# Include the progress variables for this target.
include test_problems/CMakeFiles/d_dense_qp.dir/progress.make

# Include the compile flags for this target's objects.
include test_problems/CMakeFiles/d_dense_qp.dir/flags.make

test_problems/CMakeFiles/d_dense_qp.dir/test_d_dense.c.o: test_problems/CMakeFiles/d_dense_qp.dir/flags.make
test_problems/CMakeFiles/d_dense_qp.dir/test_d_dense.c.o: /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/test_problems/test_d_dense.c
test_problems/CMakeFiles/d_dense_qp.dir/test_d_dense.c.o: test_problems/CMakeFiles/d_dense_qp.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object test_problems/CMakeFiles/d_dense_qp.dir/test_d_dense.c.o"
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build/test_problems && /usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT test_problems/CMakeFiles/d_dense_qp.dir/test_d_dense.c.o -MF CMakeFiles/d_dense_qp.dir/test_d_dense.c.o.d -o CMakeFiles/d_dense_qp.dir/test_d_dense.c.o -c /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/test_problems/test_d_dense.c

test_problems/CMakeFiles/d_dense_qp.dir/test_d_dense.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/d_dense_qp.dir/test_d_dense.c.i"
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build/test_problems && /usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/test_problems/test_d_dense.c > CMakeFiles/d_dense_qp.dir/test_d_dense.c.i

test_problems/CMakeFiles/d_dense_qp.dir/test_d_dense.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/d_dense_qp.dir/test_d_dense.c.s"
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build/test_problems && /usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/test_problems/test_d_dense.c -o CMakeFiles/d_dense_qp.dir/test_d_dense.c.s

# Object files for target d_dense_qp
d_dense_qp_OBJECTS = \
"CMakeFiles/d_dense_qp.dir/test_d_dense.c.o"

# External object files for target d_dense_qp
d_dense_qp_EXTERNAL_OBJECTS =

test_problems/d_dense_qp: test_problems/CMakeFiles/d_dense_qp.dir/test_d_dense.c.o
test_problems/d_dense_qp: test_problems/CMakeFiles/d_dense_qp.dir/build.make
test_problems/d_dense_qp: libhpipm.a
test_problems/d_dense_qp: /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/lib/lib/libblasfeo.a
test_problems/d_dense_qp: test_problems/CMakeFiles/d_dense_qp.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking C executable d_dense_qp"
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build/test_problems && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/d_dense_qp.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
test_problems/CMakeFiles/d_dense_qp.dir/build: test_problems/d_dense_qp
.PHONY : test_problems/CMakeFiles/d_dense_qp.dir/build

test_problems/CMakeFiles/d_dense_qp.dir/clean:
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build/test_problems && $(CMAKE_COMMAND) -P CMakeFiles/d_dense_qp.dir/cmake_clean.cmake
.PHONY : test_problems/CMakeFiles/d_dense_qp.dir/clean

test_problems/CMakeFiles/d_dense_qp.dir/depend:
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/test_problems /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build/test_problems /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/hpipm/build/test_problems/CMakeFiles/d_dense_qp.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : test_problems/CMakeFiles/d_dense_qp.dir/depend

