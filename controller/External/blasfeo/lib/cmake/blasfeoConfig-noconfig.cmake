#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "blasfeo" for configuration ""
set_property(TARGET blasfeo APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(blasfeo PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_NOCONFIG "C"
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libblasfeo.a"
  )

list(APPEND _cmake_import_check_targets blasfeo )
list(APPEND _cmake_import_check_files_for_blasfeo "${_IMPORT_PREFIX}/lib/libblasfeo.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
