# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.25

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Produce verbose output by default.
VERBOSE = 1

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build

# Include any dependencies generated for this target.
include examples/CMakeFiles/getting_started.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include examples/CMakeFiles/getting_started.dir/compiler_depend.make

# Include the progress variables for this target.
include examples/CMakeFiles/getting_started.dir/progress.make

# Include the compile flags for this target's objects.
include examples/CMakeFiles/getting_started.dir/flags.make

examples/CMakeFiles/getting_started.dir/getting_started.c.o: examples/CMakeFiles/getting_started.dir/flags.make
examples/CMakeFiles/getting_started.dir/getting_started.c.o: /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/examples/getting_started.c
examples/CMakeFiles/getting_started.dir/getting_started.c.o: examples/CMakeFiles/getting_started.dir/compiler_depend.ts
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building C object examples/CMakeFiles/getting_started.dir/getting_started.c.o"
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build/examples && /usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -MD -MT examples/CMakeFiles/getting_started.dir/getting_started.c.o -MF CMakeFiles/getting_started.dir/getting_started.c.o.d -o CMakeFiles/getting_started.dir/getting_started.c.o -c /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/examples/getting_started.c

examples/CMakeFiles/getting_started.dir/getting_started.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/getting_started.dir/getting_started.c.i"
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build/examples && /usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -E /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/examples/getting_started.c > CMakeFiles/getting_started.dir/getting_started.c.i

examples/CMakeFiles/getting_started.dir/getting_started.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/getting_started.dir/getting_started.c.s"
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build/examples && /usr/bin/cc $(C_DEFINES) $(C_INCLUDES) $(C_FLAGS) -S /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/examples/getting_started.c -o CMakeFiles/getting_started.dir/getting_started.c.s

# Object files for target getting_started
getting_started_OBJECTS = \
"CMakeFiles/getting_started.dir/getting_started.c.o"

# External object files for target getting_started
getting_started_EXTERNAL_OBJECTS =

examples/getting_started: examples/CMakeFiles/getting_started.dir/getting_started.c.o
examples/getting_started: examples/CMakeFiles/getting_started.dir/build.make
examples/getting_started: libblasfeo.a
examples/getting_started: examples/CMakeFiles/getting_started.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking C executable getting_started"
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build/examples && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/getting_started.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
examples/CMakeFiles/getting_started.dir/build: examples/getting_started
.PHONY : examples/CMakeFiles/getting_started.dir/build

examples/CMakeFiles/getting_started.dir/clean:
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build/examples && $(CMAKE_COMMAND) -P CMakeFiles/getting_started.dir/cmake_clean.cmake
.PHONY : examples/CMakeFiles/getting_started.dir/clean

examples/CMakeFiles/getting_started.dir/depend:
	cd /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/examples /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build/examples /home/scutracing/scutaracing_driverless_2022/src/6_control/controller/External/blasfeo/build/examples/CMakeFiles/getting_started.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : examples/CMakeFiles/getting_started.dir/depend

