/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#ifndef RIBBON_CONTROLLER_HPP
#define RIBBON_CONTROLLER_HPP

#include "ros/ros.h"
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "fsd_common_msgs/ConeDetections.h"
#include "fsd_common_msgs/Cone.h"
#include "nav_msgs/Path.h"
#include "std_msgs/String.h"
#include "visualization_msgs/MarkerArray.h"

namespace ns_controller {

class RibbonController {

public:
    //methods
    //double createLocalSteeringControlCommand(double older_steering_command);
    int find_nearCone(fsd_common_msgs::ConeDetections ConeDetections);
    void find_angle_rangle(fsd_common_msgs::Cone Cone_left,fsd_common_msgs::Cone Cone_right);
    double createGlobalSteeringControlCommand(double older_steering_command);
    double createSpeedFromControlCommmand();

    //visualize
    visualization_msgs::MarkerArray createMarkers(double x_l, double y_l, double x_r, double y_r, double x_l2, double y_l2, double x_r2, double y_r2, std::string frame_str) const;
 
    //common parameters
    double path_resolution_ = 0.1; //m
    double lidar2rearwheel_ = 2.4; //m
    double imu2rearwheel_ = 1.29;  //m, use to transfor imu state_ to rear_axis_center_state_
    double steering_change_rate_limit_ = 0.15; //rad/s
    double steering_limit_ = 0.3;  //rad, means the max steering angle range is [-steering_limit_, steering_limit_]
    double wheel_base_ = 1.575;    //m
    double car_width = 1.28; //m
    double gra_acc=9.80; //m/s/s
    double max_brake_a=15; //m/ss

    //algorithm parameters
    double steering_p_global_ = 1.0;
    double ribbon_max_speed_=10.;
    double circle_speed_k=1.;
    double brake_speed_k=1./3.;
    double speed_k_=0.5;
    

    //data
    fsd_common_msgs::ConeDetections ConeDetections_;
    fsd_common_msgs::ConeDetections ConeDetections_left_;
    fsd_common_msgs::ConeDetections ConeDetections_right_;
    fsd_common_msgs::CarState       front_axis_center_state_;
    fsd_common_msgs::CarState       rear_axis_center_state_;
    fsd_common_msgs::CarStateDt     velocity_;
    fsd_common_msgs::ControlCommand control_command_;
    visualization_msgs::MarkerArray markers_;
    fsd_common_msgs::Cone tempCone;

private:

    //delta parameters
    int l_index_;
    int r_index_;
    int angle_index;
    int lenLeft_Conedetections;
    int lenRight_Conedetections;
    double center_deflection_angle_;
    double l_delta_;
    double r_delta_;
    double temp_l_delta_;
    double temp_r_delta_;
    double beta_est_ = 0;
    double alpha_ = 0;
    double delta_ = 0;


    //speed parameters
    double speed_ = 0;
    double target_speed_ =0;
    double circle_speed_;
    double brake_speed_;
    std::vector<double> old_steering_command_set = {0.0,0.0};
};
} //namespace ns_controller
#endif //RIBBON_CONTROLLER_HPP