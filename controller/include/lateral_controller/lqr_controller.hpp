/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/
#ifndef LQR_CONTROLLER_HPP
#define LQR_CONTROLLER_HPP

#include <ros/ros.h>
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/Map.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "nav_msgs/Path.h"
#include "std_msgs/String.h"
#include <Eigen/Core>
#include <Eigen/LU>
#include "tool/linear_quadratic_regulator.hpp"

namespace ns_controller {

class LqrController{

public:
    //
    const int preview_window_ = 0; // number of control cycles look ahead (preview controller)
    const int basic_state_size_ = 4; // lateral error, lateral error rate, heading error, heading error rate
    const int matrix_size = basic_state_size_ + preview_window_;

    //methods
    double createLocalSteeringControlCommand();
    double createGlobalSteeringControlCommand();

    //common parameters
    double cf_ = 62280.512;           //N/rad
    double cr_ = 62280.512;           //N/rad
    double lf_ = 0.919;               //m
    double lr_ = 0.656;               //m
    double total_mass_ = 290;         //kg;
    double iz_ = 175.735;             //kg/m^2
    double path_resolution_ = 0.1;    //m
    double lidar2rearwheel_ = 2.4;    //m
    double imu2rearwheel_ = 1.29;     //m
    double steering_change_rate_limit_ = 0.15; //rad/s
    double steering_limit_ = 0.3;              //rad, means the max steering angle range is [-steering_limit_, steering_limit_]
    double wheel_base_ = 1.575;                //m

    //algorithm parameters
    double Ts_ = 0.01;                         //s, the time gap of discretization
    double ref_k_limit_ = 0.12;                //1/m, limit the curvature of the reference trajectory
    int lqr_max_iteration_ = 200;              // parameters for lqr solver; number of iterations
    double lqr_tolerance_ = 0.008;             // parameters for lqr solver; threshold for computation
    double look_forward_distance_local_ = 0.1;   //only for local steering control
    double feedforward_distance_local_ = 1;    //m
    Eigen::MatrixXd matrix_r_local_ = Eigen::MatrixXd::Identity(1, 1);
    Eigen::MatrixXd matrix_q_local_ = Eigen::MatrixXd::Zero(matrix_size, matrix_size);
    double look_forward_distance_global_ = 0.1;  //m
    double feedforward_distance_global_ = 1;   //m
    Eigen::MatrixXd matrix_r_global_ = Eigen::MatrixXd::Identity(1, 1);
    Eigen::MatrixXd matrix_q_global_ = Eigen::MatrixXd::Zero(matrix_size, matrix_size);

    //temporary parameters 
    int index_ = 0;

    //data
    nav_msgs::Path   global_trajectory_;
    nav_msgs::Path   local_trajectory_;
    fsd_common_msgs::CarState       state_;
    fsd_common_msgs::CarState       front_axis_center_state_;
    fsd_common_msgs::CarStateDt     velocity_;
    fsd_common_msgs::ControlCommand control_command_;
    
private:
    //temporary parameters 
    double delta_ = 0.0;

    //methods
    // 更新状态矩阵
    void updateState(int index, fsd_common_msgs::CarState state, nav_msgs::Path trajectory);
    // 更新矩阵A
    void updateMatrix(fsd_common_msgs::CarState state);
    // 矩阵离散化处理
    void updateMatrixCompound();
    // 前馈角的计算
    double computeFeedForward(int &next_index, fsd_common_msgs::CarState state, nav_msgs::Path trajectory) const;

    //
    Eigen::MatrixXd matrix_a_ = Eigen::MatrixXd::Zero(basic_state_size_, basic_state_size_);
    Eigen::MatrixXd matrix_ad_ = Eigen::MatrixXd::Zero(basic_state_size_, basic_state_size_);
    Eigen::MatrixXd matrix_adc_ = Eigen::MatrixXd::Zero(matrix_size, matrix_size);
    Eigen::MatrixXd matrix_a_coeff_ = Eigen::MatrixXd::Zero(matrix_size, matrix_size);
    Eigen::MatrixXd matrix_b_ = Eigen::MatrixXd::Zero(basic_state_size_, 1);
    Eigen::MatrixXd matrix_bd_ = Eigen::MatrixXd::Zero(basic_state_size_, 1);
    Eigen::MatrixXd matrix_bdc_ = Eigen::MatrixXd::Zero(matrix_size, 1);
    Eigen::MatrixXd matrix_state_ = Eigen::MatrixXd::Zero(matrix_size, 1);
    Eigen::MatrixXd matrix_k_ = Eigen::MatrixXd::Zero(1, matrix_size);
};
} //namespace ns_controller
#endif //LQR_CONTROLLER_HPP