/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/
#ifndef MPC_CONTROLLER_HPP
#define MPC_CONTROLLER_HPP

#include <ros/ros.h>
#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/PolygonStamped.h>
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/Map.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "geometry_msgs/Point.h"
#include "nav_msgs/Path.h"
#include "std_msgs/String.h"

#include <array>
#include <memory>
#include "Eigen/Dense"
#include "tool/interface/hpipm_interface.hpp"

namespace ns_controller
{
    const int MPC_NP = 5; //prediction horizon and control horizon is the same
    const int MPC_NPC = 2; //number of inequality constraints

    //set some name for simplify
    using MPC_ErrorVec = Eigen::Matrix<double, 4, 1>;
    using MPC_MAT_Ad = Eigen::Matrix4d;
    using MPC_MAT_Bd = Eigen::Vector4d;
    using MPC_MAT_gd = Eigen::Vector4d;

    using MPC_ATILDE = Eigen::Matrix<double, 5 * MPC_NP, 5>;
    using MPC_BTILDE = Eigen::Matrix<double, 5 * MPC_NP, MPC_NP>;
    using MPC_gTILDE = Eigen::Matrix<double, 5 * MPC_NP, 1>;

    using MPC_Q = Eigen::Matrix<double, 5, 5>;
    using MPC_Q_TILDE = Eigen::Matrix<double, 5 * MPC_NP, 5 * MPC_NP>;
    using MPC_R = Eigen::Matrix<double, 1, 1>;
    using MPC_R_TILDE = Eigen::Matrix<double, MPC_NP, MPC_NP>;

    using MPC_H = Eigen::Matrix<double, MPC_NP, MPC_NP>;
    using MPC_G = Eigen::Matrix<double, MPC_NP, 1>;
    using MPC_A = Eigen::Matrix<double, MPC_NP, MPC_NP>;
    using MPC_b = Eigen::Matrix<double, MPC_NP, 1>;
    using MPC_BU = Eigen::Matrix<double, MPC_NP, 1>;

    struct MpcStateTransMat
    {
        MPC_ATILDE Ad_bar_tilde;
        MPC_BTILDE Bd_bar_tilde;
        MPC_gTILDE gd_bar_tilde;
    };

    struct QuadProgMat
    {
        // Cost mat
        MPC_H H;
        MPC_G G;
        // inequality constraints
        // b_low <= AX <= b_up
        MPC_A A;
        MPC_b b_low;
        MPC_b b_up;
        // boundary of input
        MPC_BU LB_U;
        MPC_BU UB_U;
    };

    class MpcController{

    public:
        //methods
        double createLocalSteeringControlCommand(double older_steering_command);
        double createGlobalSteeringControlCommand(double older_steering_command);

        //common parameters
        double cf_ = 62280.512;                       //N/rad
        double cr_ = 62280.512;                       //N/rad
        double lf_ = 0.919;               //m
        double lr_ = 0.656;               //m
        double total_mass_ = 290;         //kg;
        double iz_ = 175.735;             //kg/m^2
        double path_resolution_ = 0.1;    //m
        double lidar2rearwheel_ = 2.4;  //m
        double imu2rearwheel_ = 1.29;     //m
        double steering_change_rate_limit_ = 0.15; //rad/s
        double steering_limit_ = 0.3;              //rad, means the max steering angle range is [-steering_limit_, steering_limit_]
        double wheel_base_ = 1.575;                //m

        //algorithm parameters
        double Ts_ = 0.1;                 //s, the time gap of discretization
        double ref_k_limit_ = 0.12;       //1/m, limit the curvature of the reference trajectory

        //data
        nav_msgs::Path   global_trajectory_;
        nav_msgs::Path   local_trajectory_;
        nav_msgs::Path   trajectory_;
        fsd_common_msgs::CarState       state_;
        fsd_common_msgs::CarState       rear_axis_center_state_;
        fsd_common_msgs::CarStateDt     velocity_;
        fsd_common_msgs::ControlCommand control_command_;

        // Cost Matrix
        Eigen::Matrix<double, 4, 4> mat_q_local_ = Eigen::Matrix<double, 4, 4>::Zero();
        Eigen::Matrix<double, 1, 1> mat_r_local_ = Eigen::Matrix<double, 1, 1>::Zero();
        Eigen::Matrix<double, 4, 4> mat_q_global_ = Eigen::Matrix<double, 4, 4>::Zero();
        Eigen::Matrix<double, 1, 1> mat_r_global_ = Eigen::Matrix<double, 1, 1>::Zero();

        //temporary parameters 
        int index_ = 0;
        Eigen::Matrix<double, 5 * MPC_NP, 1> ref_trajectory_matrix_ = Eigen::Matrix<double, 5 * MPC_NP, 1>::Zero();

    private:
        //temporary parameters 
        double v_ = 0;
        int old_index_ = 1;
        double delta_ = 0.0;
        double old_delta_ = 0.0;

        // QP solver interface
        //std::unique_ptr<SolverInterface> interface_;
        HpipmInterface interface_;

        // State Transition Matrix
        Eigen::Matrix<double, 5, 5> Ad_bar_ = Eigen::Matrix<double, 5, 5>::Zero();
        Eigen::Matrix<double, 5, 1> Bd_bar_ = Eigen::Matrix<double, 5, 1>::Zero();
        Eigen::Matrix<double, 5, 1> gd_bar_ = Eigen::Matrix<double, 5, 1>::Zero();

        // Save Ad^n
        std::array<Eigen::Matrix<double, 5, 5>, MPC_NP> Ad_bar_power_;

        //methods
        MPC_ErrorVec getMPCErrorMatrix(int index, nav_msgs::Path trajectory, fsd_common_msgs::CarState state);
        void updateMPCMatrixABCBar(int index, nav_msgs::Path trajectory, fsd_common_msgs::CarState state);
        MpcStateTransMat getMPCConfusionMatrix();
        QuadProgMat getQuadProgMatrix(const MpcStateTransMat &mpc_lin_mat, const MPC_ErrorVec &error_vec, Eigen::Matrix<double, 4, 4> mat_q, Eigen::Matrix<double, 1, 1> mat_r) const;
    };
} // namespace ns_controller

#endif // MPC_CONTROLLER_HPP