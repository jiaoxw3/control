/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#ifndef PURE_PURSUIT_CONTROLLER_HPP
#define PURE_PURSUIT_CONTROLLER_HPP

#include "ros/ros.h"
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "nav_msgs/Path.h"
#include "std_msgs/String.h"
#include "visualization_msgs/MarkerArray.h"

namespace ns_controller {

class PurePursuitController {

public:
    //methods
    double createLocalSteeringControlCommand(double older_steering_command);
    double createGlobalSteeringControlCommand(double older_steering_command);

    //visualize
    visualization_msgs::MarkerArray createMarkers(double x_pos, double y_pos, double x_next, double y_next, std::string frame_str) const;
 
    //common parameters
    double path_resolution_ = 0.1; //m
    double lidar2rearwheel_ = 2.4; //m
    double imu2rearwheel_ = 1.29;  //m, use to transfor imu state_ to rear_axis_center_state_
    double steering_change_rate_limit_ = 0.15; //rad/s
    double steering_limit_ = 0.3;  //rad, means the max steering angle range is [-steering_limit_, steering_limit_]
    double wheel_base_ = 1.575;    //m

    //algorithm parameters
    double steering_p_local_ = 1.0;
    double look_forward_distance_local_ = 4.0; //m
    double look_forward_k_local_ = 0.1;
    double steering_p_global_ = 1.0;
    double look_forward_distance_global_ = 4.0; //m
    double look_forward_k_global_ = 0.1;

    //temporary parameters
    int index_ = 0;
    double lat_error_rear_axis_;
    double yaw_error_rear_axis_;

    //data
    nav_msgs::Path   global_trajectory_;
    nav_msgs::Path   local_trajectory_;
    fsd_common_msgs::CarState       state_;
    fsd_common_msgs::CarState       rear_axis_center_state_;
    fsd_common_msgs::CarStateDt     velocity_;
    fsd_common_msgs::ControlCommand control_command_;
    visualization_msgs::MarkerArray markers_;

private:
    //parameters
    double beta_est_ = 0;
    double alpha_ = 0;
    double delta_ = 0;
};
} //namespace ns_controller
#endif //PURE_PURSUIT_CONTROLLER_HPP