/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#ifndef STANLEY_CONTROLLER_HPP
#define STANLEY_CONTROLLER_HPP

#include "ros/ros.h"
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "nav_msgs/Path.h"
#include "std_msgs/String.h"
#include "visualization_msgs/MarkerArray.h"

namespace ns_controller {

class StanleyController {

public:
    //methods
    double createLocalSteeringControlCommand(double older_steering_command);
    double createGlobalSteeringControlCommand(double older_steering_command);

    //visualize
    visualization_msgs::MarkerArray createMarkers(double x_pos, double y_pos, double x_next, double y_next, std::string frame_str) const;


    //common parameters
    double path_resolution_ = 0.1;
    double lidar2rearwheel_ = 2.4;
    double imu2rearwheel_ = 1.29;  // use to transfor imu state_ to rear_axis_center_state_
    double steering_change_rate_limit_ = 0.15; //rad/s
    double steering_limit_ = 0.3; //rad, means the max steering angle range is [-steering_limit_, steering_limit_]
    double wheel_base_ = 1.575;

    //algorithm parameters
    double steering_p_local_ = 0.2;
    double look_forward_distance_local_ = 0;
    double look_forward_k_local_ = 0.1;
    double steering_p_global_ = 0.2;
    double look_forward_distance_global_ = 0;
    double look_forward_k_global_ = 0.1;

    //temporary parameters
    int index_ = 0;

    //data
    nav_msgs::Path   global_trajectory_;
    nav_msgs::Path   local_trajectory_;
    fsd_common_msgs::CarState       state_;
    fsd_common_msgs::CarState       front_axis_center_state_;
    fsd_common_msgs::CarStateDt     velocity_;
    fsd_common_msgs::ControlCommand control_command_;
    visualization_msgs::MarkerArray markers_;

private:
    //temporary parameters
    int index_last_ = 0; //save the index of the old closest point
    double heading_error_ = 0;
    double crosstrack_error_ = 0;
    double lateral_error_angle_ = 0;
    double delta_ = 0;
    double older_steering_command_ = 0;
};
}

#endif //STANLEY_CONTROLLER_HPP