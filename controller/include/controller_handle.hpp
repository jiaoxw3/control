/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#ifndef CONTROLLER_HANDLE_HPP
#define CONTROLLER_HANDLE_HPP

#include "ros/ros.h"
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "fsd_common_msgs/Feedback.h"
#include "nav_msgs/Path.h"
#include "controller.hpp"
#include <sys/time.h>

namespace ns_controller {

class ControllerHandle {

public:
    // Constructor
    explicit ControllerHandle(ros::NodeHandle &nodeHandle);
    // Methods
    void loadParameters();
    void subscribeToTopics();
    void publishToTopics();
    void run();
    void sendControlCommand();
    void sendMarkers();
    void sendVisualization();
    void calculate_compute_time(struct timeval start_time, struct timeval end_time);

    //data
    int node_rate_ = 20;

private:
    ros::NodeHandle nodeHandle_;

    ros::Publisher controlCommandPublisher_;
    ros::Publisher computeTimePublisher_;
    ros::Publisher makersPublisher_;
    ros::Publisher visualizationPublisher_;

    ros::Subscriber slamStateSubscriber_;
    ros::Subscriber slamFrontAxisCenterStateSubscriber_;
    ros::Subscriber slamRearAxisCenterStateSubscriber_;
    ros::Subscriber velocityEstimateSubscriber_;
    ros::Subscriber localTrajectorySubscriber_;
    ros::Subscriber globalTrajectorySubscriber_;
    ros::Subscriber decisionAndAsStateSubscriber_;
    ros::Subscriber resAndAmiSubscriber_;
    ros::Subscriber steeringReturnSubscriber_;
    // ros::Subscriber feedBackSubscriber_;
    ros::Subscriber leftConesSubscriber_;
    ros::Subscriber rightConesSubscriber_;
    

    void velocityEstimateCallback(const fsd_common_msgs::CarStateDt &velocity);
    void localTrajectoryCallback(const nav_msgs::Path &local_trajectory);
    void globalTrajectoryCallback(const nav_msgs::Path &global_trajectory);
    void slamStateCallback(const fsd_common_msgs::CarState &state);
    void slamFrontAxisCenterStateCallback(const fsd_common_msgs::CarState &front_axis_center_state);
    void slamRearAxisCenterStateCallback(const fsd_common_msgs::CarState &rear_axis_center_state);
    void decisionAndAsStateCallback(const fsd_common_msgs::AsState &decision_and_asState);
    void resAndAmiCallback(const fsd_common_msgs::ResAndAmi &res_and_ami);
    void steeringReturnCallback(const fsd_common_msgs::ControlCommand &steering_return);
    // void feedBackCallback(const fsd_common_msgs::Feedback &feedback);
    void leftConesCallback(const fsd_common_msgs::ConeDetections &leftconeDetections);
    void rightConesCallback(const fsd_common_msgs::ConeDetections &rightconeDetections);

    std::string control_command_topic_name_;
    std::string compute_time_topic_name_;
    std::string velocity_estimate_topic_name_;
    std::string slam_state_topic_name_;
    std::string slam_front_axis_center_state_topic_name_;
    std::string slam_rear_axis_center_state_topic_name_;
    std::string local_trajectory_topic_name_;
    std::string global_trajectory_topic_name_;
    std::string asState_topic_name_;
    std::string res_and_ami_topic_name_;
    std::string makers_topic_name_;
    std::string visualization_topic_name_;
    std::string steering_return_topic_name_;
    // std::string feedback_topic_name_;
    std::string cones_topic_name_left;
    std::string cones_topic_name_right;

    Controller controller_;

    fsd_common_msgs::ControlCommand control_command_;
    fsd_common_msgs::AsState decision_and_asState_;
    fsd_common_msgs::ResAndAmi res_and_ami_;
    std_msgs::Float32 control_compute_time;
    visualization_msgs::MarkerArray markers_;
    fsd_common_msgs::Visualization  visualization_data_;
    fsd_common_msgs::ControlCommand steering_return_data_;
    // fsd_common_msgs::Feedback feedback_data_;

    struct timeval start_time_;
    struct timeval end_time_;
};
} //namespace ns_controller

#endif //CONTROLLER_HANDLE_HPP