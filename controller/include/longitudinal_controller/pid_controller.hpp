/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#ifndef PID_CONTROLLER_HPP
#define PID_CONTROLLER_HPP

#include "ros/ros.h"
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/Map.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "nav_msgs/Path.h"
#include "std_msgs/String.h"
#include <vector>
#include <cmath>

namespace ns_controller {

class PidController {

public:
    //methods
    double createConstLowSpeedControlCommand();
    double createConstHighSpeedControlCommand();
    double createFrictionCircleSpeedControlCommand(double older_steering_command);
    double createSpeedFromPlannningControlCommmand();
    double createUniformlyAcceleratedSpeedControlCommand();
    double createLogSpeedControlCommand();

    double createCosStopControlCommand(double stop_time, double speed); // just be a statement
    double createLinearStopControlCommand(double stop_time,double speed);

    //用于acceleration和ebs_test, 接收到GO信号3秒后才开始加速函数
   void setGoReceiveTime(double go_receive_time);
 
    //common parameters
    double path_resolution_ = 0.1; //m
    double lidar2rearwheel_ = 2.4;
    double imu2rearwheel_ = 1.29;  
    double steering_change_rate_limit_ = 0.15; //rad/s
    double wheel_base_ = 1.575; //m

    //algorithm parameters
    double speed_k_ = 0.5;
    double const_speed_low_ = 1;
    double const_speed_high_ = 1;
    double friction_circle_max_speed_ = 1;
    double friction_circle_max_ay_ = 1;    //m/s2
    double speed_from_planning_max_speed_ = 1;
    double speed_from_planning_lookforward_dist_ = 0;

    //##############函数参数----用于起步#################
    //一次函数
    double acceleration_k_ = 1;  // m / s^2
    double max_linear_acceleration_speed_= 1;

    //log函数
    double log_ = 1; 
    double max_log_acceleration_speed_ = 1;
    //temporary parameters
    int index_ = 0;

    //############函数参数----用于停车##################
    double cos_omiga_ = 0.147;        //omiga =  (v * PI / 2   -    PI^2  /  4) / stop_dis，还没实现cos停车，因为用处不大
    double linear_k_ = -6;

    //data
    nav_msgs::Path   global_trajectory_;
    nav_msgs::Path   local_trajectory_;
    fsd_common_msgs::CarState       state_;
    fsd_common_msgs::CarState       rear_axis_center_state_;
    fsd_common_msgs::CarStateDt     velocity_;
    fsd_common_msgs::ControlCommand control_command_;

private:
    //temporary parameters
    std::vector<double> old_steering_command_set = {0.0, 0.0, 0.0, 0.0, 0.0};
    double speed_ = 0;
    double target_speed_ = 0;
    double go_receive_time_ ;
    double start_time_;
    int resState = 0; 
    int i_start_ = 0;
};
}

#endif //PID_CONTROLLER_HPP