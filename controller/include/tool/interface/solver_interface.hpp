#ifndef SOLVER_INTERFACE_HPP_
#define SOLVER_INTERFACE_HPP_

#include <iostream>
#include <vector>

namespace ns_controller
{
    // declaration
    struct QuadProgMat;

    class SolverInterface
    {
    public:
        virtual std::vector<double> solve(QuadProgMat &, int *status) = 0;
        virtual ~SolverInterface()
        {
            std::cout << "Deleting Solver Interface" << std::endl;
        }
    };
} // namespace ns_controller

#endif //SOLVER_INTERFACE_HPP_