#ifndef HPIPM_INTERFACE_HPP_
#define HPIPM_INTERFACE_HPP_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/time.h>

#include <blasfeo_d_aux_ext_dep.h>

#include "hpipm_d_ocp_qp_ipm.h"
#include "hpipm_d_ocp_qp_dim.h"
#include "hpipm_d_ocp_qp.h"
#include "hpipm_d_ocp_qp_sol.h"
#include "hpipm_timing.h"

#include "solver_interface.hpp"

#include <array>
#include <vector>
#include <iterator>

namespace ns_controller
{
    struct QuadProgMat;

    struct HpipmBound
    {
        std::vector<int> idx_u;
        std::vector<int> idx_x;
        std::vector<int> idx_s;
        std::vector<double> lower_bounds_u;
        std::vector<double> upper_bounds_u;
        std::vector<double> lower_bounds_x;
        std::vector<double> upper_bounds_x;
    };

    class HpipmInterface : public SolverInterface
    {
    public:
        std::vector<double> solve(QuadProgMat &, int *status);
        HpipmInterface() { std::cout << "Hpipm Interface Init NOW!" << std::endl; }
        ~HpipmInterface()
        {
            std::cout << "Deleting Hpipm Interface" << std::endl;
        }

    private:
        int nx_[1];         //    -> number of states
        int nu_[1];         //    -> number of inputs
        int nbx_[1];        //   -> number of bounds on x
        int nbu_[1];        //   -> number of bounds on u
        int ng_[1];         //    -> number of polytopic constratins
        int nsbx_[1] = {0}; //   -> number of slacks variables on x
        int nsbu_[1] = {0}; //   -> number of slacks variables on u
        int nsg_[1] = {0};  //   -> number of slacks variables on polytopic constraints

        // LTV dynamics
        // x_k+1 = A_k x_k + B_k u_k + b_k
        double *hA_[1]; //hA[k] = A_k
        double *hB_[1]; //hB[k] = B_k
        double *hb_[1]; //hb[k] = b_k

        // Cost (without soft constraints)
        // min_x,u sum_k=0^N 1/2*[x_k;u_k]^T*[Q_k , S_k; S_k^T , R_k]*[x_k;u_k] + [q_k; r_k]^T*[x_k;u_k]
        double *hQ_[1]; //hQ[k] = Q_k
        double *hS_[1]; //hS[k] = S_k
        double *hR_[1]; //hR[k] = R_k
        double *hq_[1]; //hq[k] = q_k
        double *hr_[1]; //hr[k] = r_k

        // Polytopic constraints
        // g_lower,k <= D_k*x_k + C_k*u_k
        // D_k*x_k + C_k*u_k  <= g_upper,k
        double *hlg_[1]; //hlg[k] =  g_lower,k
        double *hug_[1]; //hug[k] =  g_upper,k
        double *hC_[1];  //hC[k] = C_k
        double *hD_[1];  //hD[k] = D_k

        // General bounds
        // x_lower,k <= x_k <= x_upper,k
        // hidxbx can be used to select bounds on a subset of states
        int *hidxbx_[1];  // hidxbx[k] = {0,1,2,...,nx} for bounds on all inputs and states
        double *hlbx_[1]; // x_lower,k
        double *hubx_[1]; //x_upper,k
        // u_lower,k <= u_k <=  u_upper,k
        // hidxbu can be used to select bounds on a subset of inputs
        int *hidxbu_[1];  // hidxbuk] = {0,1,2,...,nu} for bounds on all inputs and states
        double *hlbu_[1]; // u_lower,k
        double *hubu_[1]; // u_upper,k

        // Cost (only soft constriants)
        // s_lower,k -> slack variable of lower polytopic constraint (3) + lower bounds
        // s_upper,k -> slack variable of upper polytopic constraint (4) + upper bounds
        // min_x,u sum_k=0^N 1/2*[s_lower,k;s_upper,k]^T*[Z_lower,k , 0; 0 , Z_upper,k]*[s_lower,k;s_upper,k] + [z_lower,k; z_upper,k]^T*[s_lower,k;s_upper,k]
        double **hZl_ = nullptr; // hZl[k] = Z_lower,k
        double **hZu_ = nullptr; // hZu[k] = Z_upper,k
        double **hzl_ = nullptr; // hzl[k] = z_lower,k
        double **hzu_ = nullptr; // hzu[k] = z_upper,k

        // Bounds on the soft constraint multipliers
        // s_lower,k >= s_lower,bound,k
        // s_upper,k >= s_upper,bound,k
        double **hlls_ = nullptr;
        double **hlus_ = nullptr;
        // index of the bounds and constraints that are softened
        // order is not really clear
        int **hidxs_ = nullptr;

        HpipmBound hpipm_bounds_;

        void setDynamics();
        void setCost(QuadProgMat &);
        void setBounds(QuadProgMat &);
        void setPolytopicConstraints(QuadProgMat &);
        void setSoftConstraints(QuadProgMat &);

        std::vector<double> Solve(int *status);

        void print_data();
    };
} // namespace ns_controller
#endif // HPIPM_INTERFACE_HPP_