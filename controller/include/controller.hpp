/*
    control module, input: reference trajectory..., output: control commmand
    @chenshaohao
*/

#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include "ros/ros.h"
#include <geometry_msgs/Polygon.h>
#include <geometry_msgs/PolygonStamped.h>
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/Map.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "fsd_common_msgs/Visualization.h"
#include "geometry_msgs/Point.h"
#include "nav_msgs/Path.h"
#include "std_msgs/String.h"
#include "longitudinal_controller/pid_controller.hpp"
#include "lateral_controller/pure_pursuit_controller.hpp"
#include "lateral_controller/stanley_controller.hpp"
#include "lateral_controller/lqr_controller.hpp"
#include "lateral_controller/mpc_controller.hpp"
#include "lateral_controller/pure_pursuit_front_axis_controller.hpp"
#include "lateral_controller/ribbon_controller.hpp"
#include "visualization_msgs/MarkerArray.h"

namespace ns_controller {

class Controller {
public:
    //Constructor
    Controller(ros::NodeHandle &nodeHandle);

    //Setters
    void setLocalTrajectory(const nav_msgs::Path &local_trajectory);
    void setGlobaTrajectory(const nav_msgs::Path &global_trajectory);
    void setState(const fsd_common_msgs::CarState &state);
    void setFrontAxisCenterState(const fsd_common_msgs::CarState &front_axis_center_state);
    void setRearAxixCenterState(const fsd_common_msgs::CarState &rear_axis_center_state);
    void setVelocity(const fsd_common_msgs::CarStateDt &velocity); // velocity of velocity estimation
    void setDecisionAndAsState(const fsd_common_msgs::AsState &decision_and_asState);
    void setResAndAmi(const fsd_common_msgs::ResAndAmi &res_and_ami);
    void setSteeringReturn(const fsd_common_msgs::ControlCommand &steering_return);
    void setLeftConeDetections(const fsd_common_msgs::ConeDetections &leftconeDetections);
    void setRightConeDetections(const fsd_common_msgs::ConeDetections &leftconeDetections);

    //Inspection
    void runInspection();

    //common function
    int calculate_closest_point(fsd_common_msgs::CarState &state, nav_msgs::Path &trajectory); //calculate the index of the closet point, between the car state and the reference path
    int calculate_closest_new_point(fsd_common_msgs::CarState &state, nav_msgs::Path &trajectory); //calculate the index of the closet point, between the car state and the reference path, but limit the search area

    //Others
    void runAlgorithm();
    void switch_cases(int steering_mode, int speed_mode);
    void loadParameters();
    void initialControllers();
    fsd_common_msgs::ControlCommand createControlCommand();
    double geometric_road_constraints(double &delta, fsd_common_msgs::CarState &state, nav_msgs::Path &trajectory);
    std::pair<double, double> createPreDist(double &delta, fsd_common_msgs::CarState &state, nav_msgs::Path &trajectory);
    void calculateVisualization();


    //data
    int node_rate_ = 20;
    visualization_msgs::MarkerArray markers_;
    fsd_common_msgs::Visualization  visualization_data_;

protected:

private:
    //Visualize
    void publishMarkers(double x_pos, double y_pos, double x_next, double y_next, std::string frame_str) const;

    ros::NodeHandle nodeHandle_;

    //Common data
    nav_msgs::Path   global_trajectory_;
    nav_msgs::Path   local_trajectory_;
    fsd_common_msgs::CarState       state_;
    fsd_common_msgs::CarState       front_axis_center_state_;
    fsd_common_msgs::CarState       rear_axis_center_state_;
    fsd_common_msgs::CarStateDt     velocity_;
    fsd_common_msgs::AsState        decision_and_asState_;
    fsd_common_msgs::ResAndAmi      res_and_ami_;
    fsd_common_msgs::ControlCommand control_command_;
    fsd_common_msgs::ControlCommand steering_return_;

    double cf_ = 62280.512;           //N/rad
    double cr_ = 62280.512;           //N/rad
    double lf_ = 0.919;               //m
    double lr_ = 0.656;               //m
    double total_mass_ = 290;         //kg;
    double iz_ = 175.735;             //kg/m^2
    double path_resolution_ = 0.1;    //m
    double lidar2rearwheel_ = 2.4;  //m
    double imu2rearwheel_ = 1.29;     //m
    double steering_change_rate_limit_ = 0.15; //rad/s
    double steering_limit_ = 0.3;              //rad, means the max steering angle range is [-steering_limit_, steering_limit_]
    double wheel_base_ = 1.575;                //m
    double time_length_ = 0.5; //[s]
    double road_width_ = 3; //[m]
    double car_width_ = 1.7; //[m], using bigger data than real car, just for safety

    int run_mode_ = 0;
    int ebs_test_steering_mode_ = 0;
    int ebs_test_speed_mode_ = 0;
    int acceleration_steering_mode_ = 0;
    int acceleration_speed_mode_ = 0;
    int skidpad_steering_mode_ = 0;
    int skidpad_speed_mode_ = 0;
    int trackDrive_lap1_steering_mode_ = 0;
    int trackDrive_lap1_speed_mode_ = 0;
    int trackDrive_lap2_to_end_steering_mode_ = 0;
    int trackDrive_lap2_to_end_speed_mode_ = 0;
    int panduan_ = 1;

    int i_go = 0;
    double go_receive_time = 0;
    int last_index_ = 0; //used in int calculate_closest_new_point(fsd_common_msgs::CarState &state, nav_msgs::Path &trajectory)

    double delta_ = 0;
    double speed_ = 0;
    double steering_without_compensate_;
    double steering_with_compensate_;
    double steering_compensate_gain_ = 0.0;

    //inspection
    float inspection_steering_a_ = 0.1; //[rad]
    float inspection_steering_t_ = 0.5;   //[s]
    float inspection_speed_ = 1;        //[m/s]

    //stop method
    int stop_mode_ = 0;
    int i_stop_ = 0;    //用于多次赋值 stop_speed_ 和 stop_receive_time_
    double stop_receive_time_;
    double stop_speed_;   //穿过终点线时的速度

    //controller
    //longutidinal_controller
    PidController pid_controller_;
    //lateral_controller
    PurePursuitController pure_pursuit_controller_;
    StanleyController stanley_controller_;
    LqrController lqr_controller_;
    MpcController mpc_controller_;
    PurePursuitFrontAxisController pure_pursuit_front_axis_controller_;
    RibbonController ribbon_controller_;
    
};
}

#endif //CONTRLLER_HPP