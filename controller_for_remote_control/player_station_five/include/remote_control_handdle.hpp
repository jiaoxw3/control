/*
    Formula Student Driverless Project (FSD-Project).
    Copyright (c) 2018:
     - Sonja Brits <britss@ethz.ch>

    FSD-Project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    FSD-Project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with FSD-Project.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef REMOTE_CONTROL_HANDLE_HPP
#define REMOTE_CONTROL_HANDLE_HPP

// #include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/RemoteControlCommand.h"
#include <geometry_msgs/Twist.h>
#include "player_station_five.hpp"

namespace ns_remote_control {

class RemoteControlHandle {

 public:
  // Constructor
  RemoteControlHandle(ros::NodeHandle &nodeHandle);

//  // Getters
  int getNodeRate() const;

  // Methods
  void loadParameters();
  void publishToTopics();
  void sendControlCommand();

 private:
  ros::NodeHandle nodeHandle_;
  ros::Publisher RemoteControlHandlePublisher_;
  std::string control_output_topic_name_;
  int node_rate_;
  

  fsd_common_msgs::RemoteControlCommand remote_control_;
  player_station::Player_Station ps_;

  double velocity_max_;
  double steering_max_;
  double dv_min_;
  double dv_max_;
  double ds_min_;
  double ds_max_;
  
};
}
#endif //REMOTE_CONTROL_HANDLE_HPP
