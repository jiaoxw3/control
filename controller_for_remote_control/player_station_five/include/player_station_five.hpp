/*
    
*/

#ifndef PLAYER_STATION_FIVE_HPP
#define PLAYER_STATION_FIVE_HPP

#include <ros/ros.h>
#include <termio.h>
#include <stdio.h>

namespace player_station{

#define KEYCODE_W 0x77
#define KEYCODE_A 0x61
#define KEYCODE_S 0x73
#define KEYCODE_D 0x64

#define KEYCODE_A_CAP 0x41
#define KEYCODE_D_CAP 0x44
#define KEYCODE_S_CAP 0x53
#define KEYCODE_W_CAP 0x57

#define KEYCODE_BLANK 0X20

#define KEYCODE_T 0X74

#define KEYCODE_O 0x6f
#define KEYCODE_U 0x75
#define KEYCODE_K 0x6b

class Player_Station{

public:
    Player_Station();
    void set_max(double velocity_max, double steering_max, double delta_velocity_max, double delta_velocity_min, double delta_steering_max, double delta_steering_min);
    bool is_init();
    double throttle();
    double steering();
    bool go();
    bool stop();
    void get_keyboard();
    void limit_determind();

    bool test1();
    double steering_;
    double velocity_;
private:
    
   
    double velocity_max_;
    double steering_max_;
    
    double dv_min_;
    double dv_max_;
    double ds_min_;
    double ds_max_;

    bool is_init_;

    int in;
    struct termios new_settings;
    struct termios stored_settings;

    bool go_;
    bool stop_; 
    bool test1_; // simulate RES 1 emergency brake


};


}



#endif // PLAYER_STATION_FIVE_HPP