/*

*/

#include <ros/ros.h>
#include "remote_control_handdle.hpp"

typedef ns_remote_control::RemoteControlHandle RemoteControlHandle;

int main(int argc, char **argv)
{
    
    ros::init(argc, argv, "ps5");
	ros::NodeHandle nodeHandle("~");
	RemoteControlHandle RemoteControlHandle(nodeHandle);
	ros::Rate loop_rate(RemoteControlHandle.getNodeRate());

    while(ros::ok()){

		RemoteControlHandle.sendControlCommand();
		ros::spinOnce();
		loop_rate.sleep();
	}
    ros::shutdown();
   	return 0;
}
