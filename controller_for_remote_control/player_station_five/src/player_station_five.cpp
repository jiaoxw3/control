#include "player_station_five.hpp"

namespace player_station{

Player_Station::Player_Station(){
    std::cout<<"A Player Station 5 is created ! !"<<std::endl;
    is_init_ = false;
    velocity_ = 0.0;
    steering_ = 0.0;
    go_ = 0;
    stop_ = 0;
    test1_ = 0;
}

bool Player_Station::is_init(){
    return is_init_;
}

double Player_Station::throttle(){
    return velocity_;
}

double Player_Station::steering(){
    return steering_;
}

bool Player_Station::go(){
    return go_;
}

bool Player_Station::stop(){
    return stop_;
}

bool Player_Station::test1(){
    return test1_;
}

void Player_Station::set_max(double velocity_max, double steering_max, double delta_velocity_max, double delta_velocity_min, double delta_steering_max, double delta_steering_min){
    velocity_max_ = velocity_max;
    steering_max_ = steering_max;
    dv_min_ = delta_velocity_min;
    dv_max_ = delta_velocity_max;
    ds_min_ = delta_steering_min;
    ds_max_ = delta_steering_max;
    is_init_ = true;
}

void Player_Station::limit_determind(){
    // if (velocity_ > velocity_max_)velocity_ = velocity_max_;
    // if (velocity_ <  0)velocity_ = 0;
    if (steering_ > steering_max_)steering_ = steering_max_;
    if (steering_ <-steering_max_)steering_ =-steering_max_;
}

void Player_Station::get_keyboard(){
    tcgetattr(0,&stored_settings);
    new_settings = stored_settings;
    new_settings.c_lflag &= (~ICANON);
    new_settings.c_cc[VTIME] = 0;
    tcgetattr(0,&stored_settings);
    new_settings.c_cc[VMIN] = 1;
    tcsetattr(0,TCSANOW,&new_settings);
    in = getchar();
    tcsetattr(0,TCSANOW,&stored_settings);

    switch(in){
        // case KEYCODE_W:     velocity_ += dv_min_;break;
        // case KEYCODE_W_CAP: velocity_ += dv_max_;break;
        case KEYCODE_W:     velocity_  = dv_min_;break;


        // case KEYCODE_S:     velocity_ -= dv_min_;break;
        // case KEYCODE_S_CAP: velocity_ -= dv_max_;break;

        case KEYCODE_A:     steering_ += ds_min_;break;
        case KEYCODE_A_CAP: steering_ += ds_max_;break;

        case KEYCODE_D:     steering_ -= ds_min_;break;
        case KEYCODE_D_CAP: steering_ -= ds_max_;break;

        case KEYCODE_O:     go_ = 1;break;
        case KEYCODE_U:     stop_ = 1;break;

        case KEYCODE_BLANK: velocity_ = 0.0;break;

        case KEYCODE_T: steering_ = 0;break;

        case KEYCODE_K: test1_ = 1;break;

        default: break;
    }
}

} // namespace player_station
