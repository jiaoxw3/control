/*
    Formula Student Driverless Project (FSD-Project).
    Copyright (c) 2018:
     - Sonja Brits <britss@ethz.ch>

    FSD-Project is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    FSD-Project is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with FSD-Project.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <ros/ros.h>
#include "remote_control_handdle.hpp"


namespace ns_remote_control {

// Constructor
RemoteControlHandle::RemoteControlHandle(ros::NodeHandle &nodeHandle) :
  nodeHandle_(nodeHandle) {
  ROS_INFO("Constructing Handle");
  loadParameters();
  publishToTopics();
}

// Getters
int RemoteControlHandle::getNodeRate() const { return node_rate_; }

// Methods
void RemoteControlHandle::loadParameters() {
  ROS_INFO("loading handle parameters");
  
  if (!nodeHandle_.param<std::string>("control_output_topic_name",
                                      control_output_topic_name_,
                                      "/estimation/velocity_estimation/velocity_estimate")) {
    ROS_WARN_STREAM("Did not load control_output_topic_name. Standard value is: " << control_output_topic_name_);
  }
  if (!nodeHandle_.param<double>("steering_max",
                                      steering_max_,
                                      0.5)) {
    ROS_WARN_STREAM("Did not load steering_max. Standard value is: " << steering_max_);
  }
  if (!nodeHandle_.param<double>("velocity_max",
                                      velocity_max_,
                                      5)) {
    ROS_WARN_STREAM("Did not load velocity_max. Standard value is: " << velocity_max_);
  }

  if (!nodeHandle_.param<double>("delta_velocity_max",
                                      dv_max_,
                                      1.5)) {
    ROS_WARN_STREAM("Did not load delta_velocity_max. Standard value is: " << dv_max_);
  }
  if (!nodeHandle_.param<double>("delta_velocity_min",
                                      dv_min_,
                                      1.5)) {
    ROS_WARN_STREAM("Did not load delta_velocity_min. Standard value is: " << dv_min_);
  }
  if (!nodeHandle_.param<double>("delta_steering_max",
                                      ds_max_,
                                      0.05)) {
    ROS_WARN_STREAM("Did not load delta_steering_max. Standard value is: " << ds_max_);
  }
  if (!nodeHandle_.param<double>("delta_steering_min",
                                      ds_min_,
                                      0.01)) {
    ROS_WARN_STREAM("Did not load delta_steering_min. Standard value is: " << ds_min_);
  }
  
  if (!nodeHandle_.param("node_rate", node_rate_, 1)) {
    ROS_WARN_STREAM("Did not load node_rate. Standard value is: " << node_rate_);
  }
  ps_.set_max(velocity_max_,steering_max_,dv_max_,dv_min_,ds_max_,ds_min_);
}


void RemoteControlHandle::publishToTopics() {
  ROS_INFO("publish to Control Command topics");
    RemoteControlHandlePublisher_ = nodeHandle_.advertise<fsd_common_msgs::RemoteControlCommand>(control_output_topic_name_, 1);
}

void RemoteControlHandle::sendControlCommand() {
  RemoteControlHandlePublisher_.publish(remote_control_);
  ps_.get_keyboard();
  ps_.limit_determind();
  remote_control_.throttle = ps_.throttle();
  // if(ps_.steering() >= steering_max_){
  //    ps_.steering_ = steering_max_;
  // }
  // if(ps_.steering() <= -steering_max_){
  //   ps_.steering_ = -steering_max_;
  // }

  remote_control_.steering_angle = ps_.steering();
  remote_control_.go = ps_.go();
  remote_control_.stop = ps_.stop();

  remote_control_.test1 = ps_.test1();
  
  remote_control_.header.stamp = ros::Time::now();
  // if(ps_.is_init())
  std::cout<<remote_control_.throttle<<std::endl;
  if(remote_control_.throttle!=0){
    RemoteControlHandlePublisher_.publish(remote_control_);
    remote_control_.throttle = 0;
  }

}
}
